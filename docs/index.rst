.. Cicero documentation master file, created by
   sphinx-quickstart on Sun Jul 19 10:15:26 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cicero's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   overview
   api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

