# -*- coding: utf-8 -*-


import unittest

from cicero import xml_base_handling

class TestBasesFiles(unittest.TestCase):

    def setUp(self):
        pass

    def test_smoke_localization_files(self):
        bc = xml_base_handling.CiceroBases()
        self.assertEqual(len(bc.all_bases), 3)
        self.assertEqual(bc.all_bases[0].size, 39225)
        self.assertEqual(bc.all_bases[1].size, 999)
        self.assertEqual(bc.all_bases[2].size, 180)
