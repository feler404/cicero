#!/usr/bin/env python3
# -*- codging: utf-8 -*-
#
# library for: Cicero Open Project
#


# std
import os
import re
import sys
import logging
import random
import i18n
import gettext
from cmd import Cmd
from pprint import pprint

from global_direct import *


# logging settings
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger_handler = logging.StreamHandler(sys.stdout)
logger_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s : %(levelname)s : %(message)s")
logger_handler.setFormatter(formatter)
logger.addHandler(logger_handler)



# File location directory.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

# i18n directory.
localedir = os.path.join(BASE_DIR + '/cicero_i18n')
   
#t = gettext.translation('msg', localedir='/cicero_i18n/locale', languages=['pl_PL']).install(True)


#_ = i18n.language.gettext #use ugettext instead of getttext to avoid unicode errors
#_ = t.ugettext
_ = lambda *args: args


class BColours:
    HEADER = '\033[95m'
    OK_BLUE = '\033[94m'
    OK_GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END_C = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class CiceroConsole(Cmd):

    intro_big = """
=======================================================================================
    ,o888888o.     8 8888     ,o888888o.    8 8888888 8 888888888o.      ,o888888o.    
   8888     `88.   8 8888    8888     `88.  8 8888    8 8888    `88.  . 8888     `88.  
,8 8888       `8.  8 8888 ,8 8888       `8. 8 8888    8 8888     `88 ,8 8888       `8b 
88 8888            8 8888 88 8888           8 8888    8 8888     ,88 88 8888        `8b
88 8888            8 8888 88 8888           8 8888888 8 8888.   ,88' 88 8888         88
88 8888            8 8888 88 8888           8 8888    8 888888888P'  88 8888         88
88 8888            8 8888 88 8888           8 8888    8 8888`8b      88 8888        ,8P
`8 8888       .8'  8 8888 `8 8888       .8' 8 8888    8 8888 `8b.    `8 8888       ,8P 
   8888     ,88'   8 8888    8888     ,88'  8 8888    8 8888   `8b.   ` 8888     ,88'  
    `8888888P'     8 8888     `8888888P'    8 8888888 8 8888     `88.    `8888888P'     
=======================================================================================
============================== Cicero Command Console =================================
=======================================================================================
    """

    intro_small = """
  ___ _
 / __(_)__ ___ _ _ ___ 
| (__| / _/ -_) '_/ _ ]
 \___|_\__\___|_| \___/

    Cicero Command Console
    """

#    use_rawinput = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.size = os.get_terminal_size()
        if self.size.columns >= 88:
            self.cicero_intro = self.intro_big
        else:
            self.cicero_intro = self.intro_small
        self.stdout.write(str(self.cicero_intro)+"\n")

        from xml_base_handling import CiceroBases
        from grammatic import GrammaticSolver, PartOfSpeach
        from exceptions import DeclinationError
        
        self.cicero_bases = CiceroBases()
        self.solver = GrammaticSolver()

    def do_latin(self, line):
        """
        Search latin word in `William Whitaker's Words` base.
        Press tab if wont display suggestions and matching words count.

        Use example :
                latin ancilla<enter> : search ancilla in dictionary
                latin labor<tab><tab> : display suggestions for labor

        Common part of speech shortcuts:
                N : Nouns
                V : Verbs
                ADJ : Adjectives
                PRON : Pronouns
                ADV : Adverbs
                PREP : Prepositions
                CONJ : Conjunctions
                INTERJ : Interjections
        """
        self._search(line, tag="lat")

    def complete_latin(self, text, line, begidx, endidx):
        
        completions = []
        if not text:
            ret = self.cicero_bases.search(str("a"), max_return=100)
        else:
            ret = self.cicero_bases.search(str(text), max_return=100)

        count_matching = str(len(ret)) if len(ret) < 100 else "more than 100"
        ret = ret[:20]
        for id_ in ret:
            try:
                resolved_row = self.cicero_bases.words.resolve_row(id_)
                logger.debug("{} : {}".format(resolved_row.lat, 
                                              resolved_row.info))
                completions.append("{} : {}".format(resolved_row.lat[0], 
                                                    resolved_row.info[0])) 
            except Exception as e:
                import traceback
                traceback.print_exc()
                logger.error("{}".format(e))
        completions.append("{} : {}".format("~matching", count_matching))
        if len(completions) == 1:
            completions = []
        return completions

    def do_english(self, line):
        """
        Search english word in `William Whitaker's Words` base,
        default return three occur. Use --strict switch if you wont find exact
        same fraze


        Use example :
                english man<enter> : search fraze like `man-worshiper`
                english man --strict<enter> : search strict `man` fraze

        """
        split_line = line.strip().split()
        if len(split_line) == 1:
            self._search(line, tag="eng")
        elif split_line[1] in ["-s", "--strict"]:
            self._search(split_line[0], tag="eng", pattern="exact")

    def _search(self, line, tag=None, pattern=None):
        if not line.strip():
            print("Please write word to translate.")
            return
        if not tag:
            tag = "lat"

        ret = self.cicero_bases.search(str(line.strip()), tag=tag, pattern=pattern)
        for count, id_ in enumerate(ret, 1):
            for i in range(10):
                try:
                    pos = self.cicero_bases.words.resolve_row(id_)
                except DeclinationError:
                    logger.warn("Declination Error")
                    continue
            line = "{}:{}\nlatin : {}\ninfo : {}\nenglish : {}".format(
                count, "*" * (self.size.columns-2), pos.lat, pos.info, pos.eng)
            print(line)
        if not ret:
            print("Don't found: {}".format(line))

    def do_random_adjective(self, line):
        pos = self.cicero_bases.random_adjective()
        pprint({k: v for k, v in pos.__dict__.items() if not k.startswith("_")})

    def do_random_noun(self, line):
        pos = self.cicero_bases.random_noun()
        pprint({k: v for k, v in pos.__dict__.items() if not k.startswith("_")})

    def do_random_verb(self, line):
        pos = self.cicero_bases.random_verb()
        pprint({k: v for k, v in pos.__dict__.items() if not k.startswith("_")})

    def emptyline(self):
        pass

    def do_EOF(self, line=None):
        return True

    def do_words_teach(self, s):
        """
        Sub-menu for latin words teaching. It is 50 lessons and three levels
        od difficulty. Lesson are prepare by 20 questions.
        """
        i = WordsTeach(cicero_bases=self.cicero_bases)
        i.prompt = self.prompt[:-1]+':Words) '
        i.cmdloop()

    def do_exit(self, line=None):
        """
        Exit from program.
        """
        print("Goodbye!")
        sys.exit(0)


class WordsTeach(Cmd):
    def __init__(self, *args, cicero_bases, **kwargs):
        super().__init__(*args, **kwargs)
        self.cicero_bases = cicero_bases
        self.lesson = 0

    def do_select_lesson(self, line=None):
        user_lesson = line
        if not user_lesson:
            for number, name in enumerate(v_lessons_name_1):
                print(number, "-", name)
            user_lesson = input("Select lesson: ")
        if user_lesson.isdigit() and len(user_lesson) >= 0 and len(user_lesson) < len(v_lessons_name_1):
            self.lesson = int(user_lesson)
            print("Lesson selected: {}".format(v_lessons_name_1[int(user_lesson)]))
        else:
            print("Bad choice.") 

    def do_select_level(self, line):
        print("Easy - 1")
        print("Norma - 2")
        print("Hard - 3")

    def do_start(self, line):
        questions_list = list(range(1 * self.lesson, 20 * self.lesson))
        user_points = 0 
        for _ in range(20):
            questionid_ = questions_list.pop(questions_list.index(random.choice(questions_list)))
            raw_question = self.cicero_bases.words_mini.elist[questionid_]
            print(BColours.HEADER + raw_question.get("lat") + BColours.END_C)
            user_answer = input("Answer: ").strip().lower()
            if user_answer in raw_question.get("pol").split(","):
                print(BColours.OK_GREEN + "Right! One point" + BColours.END_C)
                user_points += 1
            else:
                print(BColours.FAIL + "Bad! No point" + BColours.END_C)
                print(gettext("Right answer: {}").format(raw_question.get("pol")))
        print("Your result {}/20".format(user_points))

    def emptyline(self):
        pass

    def do_EOF(self, line=None):
        return True

    def do_exit(self, line=None):
        """
        Exit from program.
        """
        print("Goodbye!")
        sys.exit(0)

if __name__ == '__main__':
    cicero_cmd = CiceroConsole()
    cicero_cmd.cmdloop()
