# -*- coding: utf-8 -*-


# IMPORTS
import copy
from collections  import defaultdict
from time import time
from random import randint

from exceptions import DeclinationError
import global_direct as GD

class PartOfSpeach(object):

    def __init__(self, e_element, grammatic_forms=False):
        self.solver = GrammaticSolver()
        self._element = e_element
        self.info = None
        self.lat = None
        self.type_of_part = None
        self.problem = []
        self.info = self._element.get("info")[1:-1].split(",")
        self.lat = self._element.get("lat").split(",")
        self.eng = self._element.get("eng").split(";")
        self.type_of_part = self.info[0]
        if grammatic_forms:
            self._init_latin_part()

    def __repr__(self):
        return "<grammatic.PartOfSpeach {} {}>".format(self.lat, self.type_of_part)

    def _init_latin_part(self):
        if self.type_of_part == "N":
            if self._check_noun():
                self._resolve()
            else:
                raise DeclinationError("Checking noun wrong {}".format(self.__dict__))
        elif self.type_of_part == "ADJ":
            if self._check_adjective():
                self._resolve()
            else:
                raise DeclinationError("Checking adjective wrong {}".format(self.__dict__))
        elif self.type_of_part == "V":
            if self._check_verb():
                self._resolve()
            else:
                raise DeclinationError("Checking verb wrong {}".format(self.__dict__))

    def _check_noun(self):
        if len(self.lat) < 2:
            self.type_of_part = "X"
            self.problem.append("Unnown noun forms.")
        if len(self.info) < 3 or self.info[2] not in 'MFN':
            self.type_of_part = "X"
            self.problem.append("Unnown gender.")
        if len(self.info) < 2 or self.info[1] not in '12345':
            self.type_of_part = "X"
            self.problem.append("Unnown declination.")

        if self.type_of_part == "N":
            return True

    def _check_adjective(self):
        return True

    def _check_verb(self):
        return True

    def _get_nominativus(self):
        if self.type_of_part == "N":
            return self.lat[0].strip()

    def _get_genetivus(self):
        if self.type_of_part == "N":
            return self.lat[1].strip()

    def _get_gender(self):
        if self.type_of_part == "N":
            return self.info[2]

    def _get_declination(self):
        if self.type_of_part == "N":
            return int(self.info[1])

    def _resolve(self):
        if self.type_of_part == "N":
            self._make_declination_of_noun()
        elif self.type_of_part == "ADJ":
            self._make_declination_of_adjective()
        elif self.type_of_part == "V":
            self._make_conjugation_of_verb()

    def _make_declination_of_noun(self):
        self._list_of_various = list(map(lambda s: s.strip(), self.solver.resolve_noun(pos=self)))
        form_names = ["nom_sing", "gen_sing", "dat_sing", "acc_sing", "abl_sing", "voc_sing",
                      "nom_plur", "gne_plur", "dat_plur", "acc_plur", "abl_plur", "voc_plur"]
        self.form = dict(zip(form_names, self._list_of_various))

    def _make_declination_of_adjective(self):
        self._list_of_various = self.solver.resolve_adjective(pos=self)
        form_names = ["mas_nom_sing", "mas_gen_sing", "mas_dat_sing", "mas_acc_sing", "mas_abl_sing", "mas_voc_sing",
                      "mas_nom_plur", "mas_gne_plur", "mas_dat_plur", "mas_acc_plur", "mas_abl_plur", "mas_voc_plur",
                      "fem_nom_sing", "fem_gen_sing", "fem_dat_sing", "fem_acc_sing", "fem_abl_sing", "fem_voc_sing",
                      "fem_nom_plur", "fem_gne_plur", "fem_dat_plur", "fem_acc_plur", "fem_abl_plur", "fem_voc_plur",
                      "neu_nom_sing", "neu_gen_sing", "neu_dat_sing", "neu_acc_sing", "neu_abl_sing", "neu_voc_sing",
                      "neu_nom_plur", "neu_gne_plur", "neu_dat_plur", "neu_acc_plur", "neu_abl_plur", "neu_voc_plur"]
        self.form = dict(zip(form_names, self._list_of_various))
        #print (len(self.form))

    def _make_conjugation_of_verb(self):
        self._list_of_various = self.solver.resolve_verb(pos=self)
        #form_names = ["conjugation_1", "conjugation_2", "conjugation_3", "conjugation_4"]
        self.form = self._list_of_various

    def _init_english_part(self):
        pass


class GrammaticSolver(object):

    # Noun endings
    DEC_1N_F = ('a', 'ae', 'ae', 'am', 'a', 'a', 'ae', 'arum', 'is', 'as', 'is', 'ae')
    DEC_2N_M = ('us', 'i', 'o', 'um', 'o', 'e', 'i', 'orum', 'is', 'os', 'is', 'i')
    DEC_2N_N = ('um', 'i', 'o', 'um', 'o', 'um', 'a', 'orum', 'is', 'a', 'is', 'a')
    DEC_3SPN_MF = ('or', 'is', 'i', 'em', 'e', 'or', 'es', 'um', 'ibus', 'es', 'ibus', 'es')
    DEC_3SPN_N = ('', 'is', 'i', '', 'e', '', 'a', 'um', 'ibus', 'a', 'ibus', 'a')
    DEC_3SGN_N = ('', 'is', 'i', '', 'i', 'e', 'ia', 'ium', 'ibus', 'ia', 'ibus', 'ia')
    DEC_3MN_F1 = ('is', 'is', 'i', 'em', 'e', 'is', 'es', 'ium', 'ibus', 'es', 'ibus', 'es')
    DEC_3MN_F2 = ('s', 'is', 'i', 'em', 'e', 's', 'es', 'ium', 'ibus', 'es', 'ibus', 'es')
    DEC_4N_M = ('us', 'us', 'ui', 'um', 'u', 'us', 'us', 'uum', 'ibus', 'us', 'ibus', 'us')
    DEC_4N_N = ('u', 'us', 'u', 'u', 'u', 'u', 'ua', 'uum', 'ibus', 'ua', 'ibus', 'ua')
    DEC_5N_F = ('s', 'i', 'i', 'm', '', 's', 's', 'rum', 'bus', 's', 'bus', 's')

    # Adjective endings
    DEC_1F = ('a', 'ae', 'ae', 'am', 'a', 'a', 'ae', 'arum', 'is', 'as', 'is', 'ae')
    DEC_2M = ('us', 'i', 'o', 'um', 'o', 'e', 'i', 'orum', 'is', 'os', 'is', 'i')
    DEC_2N = ('um', 'i', 'o', 'um', 'o', 'um', 'o', 'orum', 'is', 'a', 'is', 'a')
    DEC_31M = ('', 'is', 'i', 'em', 'i', '', 'es', 'ium', 'ibus', 'es', 'ibus', 'es')  # -er, -is, -e
    DEC_31F = ('is', 'is', 'i', 'em', 'i', 'is', 'es', 'ium', 'ibus', 'es', 'ibus', 'es')  # -er, -is, -e
    DEC_31N = ('e', 'is', 'i', 'e', 'i', 'e', 'ia', 'ium', 'ibus', 'ia', 'ibus', 'ia')  # -er, -is, -e
    DEC_32M = ('is', 'is', 'i', 'em', 'i', 'is', 'es', 'ium', 'ibus', 'es', 'ibus', 'es')  # -is, -is, -e
    DEC_32F = ('is', 'is', 'i', 'em', 'i', 'is', 'es', 'ium', 'ibus', 'es', 'ibus', 'es') # -is, -is, -e
    DEC_32N = ('e', 'is', 'i', 'e', 'i', 'e', 'ia', 'ium', 'ibus', 'ia', 'ibus', 'ia')  # -is, -is, -e
    DEC_33M = ('', 'is', 'i', 'em', 'i', '', 'es', 'ium', 'ibus', 'es', 'ibus', 'es')  # -x, -x, -x
    DEC_33F = ('', 'is', 'i', 'es', 'i', '', 'es', 'ium', 'ibus', 'es', 'ibus', 'es')  # -x, -x, -x
    DEC_33N = ('', 'is', 'i', '', 'i', '', 'ia', 'ium', 'ibus', 'ia', 'ibus', 'ia')  # -x, -x, -x

    # Verb endings
    CON_1_PREASENTIS = (
            'o', 'as', 'at', 'amus', 'atis', 'ant',  # Indicativus
            'em', 'es', 'et', 'emus', 'etis', 'ent',  # Coniunctivus
            '-', 'a', '-', '-', 'ate', '-',  # Imperativus
            '-', '-', 'ans', '-', '-', '-',  # Participium
            '-', '-', 'are', '-', '-', '-',  # Infinitivus
            'or', 'aris', 'atur', 'amur', 'amini', 'antur',  # Indicativus
            'er', 'eris', 'etur', 'emur', 'emini', 'entur',  # Coniunctivus
            '-', 'are', '-', '-', 'amini', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', 'ari', '-', '-', '-')  # Infinitivus

    CON_1_IMPERFECTI = (  # 60
            'abam', 'abas', 'abat', 'abamus', 'abatis', 'abant',  # Indicativus
            'arem', 'ares', 'aret', 'aremus', 'aretis', 'arent',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-',  # Infinitivus
            'abar', 'abaris', 'abatur', 'abamur', 'abamini', 'abantur',  # Indicativus
            'arer', 'areris', 'aretur', 'aremur', 'aremini', 'arentur',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_1_FUTURUM_1 = (  # 120
            'abo', 'abis', 'abit', 'abimus', 'abitis', 'abunt',  # Indicativus
            '-', '-', '-', '-', '-', '-',  # Coniunctivus
            '-', 'ato', 'ato', '-', 'atote', 'anto',  # Imperativus
            'aturus', '-', '-', '-', '-', '-',  # Participium
            'aturus', '-', '-', '-', '-', '-',  # Infinitivus
            'abor', 'aberis', 'abitur', 'abimur', 'abimini', 'abuntur',  # Indicativus
            '-', '-', '-', '-', '-', '-',  # Coniunctivus
            '-', 'ator', 'ator', '-', '-', 'antor',  # Imperativus
            '-', '-', 'andus', '-', '-', '-',  # Participium
            'atum', '-', '-', '-', '-', '-')  # Infinitivus

    CON_1_PERFECTI = (  # 180
            'avi', 'avisti', 'avit', 'avimus', 'avistis', 'averunt',  # Indicativus
            'averim', 'averis', 'averit', 'averimus', 'averitis', 'averint',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            'avisse', '-', '-', '-', '-', '-',  # Infinitivus
            'atus', '-', '-', 'ati', '-', '-',  # Indicativus
            'atus', '-', '-', 'ati', '-', '-',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', 'atus', '-', '-', '-',  # Participium
            'atus', '-', '-', '-', '-', '-')  # Infinitivus

    CON_1_PLUSQUAM_PERFECTI = (  # 240
            'averam', 'averas', 'averat', 'averamus', 'averatis', 'averant',  # Indicativus
            'avissem', 'avisses', 'avisset', 'avissemus', 'avissetis', 'avissent',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-',  # Infinitivus
            'atus', '-', '-', 'ati', '-', '-',  # Indicativus
            'atus', '-', '-', 'ati', '-', '-',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_1_FUTURI_EXACTI = (  # 300
            'avero', 'averis', 'averit', 'averimus', 'averitis', 'averint',  # Indicativus
            '-', '-', '-', '-', '-', '-',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-',  # Infinitivus
            'atus', '-', '-', 'ati', '-', '-',  # Indicativus
            '-', '-', '-', '-', '-', '-',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_2_PREASENTIS = (  # 360
            'eo', 'es', 'et', 'emus', 'etis', 'ent',  # Indicativus
            'eam', 'eas', 'eat', 'eamus', 'eatis', 'eant',  # Coniunctivus
            '-', 'e', '-', '-', 'ete', '-',  # Imperativus
            '-', '-', 'ens', '-', '-', '-',  # Participium
            '-', '-', 'ere', '-', '-', '-',  # Infinitivus
            'eor', 'eris', 'etur', 'emur', 'emini', 'entur',  # Indicativus
            'ear', 'earis', 'eatur', 'eamur', 'eamini', 'eantur',  # Coniunctivus
            '-', 'ere', '-', '-', 'emini', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', 'eri', '-', '-', '-')  # Infinitivus

    CON_2_IMPERFECTI = ( # 420
            'ebam', 'ebas', 'ebat', 'ebamus', 'ebatis', 'ebant',  # Indicativus
            'erem', 'eres', 'eret', 'eremus', 'eretis', 'erent',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-',  # Infinitivus
            'ebar', 'ebaris', 'ebatur', 'ebamur', 'ebamini', 'ebantur',  # Indicativus
            'erer', 'ereris', 'eretur', 'eremur', 'eremini', 'erentur',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_2_FUTURUM_1 = ( # 480
            'ebo', 'ebis', 'ebit', 'ebimus', 'ebitis', 'ebunt',  # Indicativus
            '-', '-', '-', '-', '-', '-',  # Coniunctivus
            '-', 'eto', 'eto', '-', 'etote', 'ento',  # Imperativus
            'eturus', '-', '-', '-', '-', '-',  # Participium
            'eturus', '-', '-', '-', '-', '-',  # Infinitivus
            'ebor', 'eberis', 'ebitur', 'ebimur', 'ebimini', 'ebuntur',  # Indicativus
            '-', '-', '-', '-', '-', '-',  # Coniunctivus
            '-', 'etor', 'etor', '-', '-', 'ntor',  # Imperativus
            '-', '-', 'endus', '-', '-', '-',  # Participium
            'etum', '-', '-', '-', '-', '-')  # Infinitivus

    CON_2_PERFECTI = ( # 540
            'evi', 'evisti', 'evit', 'evimus', 'evistis', 'everunt',  # Indicativus
            'everim', 'everis', 'everit', 'everimus', 'everitis', 'everint',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            'evisse', '-', '-', '-', '-', '-',  # Infinitivus
            'etus', '-', '-', 'eti', '-', '-',  # Indicativus
            'etus', '-', '-', 'eti', '-', '-',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', 'etus', '-', '-', '-',  # Participium
            'etus', '-', '-', '-', '-', '-')  # Infinitivus

    CON_2_PLUSQUAM_PERFECTI = ( # 600
            'everam', 'everas', 'everat', 'everamus', 'everatis', 'everant',  # Indicativus
            'evissem', 'evisses', 'evisset', 'evissemus', 'evissetis', 'evissent',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-',  # Infinitivus
            'etus', '-', '-', 'eti', '-', '-',  # Indicativus
            'etus', '-', '-', 'eti', '-', '-',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_2_FUTURI_EXACTI = ( # 660
            'evero', 'everis', 'everit', 'everimus', 'everitis', 'everint',  # Indicativus
            '-', '-', '-', '-', '-', '-',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-',  # Infinitivus
            'etus', '-', '-', 'eti', '-', '-',  # Indicativus
            '-', '-', '-', '-', '-', '-',  # Coniunctivus
            '-', '-', '-', '-', '-', '-',  # Imperativus
            '-', '-', '-', '-', '-', '-',  # Participium
            '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_3_PREASENTIS = ( # 720
        'o', 'is', 'it', 'imus', 'itis', 'unt',  # Indicativus
        'am', 'as', 'at', 'amus', 'atis', 'ant',  # Coniunctivus
        '-', 'e', '-', '-', 'ite', '-',  # Imperativus
        '-', '-', 'ens', '-', '-', '-',  # Participium
        '-', '-', 'ere', '-', '-', '-',  # Infinitivus
        'or', 'eris', 'itur', 'imur', 'imini', 'untur',  # Indicativus
        'ar', 'aris', 'atur', 'amur', 'amini', 'antur',  # Coniunctivus
        '-', 'ere', '-', '-', 'imini', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', 'i', '-', '-', '-')  # Infinitivus

    CON_3_IMPERFECTI = ( # 780
        'ebam', 'ebas', 'ebat', 'ebamus', 'ebatis', 'ebant',  # Indicativus
        'erem', 'eres', 'eret', 'eremus', 'eretis', 'erent',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-',  # Infinitivus
        'ebar', 'ebaris', 'ebatur', 'ebamur', 'ebamini', 'ebantur',  # Indicativus
        'erer', 'ereris', 'eretur', 'eremur', 'eremini', 'erentur',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_3_FUTURUM_1 = ( # 840
        'am', 'es', 'et', 'emus', 'etis', 'ent',  # Indicativus
        '-', '-', '-', '-', '-', '-',  # Coniunctivus
        '-', 'ito', 'ito', '-', 'itote', 'unto',  # Imperativus
        'turus', '-', '-', '-', '-', '-',  # Participium
        'turus', '-', '-', '-', '-', '-',  # Infinitivus
        'ar', 'eris', 'etur', 'emur', 'emini', 'entur',  # Indicativus
        '-', '-', '-', '-', '-', '-',  # Coniunctivus
        '-', 'itor', 'itor', '-', '-', 'untor',  # Imperativus
        '-', '-', 'endus', '-', '-', '-',  # Participium
        'tum', '-', '-', '-', '-', '-')  # Infinitivus

    CON_3_PERFECTI = ( # 900
        'i', 'isti', 'it', 'imus', 'istis', 'erunt',  # Indicativus
        'erim', 'eris', 'erit', 'erimus', 'eritis', 'erint',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        'isse', '-', '-', '-', '-', '-',  # Infinitivus
        'tus', '-', '-', 'ti', '-', '-',  # Indicativus
        'tus', '-', '-', 'ti', '-', '-',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', 'tus', '-', '-', '-',  # Participium
        'tus', '-', '-', '-', '-', '-')  # Infinitivus

    CON_3_PLUSQUAM_PERFECTI = ( # 960
        'eram', 'eras', 'erat', 'eramus', 'eratis', 'erant',  # Indicativus
        'issem', 'isses', 'isset', 'issemus', 'issetis', 'issent',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-',  # Infinitivus
        'tus', '-', '-', 'ti', '-', '-',  # Indicativus
        'tus', '-', '-', 'ti', '-', '-',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_3_FUTURI_EXACTI = ( # 1020
        'ero', 'eris', 'erit', 'erimus', 'eritis', 'erint',  # Indicativus
        '-', '-', '-', '-', '-', '-',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-',  # Infinitivus
        'tus', '-', '-', 'ti', '-', '-',  # Indicativus
        '-', '-', '-', '-', '-', '-',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_4_PREASENTIS = ( # 1080
        'io', 'is', 'it', 'imus', 'itis', 'iunt',  # Indicativus
        'iam', 'ias', 'iat', 'iamus', 'iatis', 'iant',  # Coniunctivus
        '-', 'i', '-', '-', 'ite', '-',  # Imperativus
        '-', '-', 'iens', '-', '-', '-',  # Participium
        '-', '-', 'ire', '-', '-', '-',  # Infinitivus
        'ior', 'iris', 'itur', 'imur', 'imini', 'iuntur',  # Indicativus
        'iar', 'iaris', 'iatur', 'iamur', 'iamini', 'iantur',  # Coniunctivus
        '-', 'ire', '-', '-', 'imini', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', 'iri', '-', '-', '-')  # Infinitivus

    CON_4_IMPERFECTI = ( # 1140
        'iebam', 'iebas', 'iebat', 'iebamus', 'iebatis', 'iebant',  # Indicativus
        'irem', 'ires', 'iret', 'iremus', 'iretis', 'rient',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-',  # Infinitivus
        'iebar', 'iebaris', 'iebatur', 'iebamur', 'iebamini', 'iebantur',  # Indicativus
        'irer', 'ireris', 'iretur', 'iremur', 'iremini', 'irentur',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_4_FUTURUM_1 = ( # 1200
        'iam', 'ies', 'iet', 'iemus', 'ietis', 'ient',  # Indicativus
        '-', '-', '-', '-', '-', '-',  # Coniunctivus
        '-', 'ito', 'ito', '-', 'itote', 'iunto',  # Imperativus
        'iturus', '-', '-', '-', '-', '-',  # Participium
        'iturus', '-', '-', '-', '-', '-',  # Infinitivus
        'iar', 'ieris', 'ietur', 'iemur', 'iemini', 'ientur',  # Indicativus
        '-', '-', '-', '-', '-', '-',  # Coniunctivus
        '-', 'itor', 'itor', '-', '-', 'iuntor',  # Imperativus
        '-', '-', 'iendus', '-', '-', '-',  # Participium
        'itum', '-', '-', '-', '-', '-')  # Infinitivus

    CON_4_PERFECTI = ( # 1260
        'ivi', 'ivisti', 'ivit', 'ivimus', 'ivistis', 'iverunt',  # Indicativus
        'iverim', 'iveris', 'iverit', 'iverimus', 'iveritis', 'iverint',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        'isse', '-', '-', '-', '-', '-',  # Infinitivus
        'itus', '-', '-', 'iti', '-', '-',  # Indicativus
        'itus', '-', '-', 'iti', '-', '-',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', 'itus', '-', '-', '-',  # Participium
        'itus', '-', '-', '-', '-', '-')  # Infinitivus

    CON_4_PLUSQUAM_PERFECTI = ( # 1320
        'iveram', 'iveras', 'iverat', 'iveramus', 'iveratis', 'iverant',  # Indicativus
        'ivissem', 'ivisses', 'ivisset', 'ivissemus', 'ivissetis', 'ivissent',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-',  # Infinitivus
        'itus', '-', '-', 'iti', '-', '-',  # Indicativus
        'itus', '-', '-', 'iti', '-', '-',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-')  # Infinitivus

    CON_4_FUTURI_EXACTI = ( # 1380
        'ivero', 'iveris', 'iverit', 'iverimus', 'iveritis', 'iverint',  # Indicativus
        '-', '-', '-', '-', '-', '-',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-',  # Infinitivus
        'itus', '-', '-', 'iti', '-', '-',  # Indicativus
        '-', '-', '-', '-', '-', '-',  # Coniunctivus
        '-', '-', '-', '-', '-', '-',  # Imperativus
        '-', '-', '-', '-', '-', '-',  # Participium
        '-', '-', '-', '-', '-', '-')  # Infinitivus
        #                   # 1440

    def resolve_noun(self, pos=None):
        """
        Declination noun

        :param pos: Instance class PartOfSpeach
        :return: List with form of declination
        """

        declination_return = []
        nom = pos._get_nominativus()
        gen = pos._get_genetivus()
        gender = pos._get_gender()
        declination = pos._get_declination()

        #Declination:1
        if declination == 1 and gender == 'F':
            for i in range(12):
                declination_return.append(nom[:-1] + self.DEC_1N_F[i])

        #Declination:2
        if declination == 2 and gender == 'M' and nom[-2:] == 'us':
            for i in range(12):
                declination_return.append(nom[:-2] + self.DEC_2N_M[i])
        if declination == 2 and gender == 'M' and nom[-2:] == 'er':
            for i in range(12):
                declination_return.append(nom + self.DEC_2N_M[i])
            declination_return[0] = declination_return[5] = nom
        if declination == 2 and gender == 'N'and nom[-2:] == 'um':
            for i in range(12):
                declination_return.append(nom[:-2] + self.DEC_2N_N[i])

        #Declination:3SP
        if declination == 3 and gender == 'M':
            for i in range(12):
                declination_return.append(gen[:-2] + self.DEC_3SPN_MF[i])
            declination_return[0] = declination_return[5] = nom
        if declination == 3 and gender == 'F' and gen[-2:] == 'is' and not(self.equal_sound(nom, gen)):
            for i in range(12):
                declination_return.append(gen[:-2] + self.DEC_3SPN_MF[i])
            declination_return[0] = declination_return[5] = nom
        if declination == 3 and gender == 'N' and nom[-1:] != 'e' and nom[-2:] != 'al' and nom[-2:] != 'er':
            for i in range(12):
                declination_return.append(gen[:-2] + self.DEC_3SPN_N[i])
            declination_return[0] = declination_return[3] = declination_return[5] = nom

        #Declination:3SG
        if declination == 3 and gender == 'N' and (nom[-1:] == 'e' or nom[-2:] == 'al' or nom[-2:] == 'er'):
            for i in range(12):
                declination_return.append(gen[:-2] + self.DEC_3SGN_N[i])
            declination_return[0] = declination_return[3] = declination_return[5] = nom

        #Declination:3M
        if declination == 3 and gender == 'F' and (nom[-2:] == 'is' or nom[-2:] == 'es') and gen[-2:] == 'is' and (self.equal_sound(nom, gen)):
            for i in range(12):
                declination_return.append(gen[:-2] + self.DEC_3MN_F1[i])
        if declination == 3 and gender == 'F' and gen[-2:] == 'is' and not(self.equal_sound(gen=gen)) and not(self.equal_sound(nom, gen)):
            for i in range(12):
                declination_return.append(gen[:-2] + self.DEC_3MN_F2[i])

        #Declination:4
        if declination == 4 and gender == 'M':
            for i in range(12):
                declination_return.append(nom[:-2] + self.DEC_4N_M[i])
                declination_return[0] = nom
        if declination == 4 and gender == 'N':
            for i in range(12):
                declination_return.append(nom[:-1] + self.DEC_4N_N[i])
                declination_return[0] = nom

        #Declination:5
        if declination == 5 and gender == 'F':
            for i in range(12):
                declination_return.append(gen[:-1] + self.DEC_5N_F[i])
                declination_return[0] = nom

        #Returning
        if len(list(declination_return)) != 12:
            ##print info,len(list(declination_return)),nom,gen
            return -1, 'error count'
        return declination_return

    def resolve_adjective(self, pos=None):
        """
        Deklinacja przymiotnikow
        """
        
        declination_return = []
        declination = 0
        if declination == 2:
            declination = 1
        
        if len(pos.lat) != 3:
            return -1, 'error declination'
        if '(' in pos.lat[1]:
            return -1, 'error declination'
        form_m, form_f, form_n = pos.lat

        if '-um' in form_f:
            form_f = form_f[:-4]
            form_n = form_f[:-5] + 'um'
    
        #Declination:1,2
        if form_m[-2:] == 'us' and form_f[-1:] == 'a' and form_n[-2:] == 'um':
            declination = 1.0
            for i in range(12):  # m
                declination_return.append(form_m[:-2] + self.DEC_2M[i])
            for i in range(12):  # f
                declination_return.append(form_f[:-1] + self.DEC_1F[i])
            for i in range(12):  # n
                declination_return.append(form_n[:-2] + self.DEC_2N[i])
    
        if form_f[-2:] == 'er' and form_f[-1:] == 'a' and form_f[-2:] == 'um':
            declination = 1.1
            for i in range(12):  # m
                declination_return.append(form_m[:-2] + self.DEC_2M[i])
            for i in range(12):  # f
                declination_return.append(form_f[:-1] + self.DEC_1F[i])
            for i in range(12):  # n
                declination_return.append(form_n[:-2] + self.DEC_2N[i])
    
        #Declination:31
        if form_m[-2:] == 'er' and form_f[-2:] == 'is' and form_n[-1:] == 'e':
            declination = 3.1
            for i in range(12):  # m
                declination_return.append(form_m + self.DEC_31M[i])
            for i in range(12):  # f
                declination_return.append(form_f[:-2] + self.DEC_31F[i])
            for i in range(12):  # n
                declination_return.append(form_n[:-1] + self.DEC_31N[i])
    
        #Declination:32
        if form_m[-2:] == 'is' and form_f[-2:] == 'is' and form_n[-1:] == 'e':
            declination = 3.2
            for i in range(12):  # m
                declination_return.append(form_m[:-2] + self.DEC_32M[i])
            for i in range(12):  # f
                declination_return.append(form_f[:-2] + self.DEC_32F[i])
            for i in range(12):  # n
                declination_return.append(form_n[:-1] + self.DEC_32N[i])
    
        #Declination:33
        if form_f == '(gen.)':
            declination = 3.3
            for i in range(12):  # m
                declination_return.append(form_n[:-2] + self.DEC_33M[i])
            declination_return[0] = declination_return[5] = form_m
            for i in range(12):  # f
                declination_return.append(form_n[:-2] + self.DEC_33F[i])
            declination_return[0+12] = declination_return[5+12] = form_m
            for i in range(12):  # n
                declination_return.append(form_n[:-2] + self.DEC_33N[i])
            declination_return[0+24]=declination_return[5+24] = form_m
    
        if len(list(declination_return)) != 36:
            ##print info,len(list(declination_return)),form_m,form_f,form_n
            return -1, 'error declination'
        return declination_return

    #<h17 lat="abalieno, abalienare, abalienavi, abalienatus" info="[V,1,TRANS,XXXBS]"></h17>
    def resolve_verb(self, pos=None):
        """
        
        :param pos: 
        :return:
        """
    
        shank, info = pos.lat, pos.info
        core = shank[1][:-3]
        temp_dict = {}

        tables_1 = (self.CON_1_PREASENTIS, self.CON_1_IMPERFECTI, self.CON_1_FUTURUM_1, self.CON_1_PERFECTI, self.CON_1_PLUSQUAM_PERFECTI, self.CON_1_FUTURI_EXACTI)
        tables_2 = (self.CON_2_PREASENTIS, self.CON_2_IMPERFECTI, self.CON_2_FUTURUM_1, self.CON_2_PERFECTI, self.CON_2_PLUSQUAM_PERFECTI, self.CON_2_FUTURI_EXACTI)
        tables_3 = (self.CON_3_PREASENTIS, self.CON_3_IMPERFECTI, self.CON_3_FUTURUM_1, self.CON_3_PERFECTI, self.CON_3_PLUSQUAM_PERFECTI, self.CON_3_FUTURI_EXACTI)
        tables_4 = (self.CON_4_PREASENTIS, self.CON_4_IMPERFECTI, self.CON_4_FUTURUM_1, self.CON_4_PERFECTI, self.CON_4_PLUSQUAM_PERFECTI, self.CON_4_FUTURI_EXACTI)
        tables_names = ("PREASENTIS", "IMPERFECTI", "FUTURUM_1", "PERFECTI", "PLUSQUAM_PERFECTI", "FUTURI_EXACTI")
        forms_names = (
            "1S-IND-A", "2S-IND-A", "3S-IND-A", "1P-IND-A", "2P-IND-A", "3P-IND-A",
            "1S-CON-A", "2S-CON-A", "3S-CON-A", "1P-CON-A", "2P-CON-A", "3P-CON-A",
            "1S-IMP-A", "2S-IMP-A", "3S-IMP-A", "1P-IMP-A", "2P-IMP-A", "3P-IMP-A",
            "1S-PAR-A", "2S-PAR-A", "3S-PAR-A", "1P-PAR-A", "2P-PAR-A", "3P-PAR-A",
            "1S-INF-A", "2S-INF-A", "3S-INF-A", "1P-INF-A", "2P-INF-A", "3P-INF-A",

            "1S-IND-P", "2S-IND-P", "3S-IND-P", "1P-IND-P", "2P-IND-P", "3P-IND-P",
            "1S-CON-P", "2S-CON-P", "3S-CON-P", "1P-CON-P", "2P-CON-P", "3P-CON-P",
            "1S-IMP-P", "2S-IMP-P", "3S-IMP-P", "1P-IMP-P", "2P-IMP-P", "3P-IMP-P",
            "1S-PAR-P", "2S-PAR-P", "3S-PAR-P", "1P-PAR-P", "2P-PAR-P", "3P-PAR-P",
            "1S-INF-P", "2S-INF-P", "3S-INF-P", "1P-INF-P", "2P-INF-P", "3P-INF-P",)

        # conjugation O, 1, 2, 3, 4
        agr_tab = [None, tables_1, tables_2, tables_3, tables_4]

        for tables in agr_tab[int(info[1]):]:
            for i, table in enumerate(tables):
                for sufix_id, sufix in enumerate(table):
                    if sufix != '-':
                        full_form = core + sufix
                    else:
                        full_form = sufix
                    temp_dict[(tables_names[i], forms_names[sufix_id])] = full_form.strip()

    
        return temp_dict

    def pronpun_declination(self):
        """
        Deklinacha wybranych zaimkow
        """""
    
        personal_pronoun_1 = ('ego', 'mei', 'mihi', 'me', 'me', '-', 'nos', 'nostri', 'nobis', 'nos', 'nobis', '-')
        personal_pronoun_2 = ('tu', 'tui', 'tibi', 'te', 'te', '-', 'vos', 'vestri', 'vobis', 'vos', 'vobis', '-')
        personal_pronoun_1 = ('-', 'sui', 'sibi', 'se', 'se', '-', '-', 'sui', 'sibi', 'se', 'se', '-')
    
        demonstrative_pronoun_1m = ('is', 'eius', 'ei', 'eum', 'eo', '-', 'ei', 'eorum', 'eis', 'eos', 'eis', '-')
        demonstrative_pronoun_1f = ('ea', 'eius', 'ei', 'eam', 'ea', '-', 'eae', 'earum', 'eis', 'eas', 'eis', '-')
        demonstrative_pronoun_1n = ('id', 'eius', 'ei', 'id', 'eo', '-', 'ea', 'eorum', 'eis', 'ea', 'eis', '-')
    
        demonstrative_pronoun_2m = ('hic', 'hunc', 'huius', 'huic', 'hoc', '-', 'hi', 'hos', 'horum', 'his', 'his', '-')
        demonstrative_pronoun_2f = ('haec', 'hanc', 'huius', 'huic', 'hac', '-', 'hae', 'has', 'harum', 'his', 'his', '-')
        demonstrative_pronoun_2n = ('hoc', 'huius', 'huic', 'hoc', 'hoc', '-', 'haec', 'heac', 'horum', 'his', 'his', '-')
    
        demonstrative_pronoun_3m = ('ille', 'illum', 'illius', 'illi', 'illo', '-', 'illi', 'illos', 'illorum', 'illis', 'illis', '-')
        demonstrative_pronoun_3f = ('illa', 'illam', 'illius', 'illi', 'illa', '-', 'illae', 'illas', 'illarum', 'illis', 'illis', '-')
        demonstrative_pronoun_3n = ('illud', 'illud', 'illius', 'illi', 'illo', '-', 'illa', 'illa', 'illorum', 'illis', 'illis', '-')
    
        demonstrative_pronoun_4m = ('idem', 'eiusdem', 'eidem', 'eumdem', 'eodem', '-', 'iidem', 'eorundem', 'eisdem', 'eosdem', 'eisdem', '-')
        demonstrative_pronoun_4f = ('eadem', 'eiusdem', 'eidem', 'eandem', 'eadem', '-', 'eaedem', 'eorundem', 'eisdem', 'easdem', 'eisdem', '-')
        demonstrative_pronoun_4n = ('idem', 'eiusdem', 'eidem', 'idem', 'eodem', '-', 'eadem', 'eoryndem', 'eisdem', 'eadem', 'eisdem', '-')
    
        demonstrative_pronoun_5m = ('qui', 'cuius', 'cui', 'quem', 'quo', '-', 'qui', 'quorum', 'quibus', 'quos', 'quibus', '-')
        demonstrative_pronoun_5f = ('quae', 'cuius', 'cui', 'quam', 'qua', '-', 'quae', 'quarum', 'quibus', 'quas', 'quibus', '-')
        demonstrative_pronoun_5n = ('quod', 'cuius', 'cui', 'quod', 'quo', '-', 'quae', 'quorum', 'quibus', 'quae', 'quibus', '-')
    
        demonstrative_pronoun_4m = ('', '', '', '', '', '-', '', '', '', '', '', '-')
        demonstrative_pronoun_4f = ('', '', '', '', '', '-', '', '', '', '', '', '-')
        demonstrative_pronoun_4n = ('', '', '', '', '', '-', '', '', '', '', '', '-')

    def equal_sound(self, nom=None, gen=None):
        """
        Funkcja sprawdzajaca rownozgloskowosc wyrazow
        """
    
        nom_counter = gen_counter = 0
        if len(gen) < 4:
            return False
    
        if nom is None:
            if gen[-3] in 'aoeiyu' or gen[-4] in 'aoeiyu':
                return True
        else:
            for i in range(len(nom)):
                if nom[i] in 'aoeiyu':
                    nom_counter += 1
    
            for i in range(len(gen)):
                if gen[i] in 'aoeiyu':
                    gen_counter += 1
    
            if gen_counter == nom_counter:
                return True
        return False


def test(part_of_speech=None, percent=None):
    """
    Test funkcji odmian sprawdzajaca ilosc poprawnie odmienianych czesci mowy
    
                noun                        adjective
    shift:  0                                   0
    count:  39223                           39223
    part:  18340 , 46 [%]                9062 , 23 [%]
    errors:  770 , 4 [%]                  633 , 6 [%] 
    time:  772 [s]                          747 [s]
    """
    
    tic = time()
    shift = 1
    errors = []
    part = 0
    count = int(float(percent) / 100 * GD.limit)

    if count == 0:
        count = 10
    #print (30 * '*')
    for i in range(count):
        ret_declination = ""
        exec ('ret_declination=(' + part_of_speech + '_declination(number=' + str(i+shift) + '))')
        if ret_declination == -1:
            pass
        else:
            part += 1
            if type(ret_declination) == str:
                errors.append(ret_declination)
                ##print i+shift,ret_declination
            #if type(ret_declination)==list:#print i+shift,ret_declination[0]
        if i % 100 == 0:
            pass
            #print '--', 100 * (i+shift) / count, '%'
    
    #print '--', 100, '%'
    #print 30 * '*'
    #print 'shift: ', shift-1
    #print 'count: ', count
    #print 'part: ', part, ', ', (100 * part) / count, '[%]'
    #print 'errors: ', len(list(errors)), ', ', 100 * len(list(errors)) / part, '[%]'
    #print 'time: ', int(time()-tic), '[s]'
    #print 30 * '*'
    #test('adjective',1)


def random_question(part=None, declination=1, affinity=1, coniugation=1):

    tic = time()
    if part == 'N':
        part_of_speech = 'noun'
    if part == 'V':
        part_of_speech = 'verb'
    if part == 'ADJ':
        part_of_speech = 'adjective'
    if declination == 2 and part == 'ADJ':
        declination = 1
    
    number = randint(1, GD.limit)
    for i in range(GD.limit):
        if number < GD.limit:
            ret_declination = ""
            number += 1
            shank, info = 1, 1 #xml_split_line(xml_read_line(contest='all', number=number))

            if info[0] == 'N' == part and info[1] == str(declination):
                exec ('ret_declination=(' + part_of_speech + '_declination(number=' + str(number) + '))')
                if (ret_declination[0]) != -1:
                    if int(ret_declination[2]) == declination:
                        return number

            if info[0] == 'ADJ' == part:
                exec ('ret_declination=(' + part_of_speech + '_declination(number=' + str(number) + '))')
                if (ret_declination[0]) != -1:
                    if int(ret_declination[2]) == declination:
                        return number

            if info[0] == 'V' == part and info[1] == str(declination) and len(shank) > 1:
                return number  
        else:
            number = 0
        ##print 'VVV2', 'part',part, 'coniugation',coniugation,type(coniugation)
        if float(time()-tic) >= GD.timeSearchQuestion:
            
            #print 'NON DYNAMIK GENER'

            N1_rand_tab = GD.N1_rand_tab
            N2_rand_tab = GD.N2_rand_tab
            N3_rand_tab = GD.N3_rand_tab
            N4_rand_tab = GD.N4_rand_tab
            N5_rand_tab = GD.N5_rand_tab
            J1_rand_tab = GD.J1_rand_tab
            J3_rand_tab = GD.J3_rand_tab
            V1_rand_tab = GD.V1_rand_tab
            V2_rand_tab = GD.V2_rand_tab
            V3_rand_tab = GD.V3_rand_tab
            V4_rand_tab = GD.V4_rand_tab

            if part == 'N':
                if str(declination) == str(1): 
                    return N1_rand_tab[randint(0, len(N1_rand_tab)-1)]
                if str(declination) == str(2):
                    return N2_rand_tab[randint(0, len(N2_rand_tab)-1)]
                if str(declination) == str(3):
                    return N3_rand_tab[randint(0, len(N3_rand_tab)-1)]
                if str(declination) == str(4):
                    return N4_rand_tab[randint(0, len(N4_rand_tab)-1)]
                if str(declination) == str(5):
                    return N5_rand_tab[randint(0, len(N5_rand_tab)-1)]

            if part == 'ADJ':
                if str(declination) == str(1):
                    return J1_rand_tab[randint(0, len(J1_rand_tab)-1)]
                if str(declination) == str(3):
                    return J3_rand_tab[randint(0, len(J3_rand_tab)-1)]
            if part == 'V':
                if str(coniugation) == str(1):
                    return V1_rand_tab[randint(0, len(V1_rand_tab)-1)]
                if str(coniugation) == str(2):
                    return V2_rand_tab[randint(0, len(V2_rand_tab)-1)]
                if str(coniugation) == str(3):
                    return V3_rand_tab[randint(0, len(V3_rand_tab)-1)]
                if str(coniugation) == str(4):
                    return V4_rand_tab[randint(0, len(V4_rand_tab)-1)]
