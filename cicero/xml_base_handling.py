# -*- coding: utf-8 -*-
#
# library for: Cicero Open Project
# 
# TODO: napisac w dokumentacji że baza pochodzi od William Whitaker's Words`

# TODO: pakować pliki bazy
# TODO: pasek/wskaźnik/punkty ładowania

# TODO: środowisko tox
# TODO: testy tox na python2/python3

# IMPORTS
import os
import re
import random
import logging
from xml.etree.ElementTree import parse
from configparser import SafeConfigParser
from os.path import dirname

from grammatic import PartOfSpeach
from exceptions import DeclinationError


logger = logging.getLogger()

BASE_DIR = dirname(dirname(os.path.realpath(__file__)))
INI_FILE_DIR = os.path.join(BASE_DIR, "ui/users/default/config.ini")

parser = SafeConfigParser()
parser.read(INI_FILE_DIR)
WORDS_WHOLE_BASE_FILE = os.path.join(BASE_DIR, parser.get("xml_base", "WORDS_WHOLE_BASE_FILE"))
WORDS_TEACH_BASE_FILE = os.path.join(BASE_DIR, parser.get("xml_base", "WORDS_TEACH_BASE_FILE"))
SENTENCE_BASE_FILE = os.path.join(BASE_DIR, parser.get("xml_base", "SENTENCE_BASE_FILE"))
MAX_CHOICE_NUMBER = int(parser.get("xml_base", "MAX_CHOICE_NUMBER"))  # 100


class BaseController(object):
    """
    Object where Cicero hold all own *bases*. Is two kine of *bases*. Full base
    contain list all possible Latin words and equivalent in other languages. 
    Teach base contain set the most important words need to learn or need to
    make grammatic variation.
    """

    def __init__(self, name, base_path, description, attr_names):
        """
        Create *base* object with XML file

        :param name: short name of file base
        :param base_path: full path to *base* file
        :param description: short description of *base*
        :param attr_names: crucial parameters of *base*
        """
        self.name = name
        self.description = description
        self._base_path = base_path
        self._attr_names = attr_names

    def __str__(self):
        return "Cicero base. Name: {name}, path: {base_path}, desc: {description}.".format(**sentencelf.__dict__)

    def __repr__(self):
        return "<{} : {}>".format(self.__class__.__name__, self.name)

    def __len__(self):
        return self.size

    def __getitem__(self, index):
        try:
            return self.elist[index].get(self._attr_names[0])
        except ValueError:
            raise Exception("Bad row parameter: {}".format(index))

    def _load_from_file(self):
        """
        Make ``etree`` object from XML file. Each row in XML file is translate
        to ``etree`` object. All of objects are manageable like list.
        """
        base_path = os.path.realpath(self._base_path)
        if os.path.exists(base_path):
            self.elist = list(parse(base_path).iterfind("h"))
            self.size = len(self.elist)
            logger.info("Load base : {} : Size : {}".format(base_path, self.size))
        else:
            logger.warn("Base not found : {}".format(base_path))

    def resolve_rows(self, rows=None, *args):
        result = []
        for row in rows:
            result.append(self.resolve_row(row=row))
        return result

    def resolve_row(self, row=None, grammatic_forms=False):
        return PartOfSpeach(self.elist[row], grammatic_forms=grammatic_forms)


class CiceroBases(object):
    """

    """

    all_bases = None
    patterns = {'simple': r'^{}', 'exact': r'^{}([,].*|$)'}

    def __init__(self):
        self.all_bases = [
            BaseController("words", WORDS_WHOLE_BASE_FILE, "William Whitaker's base Latin words",
                ["lat", "eng", "info"]),
            BaseController("words_mini", WORDS_TEACH_BASE_FILE, "Cicero light base Latin, polish and English",
                ["lat", "eng", "pol"]),
            BaseController("sentence", SENTENCE_BASE_FILE, "Cicero base with sentence",
                ["lat"])
        ]

        for base in self.all_bases:
            base._load_from_file()
            setattr(self, base.name, base)

    def __str__(self):
        return "Cicero base controller. Now control {} bases".format(len(self.all_bases))

    def __repr__(self):
        return "<{}>".format(self.__class__.__name__)

    def random_noun(self, declination=1):
        """
        Find random noun with proper declination

        :param declination: <int> number of conjugation for verb default *1*
        :return: <int> number of *row* 
        """
        if declination not in [1, 2, 3, 4, 5]:
            raise AttributeError("Declination must be number in range from 1 to 5")
        return self._chose_random(type_of_speech="N", variety=declination)

    def random_adjective(self, declination=1):
        """
        Find random adjective with proper declination

        :param declination: <int> number of conjugation for verb default *1*
        :return: <int> number of *row* 
        """
        if declination not in [1, 2, 3]:
            raise AttributeError("Declination must be number in ranger from 1 to 3")
        return self._chose_random(type_of_speech="ADJ", variety=declination)

    def random_verb(self, conjugation=1):
        """
        Find random verb with proper conjugation

        :param conjugation: <int> number of conjugation for verb default *1*
        :return: <int> number of *row* 
        """
        if conjugation not in [1, 2, 3, 4]:
            raise AttributeError("Conjugation must be number in range from 1 to 4")
        return self._chose_random(type_of_speech="V", variety=1)

    def search(self, search_word, max_return=3, tag=None, pattern=None):
        """
        Search ``search_word`` phrase on ``base``. If fined more than one match
        row return table with ``max_return`` rows. Row is number of line in
        ``base``

        :param search_word: <str> search phrase
        :param max_return: <int> max size return list
        :param base: <BaseController> object, default is *Laitn* base. 
        :param pattern: kind of searching patter, can by *simple* or *exact*
            if *simple* check only first letters but if *exact* check whole word
        :return: [<int>] example: [1243, 3231, 5342]
        """

        if not tag:
            tag = self.words._attr_names[0]

        if not pattern:
            pattern = 'simple'

        pattern = re.compile(self.patterns.get(pattern).format(search_word))
        result = []
        for row in range(1, self.words.size):
            latin_word = self.words.elist[row].get(tag)
            if pattern.search(latin_word):
                if len(result) < max_return:
                    result.append(row)
        return result

    def search_english(self, search_word, max_return=3):
        return self.search(search_word=search_word,
                           max_return=max_return,
                           tag="eng")

    def search_latin(self, search_word, max_return=3):
        return self.search(search_word=search_word,
                           max_return=max_return,
                           teg="lat")

    def _chose_random(self, type_of_speech="N", variety=1, query_limit=MAX_CHOICE_NUMBER):
        """

        :param type_of_speech: Symbol of name of speech, N is noun, V is verb. etc.
        :param variety: number of declination for nouns and adjectives and
                        number of conjugation for verbs
        :param query_limit: number of try of search proper word default 100
        :return: <int> number of the row in Latin base 
        """

        if not self.all_bases:
            raise RuntimeError("Base controller haven't any base")

        for number_of_try in range(1, query_limit):
            try:
                extra_condition = True
                row = random.randint(1, self.words.size)
                lat = self.words.elist[row].get("lat")
                info = self.words.elist[row].get("info")[1:-1].split(",")
                if type_of_speech == "N":
                    extra_condition = (info[1] == str(variety))
                elif type_of_speech == "V":
                    extra_condition = (info[1] == str(variety)) and (len(info) > 2)
                elif type_of_speech == "ADJ":
                    extra_condition = True

                if info[0] == type_of_speech and extra_condition:
                    #print ("OK, row: {}, lat: {}, info: {}".format(row, lat, info))
                    #print ("Times of selecting: {}".format(number_of_try))
                    ret = self.words.resolve_row(row, grammatic_forms=True)
            except DeclinationError as e:
                logger.warn("Decliation error: {e}".format(e))

        print (type(ret))
        return ret
