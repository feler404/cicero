# coding: UTF-8
# log_in.py

UIOption='Opcje'
UIScreen='Ekran'
UITeach='Nauka'
UIBase='Baza'
UIOther='Inne'
UISave='Zapisz'
UIBack='Cofnij'
UIMark='Ocena'
UIMarkP='Ocena[%]'
UINr='Nr.'
UITS='T[s]'
UISEC='[sek]'
UILesson='Lekcja'
UIRepeat='Powtórka'
UIStart='Start'
UIName='Imię'
UIHelp='Pomoc'
UIExit='Wyjście'
UITeachGrammar='Gramatyka'
UITeachWords='Słówka'
UIWelcome='Witaj '
UIInCicero='\n w Cicero'
UISentence='Sentencje'
UIGraphics='Grafiki'
UIFullScreen='Pełny ekran'
UIMaximize='Maksymalizacja'
UIGrammarNOQ='Gramatyka: liczba pytań'
UIWordsTL='Słówka: język nauki'
UIEnglishLatin='English-Latin'
UIPolishLatin='Polsko-Łaciński'
UIBaseContest='Zawartość bazy'
UILatinBase='Baza łacińska: '
UIEnglishBase='Baza angielska: '
UIUserFiles='Plik użytkownika: '
UICounterPassowers='Licznik haseł: '
UISearchingTime='Czas szukania: '
UIStatsFilePath='Ścieżka pliku statytstyk: '
UILearningLessons='Lekcje nauki'
UILearningLesson='Lekcja nauki'
UISuggestlesson='Lekcja sugerowana'
UIGeneralStats='Statystyki ogólne'
UINumberRepeats='Liczba \npowtórek: \n0\t\t\t\t\tq'
UITimeLearning='Czas \nnauki: \n0 [sek]\t\t\t\tq'
UIGeneralLearningTime='Ogólny czas nauki: '
UIGeneralNumberRepeats='Ogólna liczba powtórek: '
UINumberRepeatsN='Liczba \npowtórek: \n'
UITimeLearningN='Czas \nnauki: \n'
UINumberRepeatsT='Liczba \npowtórek: \n0\t\t\t\t\tq'
UITimeLearningT='Czas \nnauki: \n0 [sek]\t\t\t\tq'
UILesNr='Lek.  \nNr.:'
UILessonName='Lekcja'
UIDone='Zrob.'
UITime='Czas'
UIStats='Stat. '
UIYes='Tak'
UINo='Nie'
UIDone20='Zrobione\n0/20'
UIStatistics='Statystyki'
UIStats0='Stat \n0/0'
UITime0='Czas \n0/0'
UITime1='Czas \n1/1'
UIGood='Dobrze !!!'
UISorryBadAnswer='Zła odpowiedź.\nOdpowiedź poprawna:'
UIDoneQuestions='Zrobione: '
UIAllQuestions='Wszystkich pytań: '
UINumberRightAnswers='Liczba poprawnych odpowiedzi: '
UIPercentageRightAnswers='Procent poprawnych odpowiedzi: '
UITimeRequestLastQuestion='Czas odpowiedzi na ostatnie pytanie: '
UIMeanTimeRequest='Łączny czas odpowiedzi: '
UIGramaticLessonNumber='Gramatyka, numer lekcji: '
UIWordsLessonNumber='Słówka, numer lekcji: '
UIAuthorizationUser='Uwierzytelnianie użytkownika'
UILoginPanel='Panel logowania'
UIAddUser='Dodaj użytkownika'
UIChoose='Wybierz'
UISelectUser='Wybierz użytkownika'
UINew='Nowy'
UIAddNewUser='Dodaj nowego użytkownika'
UIDelete='Usuń'
UIDeleteUser='Usuń użytkownika'
UIUsers='Użytkownicy'
