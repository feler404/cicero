# -*- coding: utf-8 -*-
#
# library for: Cicero Open Project

# GLOBAL VARIABLES

fullScreen = 0
maximize = 0
titleMenuWindow = """Menu"""
titleTeachWindow = """Teach"""
sizeWindow = [1024, 800]
longTeach = 5
langTeach = 1
longTeachTabel = [2, 5, 10, 15, 20]
userName = ""
sugestLessonConstant = 20
relaxGraphics = 20
relaxSentece = 30
relaxPicFreq = 30
relaxSenFreq = 50
backPic = 0


xml_file_lat_name = "./data/xml/latin.xml"
xml_file_eng_name = "./data/xml/english.xml"
xml_file_word_teach_base = "./data/xml/word.teach.base.xml"
xml_file_sentence_base = "./data/xml/sentence.base.xml"
usersTabelFile = "./data/config/users.txt"
configTabelFile = "./data/config/config.txt"
configUILangFile = "./data/config/UILang.txt"
statsFilePath = "./data/config/"
limit = 39223
timeSearchQuestion = 0.1

mainWindow = None
teachLesson = None
teachMaterial = "words"  # rodzaj trybu nauki slowka, gramatyka
tabelQuest = []  # =range(0,20)#tabela pytan slowek powoli usowane elementy
temporaryTabelQuest = []  # =range(0,20)#tabela pytan slowek elementy nie sa usowane
numberQuest = None  # aktualny numer zapytanie z bazy number wiersza
statsTeachWindow = [[], [], []]
tempFalg = False

###[numberQuestions,numberAnswers,numberGoodAnswers,percentGoodAnswers,meanTime,allTime]

backgroundPic = ["./data/pic/0backgroundB.jpg", "./data/pic/1backgroundB.jpg", "./data/pic/2backgroundG.jpg",
                 "./data/pic/3backgroundG.jpg", "./data/pic/4backgroundB.jpg", "./data/pic/5backgroundY.jpg",
                 "./data/pic/6backgroundB.jpg", "./data/pic/7backgroundG.jpg", "./data/pic/8backgroundB.jpg",
                 "./data/pic/8backgroundB.jpg"]

# #6aac27-G    #83a6d8-B     #bda730-Y
backColour = ["#83a6d8", "#83a6d8", "#6aac27", "#6aac27", "#83a6d8", "#bda730",
              "#83a6d8", "#6aac27", "#83a6d8", "#83a6d8"]

romePic = ["./data/pic/Rome/1.jpg", "./data/pic/Rome/2.jpg", "./data/pic/Rome/3.jpg", "./data/pic/Rome/4.jpg",
           "./data/pic/Rome/5.jpg", "./data/pic/Rome/6.jpg", "./data/pic/Rome/7.jpg", "./data/pic/Rome/8.jpg",
           "./data/pic/Rome/9.jpg", "./data/pic/Rome/10.jpg", "./data/pic/Rome/11.jpg", "./data/pic/Rome/12.jpg"]

temps_tab = ["Praesentis", "Imperfecti", "Futuri I", "Perfecti", "Plusquam-perfecti", "Futuri exacti"]
modus_tab = ["Indicativus", "Coniunctivus", "Imperativus", "Participium", "Infinitivus"]

v_lessons_name = ["Nomen substantivum: first declination(ancilla, ancillae)",
                  "Nomen substantivum: second declination(dominus, domini)",
                  "Nomen substantivum: third declination(imperator, imperatoris)",
                  "Nomen substantivum: fourth declination(exercitus, exercitus)",
                  "Nomen substantivum: fifth declination(res, rei)",

                  "Nomen adiectivum: first declination(alta)",
                  "Nomen adiectivum: second declination(altus, altum)",
                  "Nomen adiectivum: third declination(celer, celeris, celere)",

                  "Verbum: coniugation I: Praesentis, Activi",
                  "Verbum: coniugation I: Praesentis, Passivi",
                  "Verbum: coniugation I: Imperfecti, Activi",
                  "Verbum: coniugation I: Imperfecti, Passivi",
                  "Verbum: coniugation I: Futuri I, Activi",
                  "Verbum: coniugation I: Futuri I, Passivi",
                  "Verbum: coniugation I: Perfecti, Activi",
                  "Verbum: coniugation I: Perfecti, Passivi",
                  "Verbum: coniugation I: Plusquam-perfecti, Activi",
                  "Verbum: coniugation I: Plusquam-perfecti, Passivi",
                  "Verbum: coniugation I: Futuri exacti, Activi",
                  "Verbum: coniugation I: Futuri exacti, Passivi",
                
                  "Verbum: coniugation II: Praesentis, Activi",
                  "Verbum: coniugation II: Praesentis, Passivi",
                  "Verbum: coniugation II: Imperfecti, Activi",
                  "Verbum: coniugation II: Imperfecti, Passivi",
                  "Verbum: coniugation II: Futuri I, Activi",
                  "Verbum: coniugation II: Futuri I, Passivi",
                  "Verbum: coniugation II: Perfecti, Activi",
                  "Verbum: coniugation II: Perfecti, Passivi",
                  "Verbum: coniugation II: Plusquam-perfecti, Activi",
                  "Verbum: coniugation II: Plusquam-perfecti, Passivi",
                  "Verbum: coniugation II: Futuri exacti, Activi",
                  "Verbum: coniugation II: Futuri exacti, Passivi",
                
                  "Verbum: coniugation III: Praesentis, Activi",
                  "Verbum: coniugation III: Praesentis, Passivi",
                  "Verbum: coniugation III: Imperfecti, Activi",
                  "Verbum: coniugation III: Imperfecti, Passivi",
                  "Verbum: coniugation III: Futuri I, Activi",
                  "Verbum: coniugation III: Futuri I, Passivi",
                  "Verbum: coniugation III: Perfecti, Activi",
                  "Verbum: coniugation III: Perfecti, Passivi",
                  "Verbum: coniugation III: Plusquam-perfecti, Activi",
                  "Verbum: coniugation III: Plusquam-perfecti, Passivi",
                  "Verbum: coniugation III: Futuri exacti, Activi",
                  "Verbum: coniugation III: Futuri exacti, Passivi",
                
                  "Verbum: coniugation IV: Praesentis, Activi",
                  "Verbum: coniugation IV: Praesentis, Passivi",
                  "Verbum: coniugation IV: Imperfecti, Activi",
                  "Verbum: coniugation IV: Imperfecti, Passivi",
                  "Verbum: coniugation IV: Futuri I, Activi",
                  "Verbum: coniugation IV: Futuri I, Passivi",
                  "Verbum: coniugation IV: Perfecti, Activi",
                  "Verbum: coniugation IV: Perfecti, Passivi",
                  "Verbum: coniugation IV: Plusquam-perfecti, Activi",
                  "Verbum: coniugation IV: Plusquam-perfecti, Passivi",
                  "Verbum: coniugation IV: Futuri exacti, Activi",
                  "Verbum: coniugation IV: Futuri exacti, Passivi",
                ]

v_lessons = [
            ["N", "1", "0", "0", "0"], ["N", "2", "0", "0", "0"], ["N", "3", "0", "0", "0"],
            ["N", "4", "0", "0", "0"], ["N", "5", "0", "0", "0"], ["ADJ", "1", "0", "0", "0"],
            ["ADJ", "2", "0", "0", "0"], ["ADJ", "3", "0", "0", "0"],

            ["V", "1", "Praesentis", "Activi"], ["V", "1", "Praesentis", "Passivi"],
            ["V", "1", "Imperfecti", "Activi"], ["V", "1", "Imperfecti", "Passivi"],
            ["V", "1", "Futuri I", "Activi"], ["V", "1", "Futuri I", "Passivi"],
            ["V", "1", "Perfecti", "Activi"], ["V", "1", "Perfecti", "Passivi"],
            ["V", "1", "Plusquam-perfecti", "Activi"], ["V", "1", "Plusquam-perfecti", "Passivi"],
            ["V", "1", "Futuri exacti", "Activi"], ["V", "1", "Futuri exacti", "Passivi"],
            
            ["V", "2", "Praesentis", "Activi"], ["V", "2", "Praesentis", "Passivi"],
            ["V", "2", "Imperfecti", "Activi"], ["V", "2", "Imperfecti", "Passivi"],
            ["V", "2", "Futuri I", "Activi"], ["V", "2", "Futuri I", "Passivi"],
            ["V", "2", "Perfecti", "Activi"], ["V", "2", "Perfecti", "Passivi"],
            ["V", "2", "Plusquam-perfecti", "Activi"], ["V", "2", "Plusquam-perfecti", "Passivi"],
            ["V", "2", "Futuri exacti", "Activi"], ["V", "2", "Futuri exacti", "Passivi"],

            ["V", "2", "Praesentis", "Activi"], ["V", "1", "Praesentis", "Passivi"],
            ["V", "2", "Imperfecti", "Activi"], ["V", "1", "Imperfecti", "Passivi"],
            ["V", "2", "Futuri I", "Activi"], ["V", "1", "Futuri I", "Passivi"],
            ["V", "2", "Perfecti", "Activi"], ["V", "1", "Perfecti", "Passivi"],
            ["V", "2", "Plusquam-perfecti", "Activi"], ["V", "1", "Plusquam-perfecti", "Passivi"],
            ["V", "2", "Futuri exacti", "Activi"], ["V", "1", "Futuri exacti", "Passivi"],
            
            ["V", "2", "Praesentis", "Activi"], ["V", "2", "Praesentis", "Passivi"],
            ["V", "2", "Imperfecti", "Activi"], ["V", "2", "Imperfecti", "Passivi"],
            ["V", "2", "Futuri I", "Activi"], ["V", "2", "Futuri I", "Passivi"],
            ["V", "2", "Perfecti", "Activi"], ["V", "2", "Perfecti", "Passivi"],
            ["V", "2", "Plusquam-perfecti", "Activi"], ["V", "2", "Plusquam-perfecti", "Passivi"],
            ["V", "2", "Futuri exacti", "Activi"], ["V", "2", "Futuri exacti", "Passivi"]

            ]

v_lessons_name_1 = [
                "Verba: I - (ancilla, rosa, viola)",  # 20
                "Verba: II - (schola, puer, pater)",  # 40
                "Verba: III - (orno, clarus, specto)",  # 60
                "Verba: IV - (historia, ager, oppidum)",  # 80
                "Verba: V - (altus, visito, possessor)",  # 100
                "Verba: VI - (alvus, dialectus, paragraphus)",  # 120
                "Verba: VII - (populus, fluvius, regina)",  # 140
                "Verba: VIII - (paro, erro, bene)",  # 160
                "Verba: IX - (sedulus, discipulus, libenter)",  # 180
                "Verba: X - (navigium, fatigatus, securus)",  # 200
                "Verba: XI - (verbum, doceo, traho)",  # 220
                "Verba: XII - (filia, unus, frumentum)",  # 240
                "Verba: XIII - (cupio, hora, annus)",  # 260
                "Verba: XIV - (asporto, vinco, victor)",  # 280
                "Verba: XV - (cibus, munio, fossa)",  # 300
                "Verba: XVI - (cerno, signum, telum)",  # 320
                "Verba: XVII - (lex, auctor, disputo)",  # 340
                "Verba: XVIII - (orator, cognomen, interrogo)",  # 360
                "Verba: XIX - (honor, mos, amitto)",  # 380
                "Verba: XX - (praetor, onus, impono)",  # 400
                "Verba: XXI - (provincia, vectigal, aetas)",  # 420
                "Verba: XXII - (sors, exsilium, diruo)",  # 440
                "Verba: XXIII - (novus, leo, vulpecula)",  # 460
                "Verba: XXIV - (imperium, castigatio, error)",  # 480
                "Verba: XXV - (celer, virtus, dico)",  # 500
                "Verba: XXVI - (corvus, folium, invidia)",  # 520
                "Verba: XXVII - (retineo, sol, rogo)",  # 540
                "Verba: XXVIII - (impero, dulce, nocens)",  # 560
                "Verba: XXIX - (venio, sedeo, mors)",  # 580
                "Verba: XXX - (mille, longe, pugna)",  # 600
                "Verba: XXXI - (centurio, succurro, paulum)",  # 620
                "Verba: XXXII - (sero, fors, respondeo)",  # 640
                "Verba: XXXIII - (umbra, nemo, flumen)",  # 660
                "Verba: XXXIV - (sino, ius, victus)",  # 680
                "Verba: XXXV - (mundus, uxor, condio)",  # 700
                "Verba: XXXVI - (silva, ignis, mercator)",  # 720
                "Verba: XXXVII - (auditus, tango, linea)",  # 740
                "Verba: XXXVIII - (sono, tuba, reditus)",  # 760
                "Verba: XXXIX - (corona, laetus, administro)",  # 780
                "Verba: XL - (proelium, acerus, copia)",  # 800
                "Verba: XLI - (frenus, avarus, numero)",  # 820
                "Verba: XLII - (mulus, potus, hospito)",  # 840
                "Verba: XLIII - (discedo, intactus, inopia)",  # 860
                "Verba: XLIV - (immortalis, imperator, cito)",  # 880
                "Verba: XLV - (celer, deleo, communis)",  # 900
                "Verba: XLVI - (parco, niger, sacer)",  # 920
                "Verba: XLVII - (cedo, terminus, hasta)",  # 940
                "Verba: XLVIII - (apporto, capella, herba)",  # 960
                "Verba: XLIX - (gallus, lucus, neco)",  # 980
                # "Verba: L - (venustus, lector, natus)"  # 999
                ]

v_lessons_1 = [
            ["W", "1", "0", "0", "0"], ["W", "2", "0", "0", "0"], ["W", "3", "0", "0", "0"],
            ["W", "4", "0", "0", "0"], ["W", "5", "0", "0", "0"], ["W", "6", "0", "0", "0"],
            ["W", "7", "0", "0", "0"], ["W", "8", "0", "0", "0"], ["W", "9", "0", "0", "0"],
            ["W", "10", "0", "0", "0"], ["W", "11", "0", "0", "0"], ["W", "12", "0", "0", "0"],
            ["W", "13", "0", "0", "0"], ["W", "14", "0", "0", "0"], ["W", "15", "0", "0", "0"],
            ["W", "16", "0", "0", "0"], ["W", "17", "0", "0", "0"], ["W", "18", "0", "0", "0"],
            ["W", "19", "0", "0", "0"], ["W", "20", "0", "0", "0"], ["W", "21", "0", "0", "0"],
            ["W", "22", "0", "0", "0"], ["W", "23", "0", "0", "0"], ["W", "24", "0", "0", "0"],
            ["W", "25", "0", "0", "0"], ["W", "26", "0", "0", "0"], ["W", "27", "0", "0", "0"],
            ["W", "28", "0", "0", "0"], ["W", "29", "0", "0", "0"], ["W", "30", "0", "0", "0"],
            ["W", "31", "0", "0", "0"], ["W", "32", "0", "0", "0"], ["W", "33", "0", "0", "0"],
            ["W", "34", "0", "0", "0"], ["W", "35", "0", "0", "0"], ["W", "36", "0", "0", "0"],
            ["W", "37", "0", "0", "0"], ["W", "38", "0", "0", "0"], ["W", "39", "0", "0", "0"],
            ["W", "40", "0", "0", "0"], ["W", "41", "0", "0", "0"], ["W", "42", "0", "0", "0"],
            ["W", "43", "0", "0", "0"], ["W", "44", "0", "0", "0"], ["W", "45", "0", "0", "0"],
            ["W", "46", "0", "0", "0"], ["W", "47", "0", "0", "0"], ["W", "48", "0", "0", "0"],
            ["W", "49", "0", "0", "0"]
            ]

v_lessons_name_all = v_lessons_name + v_lessons_name_1



def def_const():
    """
    def_const()
    Zdefiniowane stałe
    """

# PART_OF_SPEECH_TYPE
    X = "X"
    N = "N"
    PRON = "PRON"
    PACK = "PACK"
    ADJ = "ADJ"
    NUM = "NUM"
    ADV = "ADV"
    V = "V"
    VPAR = "VPAR"
    SUPINE = "SUPINE"
    PREP = "PREP"
    CONJ = "CONJ"
    INTERJ = "INTERJ"
    TACKON = "TACKON"
    PREFIX = "PREFIX"
    SUFFIX = "SUFFIX"

# GENDER_TYPE
    X = "X"
    M = "M"
    F = "F"
    N = "N"
    C = "C"

# CASE_TYPE
    X = "X"
    NOM = "NOM"
    VOC = "VOC"
    GEN = "GEN"
    LOC = "LOC"
    DAT = "DAT"
    ABL = "ABL"
    ACC = "ACC"

# NUMBER_TYPE
    X = "X"
    S = "S"
    P = "P"

# COMPARISON_TYPE
    X = "X"
    POS = "POS"
    COMP = "COMP"
    SUPER = "SUPER"

# NUMERAL_SORT_TYPE
    X = "X"
    CARD = "CARD"
    ORD = "ORD"
    DIST = "DIST"
    ADVERB = "ADVERB"

# TENSE_TYPE
    X = "X"
    PRES = "PRES"
    IMPF = "IMPF"
    FUT = "FUT"
    PERF = "PERF"
    PLUP = "PLUP"
    FUTP = "FUTP"

# VOICE_TYPE
    X = "X"
    ACTIVE = "ACTIVE"
    PASSIVE = "PASSIVE"

# MOOD_TYPE
    X = "X"
    IND = "IND"
    SUB = "SUB"
    IMP = "IMP"
    INF = "INF"
    PPL = "PPL"

# NOUN_KIND_TYPE
    X = "X"
    S = "S"
    M = "M"
    A = "A"
    G = "G"
    N = "N"
    P = "P"
    T = "T"
    L = "L"
    W = "W"

# PRONOUN_KIND_TYPE
    X = "X"
    PERS = "PERS"
    REL = "REL"
    REFLEX = "REFLEX"
    DEMONS = "DEMONS"
    INTERR = "INTERR"
    INDEF = "INDEF"
    ADJECT = "ADJECT"

# VERB_KIND_TYPE
    X = "X"
    TO_BE = "TO_BE"
    TO_BEING = "TO_BEING"
    GEN = "GEN"
    DAT = "DAT"
    ABL = "ABL"
    TRANS = "TRANS"
    INTRANS = "INTRANS"
    IMPERS = "IMPERS"
    DEP = "DEP"
    SEMIDEP = "SEMIDEP"
    PERFDEF = "PERFDEF"

# NOUN_TAIL
Interface_language_tab = []

N1_rand_tab = [583, 1439, 2232, 2991, 3598, 4494, 4512, 5522, 5675,
               7549, 8724, 8970, 9744, 13152, 13741, 14327, 14547, 15206, 15508,
               16200, 16475, 17325, 18073, 18323, 18389, 18402, 18644, 19836, 20838,
               21330, 21373, 21729, 22919, 24206, 24647, 25219, 26111, 26734, 27279,
               28939, 29902, 30059, 30772, 31214, 31537, 33683, 36475, 37744, 38362, 39211]

N2_rand_tab = [59, 1226, 1497, 2323, 3189, 3235, 3631, 3811, 4794, 6072,
               6075, 6470, 6953, 7205, 7521, 7704, 8049, 9301, 10726, 10908, 12062,
               12140, 13627, 15933, 16596, 16770, 16840, 18063, 18570, 20246, 21034,
               21752, 25292, 25308, 25920, 26743, 27833, 29276, 29390, 29936, 29955,
               29996, 30724, 31738, 34116, 35550, 36698, 37556, 37712, 38813]

N3_rand_tab = [1722, 2189, 2536, 2621, 3694, 4896, 5084, 7348, 7897,
               7964, 8312, 9010, 9887, 12111, 12217, 12397, 12906, 14295, 14394, 14563,
               15673, 17024, 17723, 17882, 17922, 19191, 19390, 20778, 21095, 21717,
               22472, 23204, 23505, 24019, 24447, 24918, 26028, 27021, 27430, 27698,
               28961, 29015, 29566, 30000, 31942, 32281, 32453, 33564, 37280, 37726]

N4_rand_tab = [452, 1906, 2276, 2812, 3198, 3415, 4426, 4965, 5445, 8911,  # 14958,
               9918, 10764, 11518, 12471, 13221, 13298, 13636, 13862, 13946, 14992,
               15652, 15732, 16496, 16613, 18322, 18479, 19230, 19681, 20289, 20843,
               21293, 21721, 22865, 23122, 23316, 23824, 25953, 26063, 26076, 26587,
               26764, 27594, 29494, 30988, 32884, 33375, 34308, 35667, 37221, 38016]

N5_rand_tab = [547, 1130, 2707, 2879, 5815, 5815, 6015, 6116, 6688, 7381,
               7650, 7848, 11083, 12582, 14619, 16983, 17676, 18567, 18969, 19398,
               20173, 20600, 21506, 22690, 23738, 24354, 26059, 26529, 27160, 27160,
               27865, 29535, 30046, 30492, 30690, 31810, 31863, 31863, 33396, 34043,
               34172, 34817, 35439, 35479, 35591, 36025, 36383, 36894, 37747, 38390]

J1_rand_tab = [707, 961, 1798, 2427, 3687, 4799, 4868, 5081, 5543, 5719,
               7562, 8184, 9489, 9941, 10069, 11829, 12176, 13211, 14957, 18350, 18510,
               19526, 20147, 20685, 23163, 23574, 24700, 25643, 25807, 25828, 27599,
               28495, 28696, 29254, 29763, 30730, 30973, 31896, 32008, 33765, 33913,
               34225, 34822, 35264, 35292, 35954, 36463, 37012, 37229, 37772]

J3_rand_tab = [160, 235, 505, 1039, 1419, 4996, 5835, 6518, 7699, 7766,
               8445, 11179, 12076, 12918, 13519, 13608, 13668, 15336, 15618, 17157,
               18375, 19434, 20044, 20492, 20577, 20784, 23449, 23614, 24611, 24739,
               25037, 25535, 26957, 27591, 28054, 29940, 30036, 30773, 31606, 33179,
               33312, 34014, 34558, 35425, 35447, 35567, 36185, 37767, 38204, 39079]

V1_rand_tab = [493, 2760, 4851, 5451, 5956, 7167, 8059, 8511, 8662, 8890,
               9791, 10219, 10396, 10566, 11227, 11590, 12368, 13376, 13903, 14793,
               15630, 15730, 16585, 16693, 16728, 17516, 18661, 18727, 18777, 18923,
               20693, 21536, 22718, 23335, 24001, 24314, 25542, 26057, 26349, 26851,
               28549, 28683, 28737, 29312, 31415, 32881, 36038, 36231, 38593, 38850]

V2_rand_tab = [23, 750, 1163, 1316, 1993, 2217, 3153, 3652, 4084, 4839,
               5425, 5829, 8739, 9670, 10261, 11520, 12407, 12479, 12560, 12673,
               13167, 14406, 15693, 16891, 17151, 17639, 18334, 18717, 19642, 20059,
               21808, 24370, 25805, 26942, 27120, 27327, 27993, 29092, 31299, 31513,
               31761, 32217, 32905, 33425, 33732, 34221, 35369, 36236, 38036, 38888]

V3_rand_tab = [171, 951, 2443, 2533, 3969, 4085, 4171, 4214, 5788, 5847,
               5876, 5988, 7653, 7865, 8768, 11096, 11313, 13115, 13524, 13759, 14586,  # 8767
               15752, 17001, 17546, 19762, 21073, 21463, 22531, 23273, 23462, 25196,
               28142, 28388, 28923, 29317, 29658, 30186, 31188, 31690, 33952, 34664,
               35037, 35413, 35531, 36100, 36355, 36771, 36920, 37098, 38789]

V4_rand_tab = [234, 1185, 1692, 2903, 3591, 4905, 5378, 5535, 6016, 6128,
               6579, 6812, 7142, 7275, 7936, 8624, 9876, 10039, 10885, 11389, 11618,
               12495, 12874, 14508, 15979, 17246, 18153, 18857, 19748, 19851, 20651,
               21292, 21477, 23353, 23531, 24297, 24759, 26011, 26754, 27819, 28437,
               31077, 33074, 34016, 34696, 35912, 36179, 37227, 37498, 37927]
