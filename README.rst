===========================
Cicero v.2 - Open Latin Project
===========================
Cicero v.2 is a program for learning the Latin language, using built-in exercise.
It is easy way to practice grammar and learn many new words and their meanings.
On this moment Cicero v.2 is only console version. You may try older v.1 version
in Window's version.


===========
Install/Run
===========
On Linux/Mac-OS:
- download whole repository in zip file ``wget https://bitbucket.org/feler404/cicero/get/cicero.zip``
- extract ``unzip cicero.zip``
- lunch script ``./cicero/scripts/cicero.sh``
or
- lunch main python file ``python3 ./cicero/cicero/ccmd.py``

Old version Cicero v.1 is available on this site:
http://feler404-3d.baynow.de/cicero/cicero.download.html

=====
Usage
=====
Type ``help`` to see all posibility of options. You will ses like this:

Documented commands (type help <topic>):
---------------------------------------
english  exit  help  latin  words_teach

Where:
    english - is english -> latin dictionary type ``help english`` to more details
    latin - is latin -> english dictioanry type ``help latin`` to more details
    words_weach - is submenu to words teaching type ``help words_teach`` to
                  more details
    exit - immediatly out of program


=======================
Contribution guidelines
=======================
On first stage release project reposetori will be close for contributing. When
project reach pro-beta stage then contribuiting will be swich-on.


=======
Changes
=======
2015-05-01 - Cicero v.2 pre-Alpha v.0.1
- refactoring grammar engine
- migrate to xml.etree.ElementTree
- multilanguage support migrate to i18n first step
- add console interaface for main functionality
- add dictionary latin->english english->latin
- add latin sugest
- add tox test engine support
- refactoring settings switch to configparser


2007-05-01 - Cicero v.1
First release:
- main grammar engine
- main base of polish words
- main base of sentece
- use William Whitaker's Words
- UI engine base on GTK+


================
Coordinator info
================
For any information or questions about please contact to:
Dawid Aniol - feler404[at]tlen[dor]pl