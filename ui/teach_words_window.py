# coding: UTF-8
# teach_window.py
#
# library for: Cicero Open Project
# 

# PROGRAM CONSTATNTS
__version__='1.0.0'
__license__='GPL'
__author__='Cicero Develop Team'
__site__='www.cicero.glt.pl'

# IMPORTS
import pygtk
import gtk
import gobject
import string
import random
import time
import grammatic
import xml_base_handling
import global_direct as GD
pygtk.require('2.0')
from time import time
from xml_base_handling import xml_load_base, xml_read_line
from gtk.gdk import CONTROL_MASK, SHIFT_MASK
from gtk.gdk import keyval_name
from string import strip, split, replace




class Teach_Words_Window:


	def __init__(self, data='dichotomy',quest=None, lesson_name=None):
		self.lesson_name=lesson_name
		self.v_quest=quest
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.set_title(GD.titleTeachWindow)
		self.window.set_size_request(GD.sizeWindow[0],GD.sizeWindow[1])
		self.window.connect("destroy", lambda wid: gtk.main_quit())
		self.window.connect("delete_event", lambda a1,a2:gtk.main_quit())
		self.window.set_icon_from_file("./pic/log_in.png")
		self.window.set_decorated(gtk.FALSE)
		self.window.connect('key-press-event', self.key_press_event_cb)
		if GD.maximize: self.window.maximize()
		if GD.fullScreen: self.window.fullscreen()
		self.tooltips = gtk.Tooltips()
		self.tooltips.set_delay(1000)
		
		self.i_wallpaper = gtk.Image()
		GD.backPic=random.randint(0,len(GD.backgroundPic)-1)
		self.i_wallpaper.set_from_file(GD.backgroundPic[GD.backPic])
		child = gtk.EventBox()
		child.add(self.i_wallpaper)
		
		#self.show_teach_staff(data='choose')
		self.answer=None
		self.v_done1=0
		self.v_done2=GD.longTeach
		self.v_stats1=0
		self.v_stats2=0
		self.v_time_start=time()
		self.v_time1=0
		self.v_time2=0
		self.v_questions_number=0
		self.v_noun_tab=['Nominativus Singularis','Genetivus Singularis','Dativus Singularis',
						'Accusativus Singularis','Ablativus Singularis','Vocativus Singularis']
		self.v_noun_tab=self.v_noun_tab*2
		self.v_adjective_tab=['Nominativus Singularis','Genetivus Singularis','Dativus Singularis',
						'Accusativus Singularis','Ablativus Singularis','Vocativus Singularis']
		self.v_adjective_tab=self.v_adjective_tab*6
		self.v_verb_tab=['1-st Singularis', '2-st Singularis', '3-st Singularis',
						'1-st Pluralis', '2-st Pluralis', '3-st Pluralis']
		self.v_verb_tab=self.v_verb_tab*12

		self.surface=gtk.TextView()
		self.surface.show()
		self.surface.set_editable(False)
		self.show_panel_stats(self)

		self.v_question_type=data

		if self.v_question_type=='choose':self.show_panel_teach_choos(self)
		if self.v_question_type=='write':self.show_panel_teach_write(self)	
		if self.v_question_type=='dichotomy':self.show_panel_teach_dichotomy(self)
				
		self.new_question(self)


		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 0, 0)
		if self.v_question_type=='write':self.e_answer.grab_focus()
		self.surface.show_all()
		self.window.add(self.surface)
		self.window.show()
		
	def show_panel_teach_dichotomy(self, data=None):
		
		question = gtk.EventBox()
		question.show()
		self.l_question = gtk.Label("")
		self.l_question.set_markup("<big><b></b></big>");
		self.l_question.set_line_wrap(True)
		question.add(self.l_question)
		self.l_question.show()
		question.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(question)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,50)
		
		answer1 = gtk.EventBox()
		answer1.show()
		self.label1 = gtk.Label("---")
		answer1.add(self.label1)
		self.label1.show()
		answer1.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer1.connect("button_press_event", self.check_answer, 1)
		answer1.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer1)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,150)

		answer2 = gtk.EventBox()
		answer2.show()
		self.label2 = gtk.Label("---")
		answer2.add(self.label2)
		self.label2.show()
		answer2.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer2.connect("button_press_event", self.check_answer, 0)
		answer2.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer2)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,200)
		
	def show_panel_teach_write(self, data=None):
		question = gtk.EventBox()
		question.show()
		self.l_question = gtk.Label("")
		self.l_question.set_markup("<big><b></b></big>");
		self.l_question.set_line_wrap(True)
		question.add(self.l_question)
		self.l_question.show()
		question.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(question)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,50)
		
		self.e_answer = gtk.Entry()
		self.e_answer.show()
		self.e_answer.set_max_length(80)
		self.e_answer.select_region(0,0)
		self.e_answer.connect("activate", self.check_answer, self.e_answer)
		child = gtk.EventBox()
		child.add(self.e_answer)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,200)	
		
	def show_panel_teach_choos(self, data=None):
		
		question = gtk.EventBox()
		question.show()
		self.l_question = gtk.Label("")
		self.l_question.set_markup("<big><b></b></big>");
		self.l_question.set_line_wrap(True)
		question.add(self.l_question)
		self.l_question.show()
		question.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(question)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,50)
		
		answer1 = gtk.EventBox()
		answer1.show()
		self.label1 = gtk.Label("---")
		answer1.add(self.label1)
		self.label1.show()
		answer1.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer1.connect("button_press_event", self.check_answer, 0)
		answer1.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer1)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,150)

		answer2 = gtk.EventBox()
		answer2.show()
		self.label2 = gtk.Label("---")
		answer2.add(self.label2)
		self.label2.show()
		answer2.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer2.connect("button_press_event", self.check_answer, 1)
		answer2.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer2)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,200)
		
		answer3 = gtk.EventBox()
		answer3.show()
		self.label3 = gtk.Label("---")
		answer3.add(self.label3)
		self.label3.show()
		answer3.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer3.connect("button_press_event", self.check_answer, 2)
		answer3.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer3)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,250)
		
		answer4 = gtk.EventBox()
		answer4.show()
		self.label4 = gtk.Label("---")
		answer4.add(self.label4)
		self.label4.show()
		answer4.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer4.connect("button_press_event", self.check_answer,3)
		answer4.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer4)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,300)
		
		self.i_arrow1 = gtk.Image()
		self.i_arrow1.set_from_file('./pic/arrow.png')
		child = gtk.EventBox()
		child.add(self.i_arrow1)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 80, 156)
		
		self.i_arrow2 = gtk.Image()
		self.i_arrow2.set_from_file('./pic/arrow.png')
		child = gtk.EventBox()
		child.add(self.i_arrow2)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 80, 206)
		
		self.i_arrow3 = gtk.Image()
		self.i_arrow3.set_from_file('./pic/arrow.png')
		child = gtk.EventBox()
		child.add(self.i_arrow3)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 80, 256)
		
		self.i_arrow4 = gtk.Image()
		self.i_arrow4.set_from_file('./pic/arrow.png')
		child = gtk.EventBox()
		child.add(self.i_arrow4)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 80, 306)
		
	def show_panel_stats(self, data=None):

		self.b_help = gtk.Button()
		self.b_help.connect("clicked", self.dialog_help)
		self.b_help.set_relief(gtk.RELIEF_HALF)
		image = gtk.Image()
		image.set_from_file("./pic/help3.png")
		self.b_help.set_image(image)
		self.tooltips.set_tip(self.b_help, "Help")
		self.b_help.show()
		self.b_help.enter()
		child = gtk.EventBox()
		child.add(self.b_help)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 900,50)
		
		self.b_exit = gtk.Button()
		self.b_exit.connect("clicked", self.exit)
		self.b_exit.set_relief(gtk.RELIEF_HALF)
		image = gtk.Image()
		image.set_from_file("./pic/exit3.png")
		self.b_exit.set_image(image)
		self.tooltips.set_tip(self.b_exit, "Exit")
		self.b_exit.show()
		self.b_exit.enter()
		child = gtk.EventBox()
		child.add(self.b_exit)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 940,50)
		
		
		self.b_done = gtk.Button()
		self.b_done.connect("clicked", self.dialog_done)
		self.b_done.set_relief(gtk.RELIEF_HALF)
		self.i_done = gtk.Image()
		self.i_done.set_from_file("./pic/done.png")
		self.b_done.set_label("Done\n0/20")
		self.b_done.set_image(self.i_done)
		self.tooltips.set_tip(self.b_done, "Done")
		self.b_done.show()
		self.b_done.enter()
		child = gtk.EventBox()
		child.add(self.b_done)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 900,260)
		
		self.b_stats = gtk.Button()
		self.b_stats.connect("clicked", self.dialog_stats)
		self.b_stats.set_relief(gtk.RELIEF_HALF)
		self.i_stats = gtk.Image()
		self.i_stats.set_from_file("./pic/stats.png")
		self.b_stats.set_label("Stats\n0/0")
		self.b_stats.set_image(self.i_stats)
		self.tooltips.set_tip(self.b_stats, "Statistics")
		self.b_stats.show()
		self.b_stats.enter()
		child = gtk.EventBox()
		child.add(self.b_stats)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 900,310)
		
		self.b_time = gtk.Button('')
		self.b_time.connect("clicked", self.dialog_time)
		self.b_time.set_relief(gtk.RELIEF_HALF)
		self.i_time = gtk.Image()
		self.i_time.set_from_file("./pic/time.png")
		self.b_time.set_label("Time \n0/0")
		self.b_time.set_image(self.i_time)
		self.tooltips.set_tip(self.b_time, "Time")
		self.b_time.show()
		self.b_time.enter()
		child = gtk.EventBox()
		child.add(self.b_time)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 900,360)
		
		
	def time(self, data=None):
		self.b_time.set_label("Time \n1/1")

	def exit(self, data=None, off=None):
		self.window.hide()
		GD.mainWindow.window.show()
		

	def check_answer(self, data=None, off=None, on=None):
		#Przechwytywanie odpowiedzi lesji nie jest w parametrze
		if self.v_question_type=='write' and self.answer!=None:
			on=self.e_answer.get_text()
		#if self.v_question_type=='choose' and self.answer!=None:
		#	on=on
		#Odswierzanie statystyk
		if self.answer!=None:
			if self.answer==str(on):
				self.v_done1+=1
				self.v_stats1+=1
				self.v_stats2=self.v_stats1*100/self.v_done1
				self.v_time2=int(time()-self.v_time_start)
				self.v_time1=int(self.v_time2/self.v_done1)
				self.dialog_answer("Good !!!")
			else:
				self.v_done1+=1
				self.v_stats2=self.v_stats1*100/self.v_done1
				self.v_time2=int(time()-self.v_time_start)
				self.v_time1=int(self.v_time2/self.v_done1)
				self.dialog_answer('Sorry, bad answer.\nRight answer: <b>'+self.v_generation_ret[6]+'</b>')

	def new_question(self, data=None, off=None, on=None):
		self.b_done.set_label ("Done\n"+str(self.v_done1)+'/'+str(self.v_done2))
		#self.b_stats.set_label("Stats\n"+str(self.v_stats1)+'/'+str(self.v_stats2))
		self.b_stats.set_label("Stats\n"+str(self.v_stats2)+'%')
		self.b_time.set_label ("Time \n"+str(self.v_time1)+'/'+str(self.v_time2))

		#data=[pytanie,od1,od2,od3,od4,odp]
		#Ustawianie labeli na pytanie
		if self.v_question_type=='choose':
			data=self.generation(question_type='choose')
			self.l_question.set_markup('<big><b>'+data[0]+'</b></big>\n<small>'+data[7]+' - '+data[8]+'</small>');
			self.label1.set_label(data[1])
			self.label2.set_label(data[2])
			self.label3.set_label(data[3])
			self.label4.set_label(data[4])
			self.answer=data[5]
		if self.v_question_type=='write':
			data=self.generation('write')
			self.answer=data[5]
			self.l_question.set_markup('<big><b>'+data[0]+'</b></big>\n<small>'+data[7]+' - '+data[8]+'</small>');
			self.e_answer.set_text('')
		if self.v_question_type=='dichotomy':
			data=self.generation('dichotomy')
			self.answer=data[5]
			self.l_question.set_markup('<big><b>'+data[0]+'</b></big>\n<small>'+data[7]+' - '+data[8]+'</small>');
			self.label1.set_markup('<b>Yes</b>')
			self.label2.set_markup('<b>No</b>')
		#licza pytan
		if self.v_questions_number==GD.longTeach and self.v_question_type=='dichotomy':
			GD.statsTeachWindow[0]=[self.v_done1,self.v_done2,self.v_stats1,self.v_stats2,self.v_time1,self.v_time2]
			self.window.hide()
			self.__init__(data='choose',quest=self.v_quest, lesson_name=self.lesson_name)
		elif self.v_questions_number==GD.longTeach and self.v_question_type=='choose':
			GD.statsTeachWindow[1]=[self.v_done1,self.v_done2,self.v_stats1,self.v_stats2,self.v_time1,self.v_time2]
			self.window.hide()
			self.__init__(data='write',quest=self.v_quest, lesson_name=self.lesson_name)
		elif self.v_questions_number==GD.longTeach and self.v_question_type=='write':
			GD.statsTeachWindow[2]=[self.v_done1,self.v_done2,self.v_stats1,self.v_stats2,self.v_time1,self.v_time2]
			saveStats(GD.statsTeachWindow)
			GD.mainWindow.refreshTabel()
			self.exit()
			
			#self.window.destroy()
			#self.__init__(data='dichotomy',quest=self.v_quest, lesson_name=self.lesson_name)
		else:
			self.v_questions_number+=1
			
	def key_press_event_cb(self, widget, event, data=None):
		
		#if event.state & CONTROL_MASK and event.state & SHIFT_MASK:
		if self.v_question_type=='choose':
			if event.keyval == gtk.keysyms._1:
				self.check_answer(on=0)
			if event.keyval == gtk.keysyms._2:
				self.check_answer(on=1)
			if event.keyval == gtk.keysyms._3:
				self.check_answer(on=2)
			if event.keyval == gtk.keysyms._4:
				self.check_answer(on=3)
		if self.v_question_type=='dichotomy':
			if event.keyval == gtk.keysyms._1:
				self.check_answer(on=1)
			if event.keyval == gtk.keysyms._2:
				self.check_answer(on=0)


	def generation(self,question_type=None):
		#self.v_quest=['V','1','Presentis','Indicativus','Activi']
		#self.v_generation_ret=[que=question,ans1,ans2,ans3,ans4,ansRight...
		if self.v_quest[0]=='N':
			self.v_gram_ret=grammatic.noun_declination(number=grammatic.random_question(part='N',declination=int(self.v_quest[1])))
			self.v_quest_tab=self.v_gram_ret[0]
			self.v_number_line=self.v_gram_ret[1]
			self.v_number_inflection=self.v_gram_ret[2]
			self.v_lat_contest=(xml_read_line(file_name=GD.xml_file_lat_name ,contest='shank',number=self.v_gram_ret[1]))
			self.v_eng_contest=(xml_read_line(file_name=GD.xml_file_eng_name ,contest='shank',number=self.v_gram_ret[1]))
			if self.v_quest[0]=='N':
				if question_type=='choose':
					ans_1=(random.randint(0,11))#odp1
					go_search=1
					while go_search:
						ans_2=(random.randint(0,11))#odp2
						go_search+=1
						if go_search==100:go_search=0
						if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_2]:go_search=0
					go_search=1
					while go_search:
						ans_3=(random.randint(0,11))#odp2
						go_search+=1
						if go_search==100:go_search=0
						if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_3]:
							if self.v_quest_tab[ans_2]!=self.v_quest_tab[ans_3]:go_search=0
					go_search=1
					while go_search:
						ans_4=(random.randint(0,11))#odp2
						go_search+=1
						if go_search==100:go_search=0
						if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_4]:
							if self.v_quest_tab[ans_2]!=self.v_quest_tab[ans_4]:
								if self.v_quest_tab[ans_3]!=self.v_quest_tab[ans_4]:go_search=0
					ans_r=(random.randint(0,3))#pytanie
					que=[ans_1,ans_2,ans_3,ans_4][ans_r]#odp
					self.v_generation_ret=([self.v_noun_tab[que]+' '+self.v_quest_tab[0]+', '+self.v_quest_tab[1] ,self.v_quest_tab[ans_1],self.v_quest_tab[ans_2],self.v_quest_tab[ans_3],self.v_quest_tab[ans_4],str(ans_r),self.v_quest_tab[que],self.v_lat_contest,self.v_eng_contest])
				if question_type=='write':
					que=ans_r=(random.randint(0,11))#pytanie#odp
					self.v_generation_ret=([self.v_noun_tab[que]+' '+self.v_quest_tab[0]+', '+self.v_quest_tab[1] ,'','','','',self.v_quest_tab[ans_r],self.v_quest_tab[ans_r],self.v_lat_contest,self.v_eng_contest])
				if question_type=='dichotomy':
					que=(random.randint(0,11))#pytanie#odp
					ans_1=(random.randint(0,11))
					if self.v_quest_tab[ans_1]==self.v_quest_tab[que]:
						ans_r=1
						ans_rt='Yes'
					else:
						ans_r=0
						ans_rt='No'
					self.v_generation_ret=([self.v_noun_tab[que]+': '+self.v_quest_tab[ans_1],'','','','',str(ans_r),ans_rt,self.v_lat_contest,self.v_eng_contest])

		if self.v_quest[0]=='ADJ':
			self.v_adj_dec=self.v_quest[1]
			self.v_gram_ret=grammatic.adjective_declination(number=grammatic.random_question(part='ADJ',declination=int(self.v_quest[1])))
			self.v_quest_tab=self.v_gram_ret[0]
			self.v_number_line=self.v_gram_ret[1]
			self.v_number_inflection=self.v_gram_ret[2]
			self.v_lat_contest=(xml_read_line(file_name=GD.xml_file_lat_name ,contest='shank',number=self.v_gram_ret[1]))
			self.v_eng_contest=(xml_read_line(file_name=GD.xml_file_eng_name ,contest='shank',number=self.v_gram_ret[1]))
			if self.v_adj_dec==str(1):v_rnd=[12,23]
			if self.v_adj_dec==str(2):v_rnd=[[0,11],[24,35]][random.randint(0,1)]
			if self.v_adj_dec==str(3):v_rnd=[0,35]

			if question_type=='choose':
				ans_1=(random.randint(v_rnd[0],v_rnd[1]))#odp1
				go_search=1
				while go_search:
					ans_2=(random.randint(v_rnd[0],v_rnd[1]))#odp2
					go_search+=1
					if go_search==100:go_search=0
					if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_2]:go_search=0
				go_search=1
				while go_search:
					ans_3=(random.randint(v_rnd[0],v_rnd[1]))#odp2
					go_search+=1
					if go_search==100:go_search=0
					if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_3]:
						if self.v_quest_tab[ans_2]!=self.v_quest_tab[ans_3]:go_search=0
				go_search=1
				while go_search:
					ans_4=(random.randint(v_rnd[0],v_rnd[1]))#odp2
					go_search+=1
					if go_search==100:go_search=0
					if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_4]:
						if self.v_quest_tab[ans_2]!=self.v_quest_tab[ans_4]:
							if self.v_quest_tab[ans_3]!=self.v_quest_tab[ans_4]:go_search=0
				ans_r=(random.randint(0,3))#pytanie
				que=[ans_1,ans_2,ans_3,ans_4][ans_r]#odp
				if que<12:v_kind='masculum'
				if que>11 and que<24:v_kind='femininum'
				if que>23 and que<36:v_kind='neutrum'
				self.v_generation_ret=([self.v_adjective_tab[que]+' ('+v_kind+') '+self.v_quest_tab[0],self.v_quest_tab[ans_1],self.v_quest_tab[ans_2],self.v_quest_tab[ans_3],self.v_quest_tab[ans_4],str(ans_r),self.v_quest_tab[que],self.v_lat_contest,self.v_eng_contest])
	
			if question_type=='write':
				que=ans_r=(random.randint(v_rnd[0],v_rnd[1]))#pytanie#odp
				if que<12:v_kind='masculum'
				if que>11 and que<24:v_kind='femininum'
				if que>23 and que<36:v_kind='neutrum'
				self.v_generation_ret=([self.v_adjective_tab[que]+' ('+v_kind+') '+self.v_quest_tab[0],'','','','',self.v_quest_tab[ans_r],self.v_quest_tab[ans_r],self.v_lat_contest,self.v_eng_contest])

			if question_type=='dichotomy':
				que=(random.randint(v_rnd[0],v_rnd[1]))#pytanie#odp
				ans_1=(random.randint(v_rnd[0],v_rnd[1]))
				if que<12:v_kind='masculum'
				if que>11 and que<24:v_kind='femininum'
				if que>23 and que<36:v_kind='neutrum'
				if self.v_quest_tab[ans_1]==self.v_quest_tab[que]:
					ans_r=1
					ans_rt='Yes'
				else:
					ans_r=0
					ans_rt='No'
				self.v_generation_ret=([self.v_adjective_tab[que]+' ('+v_kind+')'+': '+self.v_quest_tab[ans_1],'','','','',str(ans_r),ans_rt,self.v_lat_contest,self.v_eng_contest])

		if self.v_quest[0]=='V':
			self.v_v_con=self.v_quest[1]
			self.v_gram_ret=grammatic.verb_coniugation(number=grammatic.random_question(part='V',coniugation=self.v_v_con))
			self.v_quest_tab=self.v_gram_ret[0]
			
			self.v_number_line=self.v_gram_ret[1]
			self.v_number_inflection=self.v_gram_ret[2]
			self.v_lat_contest=(xml_read_line(file_name=GD.xml_file_lat_name ,contest='shank',number=self.v_number_line))
			self.v_eng_contest=(xml_read_line(file_name=GD.xml_file_eng_name ,contest='shank',number=self.v_number_line))

			if self.v_quest[2]=='Praesentis':self.v_quest_tab=self.v_quest_tab[0]
			if self.v_quest[2]=='Imperfecti':self.v_quest_tab=self.v_quest_tab[1]
			if self.v_quest[2]=='Futuri I':self.v_quest_tab=self.v_quest_tab[2]
			if self.v_quest[2]=='Perfecti':self.v_quest_tab=self.v_quest_tab[3]
			if self.v_quest[2]=='Plusquam-perfecti':self.v_quest_tab=self.v_quest_tab[4]
			if self.v_quest[2]=='Futuri exacti':self.v_quest_tab=self.v_quest_tab[5]

			if self.v_quest[3]=='Activi':self.v_quest_tab=self.v_quest_tab[0:30]
			if self.v_quest[3]=='Passivi':self.v_quest_tab=self.v_quest_tab[30:60]
	
			if question_type=='dichotomy':

				while 1:
					que=(random.randint(0,len(self.v_quest_tab)-1))#pytanie#odp
					if self.v_quest_tab[que]!='-':break
					
				while 1:
					ans_1=(random.randint(0,len(self.v_quest_tab)-1))
					if self.v_quest_tab[ans_1]!='-':break
					
				self.modus_que=GD.modus_tab[que/6]
				#print self.v_quest_tab
				if self.v_quest_tab[ans_1]==self.v_quest_tab[que]:
					ans_r=1
					ans_rt='Yes'
				else:
					ans_r=0
					ans_rt='No'

				if self.v_quest_tab[que]=='-': self.v_generation_ret=([self.modus_que+' '+self.v_quest[2]+' '+self.v_quest[3]+' '+self.v_verb_tab[que]+': \nform not exist','','','','',str(ans_r),ans_rt,self.v_lat_contest,self.v_eng_contest])
				else: self.v_generation_ret=([self.modus_que+' '+self.v_quest[2]+' '+self.v_quest[3]+' '+self.v_verb_tab[que]+': \n'+self.v_quest_tab[ans_1],'','','','',str(ans_r),ans_rt,self.v_lat_contest,self.v_eng_contest])


			if question_type=='choose':

				while 1:
					ans_rr=que=(random.randint(0,len(self.v_quest_tab)-1))
					str_ans_0=self.v_quest_tab[ans_rr]
					self.modus_que=GD.modus_tab[que/6]
					str_ans_0=self.v_quest_tab[que]#odp2
					if str_ans_0 not in ('-','-'):break
				while 1:
					str_ans_1=self.v_quest_tab[random.randint(0,len(self.v_quest_tab)-1)]#odp2
					if str_ans_1 not in (str_ans_0,'-'):break
				while 1:
					str_ans_2=self.v_quest_tab[random.randint(0,len(self.v_quest_tab)-1)]#odp2
					if str_ans_2 not in (str_ans_0,str_ans_1,'-'):break
				while 1:
					str_ans_3=self.v_quest_tab[random.randint(0,len(self.v_quest_tab)-1)]#odp2
					if str_ans_3 not in (str_ans_0,str_ans_1,str_ans_2,'-'):break
				while 1:
					str_ans_4=self.v_quest_tab[random.randint(0,len(self.v_quest_tab)-1)]#odp2
					if str_ans_4 not in (str_ans_0,str_ans_1,str_ans_2,str_ans_3,'-'):break
					
				ans_r=random.randint(0,3)#pytanie
				if ans_r==0:str_ans_1=str_ans_0
				if ans_r==1:str_ans_2=str_ans_0
				if ans_r==2:str_ans_3=str_ans_0
				if ans_r==3:str_ans_4=str_ans_0
					

				self.v_generation_ret=([self.modus_que+' '+self.v_quest[2]+' '+self.v_quest[3]+' '+self.v_verb_tab[que],str_ans_1,str_ans_2,str_ans_3,str_ans_4,str(ans_r),self.v_quest_tab[que],self.v_lat_contest,self.v_eng_contest])
	
			if question_type=='write':
				ans_r=que=(random.randint(0,len(self.v_quest_tab)-1))
				self.modus_que=GD.modus_tab[que/6]
				self.v_generation_ret=([self.modus_que+' '+self.v_quest[2]+' '+self.v_quest[3]+' '+self.v_verb_tab[ans_r],'','','','',self.v_quest_tab[ans_r],self.v_quest_tab[ans_r],self.v_lat_contest,self.v_eng_contest])

		return(self.v_generation_ret)
			
	def dialog_done(self,data=None):
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup('Done questions: '+str(self.v_done1)+'\nAll questions: '+str(self.v_done2))
		dialog.run()
		dialog.destroy()
	def dialog_stats(self,data=None):
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup('Number right answers: '+str(self.v_stats1)+'\nPercentage right answers: '+str(self.v_stats2)+' %')
		dialog.run()
		dialog.destroy()
	def dialog_time(self,data=None):
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup('Time request last question: '+str(self.v_time1)+' [s]\nMean time request: '+str(self.v_time2)+' [s]')
		dialog.run()
		dialog.destroy()
	def dialog_answer(self,data=None):
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup(data)
		dialog.run()
		self.new_question()
		dialog.destroy()
	def dialog_help(self,data=None):
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup(self.lesson_name)
		dialog.run()
		#self.new_question()
		dialog.destroy()

def saveStats(newStats):
	question=newStats[0][0]*2
	answer=newStats[1][2]+newStats[2][2]
	timeTeach=newStats[0][5]+newStats[1][5]+newStats[2][5]
	GD.teachLesson
	file = open(GD.statsFilePath+GD.userName+".txt")
	ret=[]
	upgrade=0
	try:
		for line in file:
			ret.append(strip(line)[1:-1].replace('"','').split(','))
			if ret[-1][0]==str(GD.teachLesson):
				upgrade=True 
				ret[-1][1]=str(int(ret[-1][1])+answer)
				ret[-1][2]=str(int(ret[-1][2])+question)
				ret[-1][3]=str(int(ret[-1][3])+timeTeach)
		if not upgrade:ret.append([str(GD.teachLesson),str(answer),str(question),str(timeTeach)])
			
			
	finally:
		file.close()
	
	file = open(GD.statsFilePath+GD.userName+".txt","w")
	for i in range(len(ret)):
		out=str(ret[i])
		out=replace(out,' ','')
		out=replace(out,"'",'')
		file.writelines(out+"\n")
	file.close()
	
def startTeachWordsWindow():
	GD.m_w=Teach_Words_Window(quest=['V','1','0','0','0'])
	gtk.main()
startTeachWordsWindow()
