# coding: UTF-8
# menu.py
#
# library for: Cicero Open Project
# 

# PROGRAM CONSTATNTS
__version__='1.0.0'
__license__='GPL'
__author__='Cicero Develop Team'
__site__='www.cicero.glt.pl'

# IMPORTS
import pygtk
import gtk
import gobject
import teach_window
import global_direct as GD
import string
import random
import xml_base_handling
pygtk.require('2.0')
from string import strip, split, replace
from random import randint
from xml_base_handling import xml_load_base, xml_read_line, xml_split_line


# GLOBAL VARIABLES
GD.m_w=None #main window



class Menu_Window:


	def __init__(self):
		
##########################
#Config
##########################
		self.loadConfig(set=True)
##########################
#Window
##########################
		#GD.userName="Cicero"
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.set_title(GD.titleMenuWindow)
		self.window.set_size_request(GD.sizeWindow[0],GD.sizeWindow[1])
		self.window.connect("destroy", lambda wid: gtk.main_quit())
		self.window.connect("delete_event", lambda a1,a2:gtk.main_quit())
		self.window.set_icon_from_file("./data/pic/log_in.png")
		if GD.maximize: self.window.maximize()
		if GD.fullScreen: self.window.fullscreen()
		#self.tooltips = gtk.Tooltips()
		#self.tooltips.set_delay(1000)
		
		self.surface=gtk.TextView()
		self.surface.show()
		self.surface.set_editable(False)
		
##########################
#Menu
##########################

		self.b_teach_words = gtk.Button()
		self.b_teach_words.connect("clicked", self.showTeachWords)
		self.b_teach_words.set_relief(gtk.RELIEF_HALF)
		button_box = box_button(self.window, "./data/pic/teach1.png", "")
		#self.tooltips.set_tip(self.b_teach_words, GD.UITeachWords)
		self.b_teach_words.add(button_box)
		self.b_teach_words.show()
		child = gtk.EventBox()
		child.add(self.b_teach_words)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 275,530)


		self.b_teach = gtk.Button()
		self.b_teach.connect("clicked", self.showTeach)
		self.b_teach.set_relief(gtk.RELIEF_HALF)
		button_box = box_button(self.window, "./data/pic/teach.png", "")
		#self.tooltips.set_tip(self.b_teach, GD.UITeachGrammar)
		self.b_teach.add(button_box)
		self.b_teach.show()
		child = gtk.EventBox()
		child.add(self.b_teach)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 360,530)

		self.b_mark = gtk.Button()
		self.b_mark.connect("clicked", self.showMark)
		self.b_mark.set_relief(gtk.RELIEF_HALF)
		button_box = box_button(self.window, "./data/pic/mark.png", "")
		#self.tooltips.set_tip(self.b_mark, GD.UIMark)
		self.b_mark.add(button_box)
		self.b_mark.show()
		child = gtk.EventBox()
		child.add(self.b_mark)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 445,530)

		self.b_options = gtk.Button()
		self.b_options.connect("clicked", self.showOptions)
		self.b_options.set_relief(gtk.RELIEF_HALF)
		button_box = box_button(self.window, "./data/pic/options.png", "")
		#self.tooltips.set_tip(self.b_options, GD.UIOption)
		self.b_options.add(button_box)
		self.b_options.show()
		child = gtk.EventBox()
		child.add(self.b_options)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 530,530)
		
		#self.b_tools = gtk.Button()
		#self.b_tools.connect("clicked", self.showOptions)
		#self.b_tools.set_relief(gtk.RELIEF_HALF)
		#button_box = box_button(self.window, "./data/pic/tools.png", "")
		#self.tooltips.set_tip(self.b_tools, """Tools""")
		#self.b_tools.add(button_box)
		#self.b_tools.show()
		#child = gtk.EventBox()
		#child.add(self.b_tools)
		#self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 615,530)

		self.b_exit = gtk.Button()
		self.b_exit.connect("clicked", self.exit)
		self.b_exit.set_relief(gtk.RELIEF_HALF)
		button_box = box_button(self.window, "./data/pic/exit.png", "")
		#self.tooltips.set_tip(self.b_exit, GD.UIExit)
		self.b_exit.add(button_box)
		self.b_exit.show()
		self.b_exit.enter()
		child = gtk.EventBox()
		child.add(self.b_exit)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 700,530)
		
		self.b_help = gtk.Button()
		self.b_help.connect("clicked", self.showHelp)
		self.b_help.set_relief(gtk.RELIEF_HALF)
		button_box = box_button(self.window, "./data/pic/help.png", "")
		#self.tooltips.set_tip(self.b_help, GD.UIHelp)
		self.b_help.add(button_box)
		self.b_help.show()
		self.b_help.enter()
		child = gtk.EventBox()
		child.add(self.b_help)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 700,160)#720,160
		
		self.b_name = gtk.Button(GD.userName)
		self.b_name.connect("clicked", self.showName)
		self.b_name.set_relief(gtk.RELIEF_HALF)
		#self.tooltips.set_tip(self.b_name, GD.UIName)
		self.b_name.show()
		self.b_name.enter()
		child = gtk.EventBox()
		child.add(self.b_name)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 550,160)#720,160

##########################
#Options
##########################

		self.lblOptions = gtk.Label()
		self.lblOptions.set_markup('<big><big><b>'+GD.UIOption+'</b></big></big>')
		
		self.frameOptionsScreen1=gtk.Frame()
		self.frameOptionsScreen2=gtk.Frame()
		self.hBoxFrameOptionsScreen=gtk.HBox(False, 0)
		self.lblFrameOptionsScreen=gtk.Label()
		self.lblFrameOptionsScreen.set_markup('<b>'+GD.UIScreen+'</b>')
		
		self.frameOptionsTeach1=gtk.Frame()
		self.frameOptionsTeach2=gtk.Frame()
		self.hBoxFrameOptionsTeach=gtk.HBox(False, 0)
		self.lblFrameOptionsTeach=gtk.Label()
		self.lblFrameOptionsTeach.set_markup('<b>'+GD.UITeach+'</b>')
		
		self.frameOptionsBase1=gtk.Frame()
		self.frameOptionsBase2=gtk.Frame()
		self.hBoxFrameOptionsBase=gtk.HBox(False, 0)
		self.lblFrameOptionsBase=gtk.Label()
		self.lblFrameOptionsBase.set_markup('<b>'+GD.UIBase+'</b>')
		
		self.frameOptionsOther1=gtk.Frame()
		self.frameOptionsOther2=gtk.Frame()
		self.hBoxFrameOptionsOther=gtk.HBox(False, 0)
		self.lblFrameOptionsOther=gtk.Label()
		self.lblFrameOptionsOther.set_markup('<b>'+GD.UIOther+'</b>')
		
		self.frameBottons01=gtk.Frame()#GD.UIGeneralStats
		self.frameBottons02=gtk.Frame()#GD.UIGeneralStats
		self.hBoxFrameBottons0=gtk.HBox(False, 0)
		self.lblFrameBottons0=gtk.Label()
		self.lblFrameBottons0.set_markup('<b>'+GD.UIOther+'</b>')
		
		self.hBoxFrameOptionsScreen.pack_start(self.frameOptionsScreen1,True, True, 0)
		self.hBoxFrameOptionsScreen.pack_start(self.lblFrameOptionsScreen,False, True, 0)
		self.hBoxFrameOptionsScreen.pack_start(self.frameOptionsScreen2,True, True, 0)
		
		self.hBoxFrameOptionsTeach.pack_start(self.frameOptionsTeach1,True, True, 0)
		self.hBoxFrameOptionsTeach.pack_start(self.lblFrameOptionsTeach,False, True, 0)
		self.hBoxFrameOptionsTeach.pack_start(self.frameOptionsTeach2,True, True, 0)

		self.hBoxFrameOptionsBase.pack_start(self.frameOptionsBase1,True, True, 0)
		self.hBoxFrameOptionsBase.pack_start(self.lblFrameOptionsBase,False, True, 0)
		self.hBoxFrameOptionsBase.pack_start(self.frameOptionsBase2,True, True, 0)

		self.hBoxFrameOptionsOther.pack_start(self.frameOptionsOther1,True, True, 0)
		self.hBoxFrameOptionsOther.pack_start(self.lblFrameOptionsOther,False, True, 0)
		self.hBoxFrameOptionsOther.pack_start(self.frameOptionsOther2,True, True, 0)
		
		self.hBoxFrameBottons0.pack_start(self.frameBottons01,True, True, 0)
		self.hBoxFrameBottons0.pack_start(self.lblFrameBottons0,False, True, 0)
		self.hBoxFrameBottons0.pack_start(self.frameBottons02,True, True, 0)
		
		self.hBoxScreen=gtk.HBox(False, 0)
		self.hBoxScreen.show()
		self.hBoxTeach=gtk.HBox(False, 0)
		self.hBoxTeach.show()
		self.hBoxBase=gtk.HBox(False, 0)
		self.hBoxBase.show()
		self.hBoxOther=gtk.HBox(False, 0)
		self.hBoxOther.show()
		
		self.checkFullScreen=gtk.CheckButton(GD.UIFullScreen)
		self.checkFullScreen.set_active(1)
		self.checkFullScreen.set_sensitive(False)
		self.checkFullScreen.show()
		self.hBoxScreen.pack_start(self.checkFullScreen,True,True,10)
		
		self.checkMaximize=gtk.CheckButton(GD.UIMaximize)
		self.checkMaximize.set_active(1)
		self.checkMaximize.set_sensitive(False)
		self.checkMaximize.show()
		self.hBoxScreen.pack_start(self.checkMaximize,True,True,10)
			
		self.comboLongTeach = gtk.combo_box_new_text()
		self.comboLongTeach.append_text(GD.UIGrammarNOQ)
		self.comboLongTeach.append_text('5')
		self.comboLongTeach.append_text('10')
		self.comboLongTeach.append_text('15')
		self.comboLongTeach.append_text('20')
		self.comboLongTeach.set_active(0)
		self.comboLongTeach.show()
		self.hBoxTeach.pack_start(self.comboLongTeach,True,True,10)

		self.comboLangTeach = gtk.combo_box_new_text()
		self.comboLangTeach.append_text(GD.UIWordsTL)
		self.comboLangTeach.append_text(GD.UIEnglishLatin)
		self.comboLangTeach.append_text(GD.UIPolishLatin)
		self.comboLangTeach.set_active(0)
		self.comboLangTeach.show()
		self.hBoxTeach.pack_start(self.comboLangTeach,True,True,10)
		
		self.comboBaseConstants = gtk.combo_box_new_text()
		self.comboBaseConstants.append_text(GD.UIBaseContest)
		self.comboBaseConstants.append_text(GD.UILatinBase+str(GD.xml_file_lat_name))
		self.comboBaseConstants.append_text(GD.UIEnglishBase+str(GD.xml_file_eng_name))
		self.comboBaseConstants.append_text(GD.UIUserFiles+str(GD.usersTabelFile))
		self.comboBaseConstants.append_text(GD.UICounterPassowers+str(GD.limit))
		self.comboBaseConstants.append_text(GD.UISearchingTime+str(GD.timeSearchQuestion))
		self.comboBaseConstants.append_text(GD.UIStatsFilePath+str(GD.statsFilePath))
		self.comboBaseConstants.set_active(0)
		self.comboBaseConstants.show()
		self.hBoxBase.pack_start(self.comboBaseConstants,True,True,10)
		
		self.checkSentence=gtk.CheckButton(GD.UISentence)
		self.checkSentence.set_active(1)
		self.checkSentence.show()
		self.hBoxOther.pack_start(self.checkSentence,True,True,10)
		
		self.checkGraphics=gtk.CheckButton(GD.UIGraphics)
		self.checkGraphics.set_active(1)
		self.checkGraphics.show()
		self.hBoxOther.add(self.checkGraphics)
		
		self.boxOptionButtons = gtk.HButtonBox()
		
		self.butStart = gtk.Button(GD.UISave)
		self.boxOptionButtons.pack_start(self.butStart)
		self.butStart.connect('clicked', self.saveConfig)
            
            
		self.butBack = gtk.Button(GD.UIBack)
		self.boxOptionButtons.pack_start(self.butBack)
		self.butBack.connect('clicked', self.showMenu)
		
		self.VBoxOptions=gtk.VBox()
		self.VBoxOptions.show()
		self.VBoxOptions.pack_start(self.lblOptions,False, False, 10)
		
		self.VBoxOptions.pack_start(self.hBoxFrameOptionsScreen,False, False, 10)
		self.VBoxOptions.pack_start(self.hBoxScreen,True, True, 0)
		self.VBoxOptions.pack_start(self.hBoxFrameOptionsTeach,False, False, 10)
		self.VBoxOptions.pack_start(self.hBoxTeach,True, True, 0)
		self.VBoxOptions.pack_start(self.hBoxFrameOptionsBase,False, False, 10)
		self.VBoxOptions.pack_start(self.hBoxBase,True, True, 0)
		self.VBoxOptions.pack_start(self.hBoxFrameOptionsOther,False, False, 10)
		self.VBoxOptions.pack_start(self.hBoxOther,True, True, 0)
		self.VBoxOptions.pack_start(self.hBoxFrameBottons0,False, False, 10)

		self.VBoxOptions.pack_start(self.boxOptionButtons, False, False, 10)
		
		child = gtk.EventBox()
		child.add(self.VBoxOptions)
		self.VBoxOptions.set_size_request(0,0)
		############################Warning!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 250,150)
##########################
#Mark
##########################
		

		self.lblMark = gtk.Label()
		self.lblMark.set_markup('<big><big><b>'+GD.UIMark+'</b></big></big>')
		
		self.VBoxMark=gtk.VBox(False, 0)
		self.VBoxMark.show()

		self.frameLearningLessons1=gtk.Frame()#GD.UILearningLessons
		self.frameLearningLessons2=gtk.Frame()#GD.UILearningLessons
		self.hBoxFrameLearningLessons=gtk.HBox(False, 0)
		self.lblFrameLearningLessons=gtk.Label()
		self.lblFrameLearningLessons.set_markup('<b>'+GD.UILearningLessons+'</b>')
		
		self.frameSuggestLesson1=gtk.Frame()#GD.UISuggestlesson
		self.frameSuggestLesson2=gtk.Frame()#GD.UISuggestlesson
		self.hBoxFrameSuggestLesson=gtk.HBox(False, 0)
		self.lblFrameSuggestLesson=gtk.Label()
		self.lblFrameSuggestLesson.set_markup('<b>'+GD.UISuggestlesson+'</b>')
		
		self.frameGeneralStats1=gtk.Frame()#GD.UIGeneralStats
		self.frameGeneralStats2=gtk.Frame()#GD.UIGeneralStats
		self.hBoxFrameGeneralStats=gtk.HBox(False, 0)
		self.lblFrameGeneralStats=gtk.Label()
		self.lblFrameGeneralStats.set_markup('<b>'+GD.UIGeneralStats+'</b>')
		
		self.frameBottons11=gtk.Frame()#GD.UIGeneralStats
		self.frameBottons12=gtk.Frame()#GD.UIGeneralStats
		self.hBoxFrameBottons1=gtk.HBox(False, 0)
		self.lblFrameBottons1=gtk.Label()
		self.lblFrameBottons1.set_markup('<b>'+GD.UIOther+'</b>')
		
		self.hBoxFrameLearningLessons.pack_start(self.frameLearningLessons1,True, True, 0)
		self.hBoxFrameLearningLessons.pack_start(self.lblFrameLearningLessons,False, True, 0)
		self.hBoxFrameLearningLessons.pack_start(self.frameLearningLessons2,True, True, 0)
		
		self.hBoxFrameSuggestLesson.pack_start(self.frameSuggestLesson1,True, True, 0)
		self.hBoxFrameSuggestLesson.pack_start(self.lblFrameSuggestLesson,False, True, 0)
		self.hBoxFrameSuggestLesson.pack_start(self.frameSuggestLesson2,True, True, 0)

		self.hBoxFrameGeneralStats.pack_start(self.frameGeneralStats1,True, True, 0)
		self.hBoxFrameGeneralStats.pack_start(self.lblFrameGeneralStats,False, True, 0)
		self.hBoxFrameGeneralStats.pack_start(self.frameGeneralStats2,True, True, 0)
		
		self.hBoxFrameBottons1.pack_start(self.frameBottons11,True, True, 0)
		self.hBoxFrameBottons1.pack_start(self.lblFrameBottons1,False, True, 0)
		self.hBoxFrameBottons1.pack_start(self.frameBottons12,True, True, 0)
		
		self.hBoxLearningLessons=gtk.HBox(False, 0)
		self.hBoxLearningLessons.show()
		self.hBoxSuggestLesson=gtk.HBox(False, 0)
		self.hBoxSuggestLesson.show()
		self.vBoxGeneralStats=gtk.VBox(False, 0)
		self.vBoxGeneralStats.show()
		
		self.lblNumberRepeats=gtk.Label(GD.UINumberRepeats)#AntiWarning
		self.lblTimeLearning = gtk.Label(GD.UITimeLearning)#AntiWarning
		self.comboLearningLessons = gtk.combo_box_new_text()
		self.comboLearningLessons.append_text(GD.UILearningLesson)
		self.comboLearningLessons.connect('changed', self.refreshStatsLessonInMarkWindow)
		for i in range(len(GD.v_lessons_name)):
			self.comboLearningLessons.append_text(str(GD.v_lessons_name[i]))
		for i in range(len(GD.v_lessons_name_1)):
			self.comboLearningLessons.append_text(str(GD.v_lessons_name_1[i]))
		self.comboLearningLessons.set_active(0)
		self.comboLearningLessons.show()
		self.hBoxLearningLessons.pack_start(self.comboLearningLessons,True, True, 10)
		
		self.lblNumberRepeats = gtk.Label(GD.UINumberRepeats)
		self.lblNumberRepeats.show()
		self.hBoxLearningLessons.pack_start(self.lblNumberRepeats,True, True, 10)
		self.lblTimeLearning = gtk.Label(GD.UITimeLearning)
		self.lblTimeLearning.show()
		self.hBoxLearningLessons.pack_start(self.lblTimeLearning,True, True, 10)
		
		self.lblSuggestLesson = gtk.Label(GD.v_lessons_name_1[int(self.chooseSugestLesson())])#GD.UISuggestlesson
		self.lblSuggestLesson.show()
		self.hBoxSuggestLesson.pack_start(self.lblSuggestLesson,True, True, 10)
		
		self.lblGeneralLearningTime = gtk.Label(GD.UIGeneralLearningTime+'0')
		self.lblGeneralLearningTime.show()
		self.vBoxGeneralStats.pack_start(self.lblGeneralLearningTime,True, True, 10)

		self.lblGeneralNumberRepeats = gtk.Label(GD.UIGeneralNumberRepeats+'0')
		self.lblGeneralNumberRepeats.show()
		self.vBoxGeneralStats.pack_start(self.lblGeneralNumberRepeats,True, True, 10)

		self.v_number_sen=random.randint(1,180)
		self.v_line_sen=(xml_read_line(file_name=GD.xml_file_sentence_base,contest='all',number=self.v_number_sen))
		start=self.v_line_sen.find('sen="')
		med=self.v_line_sen.find('-')
		end=self.v_line_sen.find('">')
		
		sent_lat=self.v_line_sen[start+5:med-1]
		
		self.lblSentenceToday = gtk.Label(sent_lat)
		self.lblSentenceToday.show()
		self.vBoxGeneralStats.pack_start(self.lblSentenceToday,True, True, 10)
		
		self.boxOptionButtons = gtk.HButtonBox()

		self.butStart = gtk.Button(GD.UIBack)
		self.boxOptionButtons.pack_start(self.butStart)
		self.butStart.connect('clicked', self.showMenu)
            
		self.butBack = gtk.Button(GD.UIBack)
		self.boxOptionButtons.pack_start(self.butBack)
		self.butBack.connect('clicked', self.showMenu)


		self.VBoxMark.pack_start(self.lblMark,False, False, 10)
		
		self.VBoxMark.pack_start(self.hBoxFrameLearningLessons,False, False, 10)
		self.VBoxMark.pack_start(self.hBoxLearningLessons,True, True, 0)
		self.VBoxMark.pack_start(self.hBoxFrameSuggestLesson,False, False, 10)
		self.VBoxMark.pack_start(self.hBoxSuggestLesson,True, True, 0)
		self.VBoxMark.pack_start(self.hBoxFrameGeneralStats,False, False, 10)
		self.VBoxMark.pack_start(self.vBoxGeneralStats,True, True, 0)
		self.VBoxMark.pack_start(self.hBoxFrameBottons1,False, False, 10)
		self.VBoxMark.pack_start(self.boxOptionButtons,True, True, 10)
		
		child = gtk.EventBox()
		child.add(self.VBoxMark)
		self.VBoxMark.set_size_request(0,0)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 250,150)
		
##########################
#Teach
##########################
		self.lblTeach = gtk.Label()
		self.lblTeach.set_markup('<big><big><b>'+GD.UITeach+'</b></big></big>')
		
		self.liststore = gtk.ListStore(int,str, int, int, int, int)
		self.modelfilter = self.liststore.filter_new()
		self.treeview = gtk.TreeView()
		self.treeview.columns = [None]*5
		self.treeview.columns[0] = gtk.TreeViewColumn(GD.UINr)
		self.treeview.columns[1] = gtk.TreeViewColumn(GD.UILesson)
		self.treeview.columns[2] = gtk.TreeViewColumn(GD.UIRepeat)#Repeat
		self.treeview.columns[3] = gtk.TreeViewColumn(GD.UIMarkP)
		self.treeview.columns[4] = gtk.TreeViewColumn(GD.UITS)
		#self.treeview.columns[5] = gtk.TreeViewColumn("""SL""")

			
		for n in range(5):
			self.treeview.append_column(self.treeview.columns[n])
			self.treeview.columns[n].cell = gtk.CellRendererText()
			self.treeview.columns[n].pack_start(self.treeview.columns[n].cell,True)
			self.treeview.columns[n].set_attributes(self.treeview.columns[n].cell, text=n)
			
		self.vbox = gtk.VBox()
		self.bbox = gtk.HButtonBox()
		self.scrolledwindow = gtk.ScrolledWindow()
		self.vbox.pack_start(self.lblTeach, False, False, 10)
		self.vbox.pack_start(self.scrolledwindow)
		self.vbox.pack_start(self.bbox, False, False, 10)

		
		self.b_start = gtk.Button(GD.UIStart)
		self.bbox.pack_start(self.b_start)
		self.b_start.connect('clicked', self.startTeach)
            
		self.b_back = gtk.Button(GD.UIBack)
		self.bbox.pack_start(self.b_back)
		self.b_back.connect('clicked', self.showMenu)
		
		self.scrolledwindow.add(self.treeview)
		
		#self.cellColor = gtk.CellRendererText()
		#self.cellColor.set_property('cell-background', 'yellow')
		#self.treeview.columns[0].pack_start(self.cellColor, False)
		#self.treeview.columns[0].set_attributes(self.cellColor, cell_background_set=5)
	
		child = gtk.EventBox()
		child.add(self.vbox)
		self.vbox.set_size_request(0,0)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 250,150)
		
		
##########################
#Wallpaper
##########################


		self.i_wallpaper = gtk.Image()
		self.i_wallpaper.set_from_file('./data/pic/main_menu.jpg')
		child = gtk.EventBox()
		child.add(self.i_wallpaper)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 0, 0)

		
		self.surface.show_all()
		self.But1=gtk.Button(42*'\n')
		self.But2=gtk.Button(42*'\n')
		self.But3=gtk.Button()
		self.But4=gtk.Button(250*' ')
		
		self.But1.show()
		self.But2.show()
		self.But3.show()
		self.But4.show()
		self.But1.set_relief(gtk.RELIEF_NONE)
		self.But2.set_relief(gtk.RELIEF_NONE)
		self.But3.set_relief(gtk.RELIEF_NONE)
		self.But4.set_relief(gtk.RELIEF_NONE)
		self.VBoxMainSurface = gtk.VBox()
		self.HBoxMainSurface=gtk.HBox()
		self.HBoxMainSurfaceInto=gtk.HBox()
		self.VBoxMainSurface.show()
		self.HBoxMainSurface.show()
		self.HBoxMainSurfaceInto.show()
		
		self.HBoxMainSurfaceInto.pack_start(self.But1, False, False, 0)
		self.HBoxMainSurfaceInto.pack_start(self.surface, True, True, 0)
		self.HBoxMainSurfaceInto.pack_start(self.But2, False, False, 0)
		
		self.VBoxMainSurface.pack_start(self.But3, False, True, 0)
		self.VBoxMainSurface.pack_start(self.HBoxMainSurfaceInto, True, False, 0)
		self.VBoxMainSurface.pack_start(self.But4, False, True, 0)
		
		#self.HBoxMainSurface.pack_start(self.But1, False, False, 0)
		self.HBoxMainSurface.pack_start(self.VBoxMainSurface, True, False, 0)
		#self.HBoxMainSurface.pack_start(self.But2, False, False, 0)
		
		
		self.window.add(self.HBoxMainSurface)

		style = self.window.get_style()
		style = self.window.modify_bg(gtk.STATE_NORMAL, gtk.gdk.Color(0, 0, 0))
		self.window.set_style(style)

		self.b_teach_words.grab_focus()
		self.window.show()
		

		
	def exit(self, data=None):
		gtk.main_quit()
	
		
	def hideAll(self, data=None):
		self.b_exit.hide()
		self.b_help.hide()
		self.b_options.hide()
		self.b_mark.hide()
		self.b_teach.hide()
		#self.b_tools.hide()
		self.b_name.hide()
		self.i_wallpaper.hide()
		self.vbox.hide()
		self.VBoxOptions.hide()
		self.VBoxMark.hide()
		self.b_teach_words.hide()
		
	def showTeach(self, data=None):
		GD.teachMaterial='grammar'
		self.liststore.clear()
		for n in range(len(GD.v_lessons_name)):
			self.liststore.append([n+1,str(GD.v_lessons_name[n]),0,0,0,0])
				
		self.refreshTabel()
		self.hideAll()
		self.vbox.set_size_request(524,468)
		self.vbox.show()
		self.i_wallpaper.show()
		
	def showTeachWords(self, data=None):
		GD.teachMaterial='words'
		self.liststore.clear()
		for n in range(len(GD.v_lessons_name_1)):
			self.liststore.append([n+1,str(GD.v_lessons_name_1[n]),0,0,0,0])
			
		self.refreshTabel()
		self.hideAll()
		self.vbox.set_size_request(524,468)
		self.vbox.show()
		self.i_wallpaper.show()

		
	def showMenu(self, data=None):
		self.hideAll()
		self.b_exit.show()
		self.b_help.show()
		self.b_options.show()
		self.b_name.show()
		self.b_mark.show()
		#self.b_tools.show()
		self.b_teach.show()
		self.b_teach_words.show()
		self.i_wallpaper.show()	

	def showMark(self, data=None):
		self.hideAll()
		self.VBoxMark.set_size_request(524,468)
		self.VBoxMark.show()
		self.i_wallpaper.show()
		timeTeach=0
		repeatsNumbers=0
		GD.teachMaterial='grammar'
		userStats=loadStats()
		for i in range(len(userStats)):
			repeatsNumbers+=int(userStats[i][2])
			timeTeach+=int(userStats[i][3])
		GD.teachMaterial='words'
		userStats=loadStats()
		for i in range(len(userStats)):
			repeatsNumbers+=int(userStats[i][2])
			timeTeach+=int(userStats[i][3])
		self.lblGeneralNumberRepeats.set_text(GD.UIGeneralNumberRepeats+str(repeatsNumbers))
		self.lblGeneralLearningTime.set_text(GD.UIGeneralLearningTime+str(int(timeTeach/60))+'[min]')#+GD.UISEC
		sugestLesson=self.chooseSugestLesson()
		self.lblSuggestLesson.set_text(GD.v_lessons_name_1[sugestLesson])
		
	def showOptions(self, data=None):
		self.hideAll()
		self.VBoxOptions.set_size_request(524,468)
		self.VBoxOptions.show()
		self.i_wallpaper.show()
		config=self.loadConfig()
		self.checkFullScreen.set_active(int(config[0]))
		self.checkMaximize.set_active(int(config[1]))
		self.comboLongTeach.set_active(int(config[2]))
		self.comboLangTeach.set_active(int(config[3]))
		self.checkSentence.set_active(int(config[4]))
		self.checkGraphics.set_active(int(config[5]))
	
	def showHelp(self, data=None):
		print 'help'
		
		data=''
		data+='fullScreen='+str(GD.fullScreen)+'\n'
		data+='maximize='+str(GD.maximize)+'\n'
		data+='userName='+str(GD.userName)+'\n'
		data+='longTeach='+str(GD.longTeach)+'\n'
		data+='langTeach='+str(GD.langTeach)+'\n'
		data+='relaxGraphics='+str(GD.relaxGraphics)+'\n'
		data+='relaxSentece='+str(GD.relaxSentece)+'\n'
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup(data)
		dialog.run()
		dialog.destroy()
		
	def showName(self, data=None):
	
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup(GD.UIWelcome+GD.userName+GD.UIInCicero)
		dialog.run()
		dialog.destroy()
		
	def startTeach(self, data=None):
		selection = self.treeview.get_selection()
		list_stor, iter = selection.get_selected()
		if iter:
			self.window.hide()
			GD.mainWindow=self
			GD.teachLesson=int(list_stor.get_value(iter,0))-1
			if GD.teachMaterial=='grammar':
				config=self.loadConfig()
				GD.longTeach=GD.longTeachTabel[int(config[2])]
				teach_window.Teach_Window(quest=GD.v_lessons[GD.teachLesson], lesson_name=GD.v_lessons_name[GD.teachLesson])
			if GD.teachMaterial=='words':
				config=self.loadConfig()
				GD.longTeach=20
				teach_window.Teach_Window(quest=GD.v_lessons_1[GD.teachLesson], lesson_name=GD.v_lessons_name_1[GD.teachLesson])

			#teach_window.show_teach_window()
			
	def refreshTabel(self):
		
		userStats=loadStats()
		for i in range(len(userStats)):
			mark=round(100*int(userStats[i][1])/int(userStats[i][2]),0)
			self.liststore[int(userStats[i][0])][3]=mark
			self.liststore[int(userStats[i][0])][2]=int(userStats[i][2])
			self.liststore[int(userStats[i][0])][4]=int(userStats[i][3])#time
			#if self.chooseSugestLesson()==i:
				#self.liststore[int(userStats[i][0])][5]=1
			#else:
				#self.liststore[int(userStats[i][0])][5]=0

		self.treeview.set_model(self.modelfilter)
			
		
	def refreshStatsLessonInMarkWindow(self, data=None):
		numberRepeat=-1
		timeLearning=-1
		if GD.v_lessons_name_all.count(self.comboLearningLessons.get_active_text()):
			lesonNumber=GD.v_lessons_name_all.index(self.comboLearningLessons.get_active_text())
			GD.teachMaterial='grammar'
			userStats=loadStats()
			for i in range(len(userStats)):
				if lesonNumber==int(userStats[i][0]):
					numberRepeat=str(userStats[i][2])
					timeLearning=str(userStats[i][3])
			GD.teachMaterial='words'
			userStats=loadStats()
			for i in range(len(userStats)):#+len(GD.v_lessons
				if lesonNumber==int(userStats[i][0])+len(GD.v_lessons):
					numberRepeat=str(userStats[i][2])
					timeLearning=str(userStats[i][3])
		if numberRepeat!=-1:
			self.lblNumberRepeats.set_text(GD.UINumberRepeatsN+numberRepeat+"""\t\t\t\t\tq""")
			self.lblTimeLearning.set_text(GD.UITimeLearningN+timeLearning+' '+GD.UISEC+'\t\t\t\tq')
		else:
			self.lblNumberRepeats.set_text(GD.UINumberRepeatsT)
			self.lblTimeLearning.set_text(GD.UITimeLearningT)

	def chooseSugestLesson(self, data=None):
		#userStats=loadStats()
		#sugestLesson=len(userStats)
		#userStats.sort()
		
		#if len(userStats)==0:
			#return(0)
		
		#if len(userStats)==1:
			#if int(userStats[0][0])<len(GD.v_lessons_name):
				#sugestLesson=(int(userStats[0][0])+1)
			#else:
				#return(0)
		#return(randint(0,len(userStats)-1))
		
		return(randint(0,len(GD.v_lessons_name_1)-1))
		
	def saveConfig(self, data=None):
		config=[int(self.checkFullScreen.get_active()),int(self.checkMaximize.get_active()),
		self.comboLongTeach.get_active(),self.comboLangTeach.get_active(),
		int(self.checkSentence.get_active()),int(self.checkGraphics.get_active())]

		file = open(GD.configTabelFile,'w')
		file.writelines(str(config))
		file.close()
		self.loadConfig(set=True)

		
	def loadConfig(self, data=None, set=False):
		file = open(GD.configTabelFile)
		ret=[]
		try:
			for line in file:
				ret.append(strip(line)[1:-1].replace('"','').split(','))
		finally:
			file.close()
			
		if set==False:
			return(ret[0])
		else:
			ret=ret[0]
			GD.fullScreen=(int(ret[0]))
			GD.maximize=(int(ret[1]))
			GD.longTeach=(int(ret[2]))
			GD.langTeach=(int(ret[3]))
			GD.relaxSentece=(int(ret[4]))
			GD.relaxGraphics=(int(ret[5]))
			
		
def box_button(parent, filename, label_text, horizont=False, pack=None, markup=None):

	box = gtk.VBox(False, 0)
	box.set_border_width(2)
	
	if horizont==True:
		box = gtk.HBox(False, 0)
		box.set_border_width(2)
		
	image = gtk.Image()
	image.set_from_file(filename)

	if pack=='center':
		box.pack_start(image, True, False, 3)
	else:
		box.pack_start(image, False, False, 3)


	image.show()
	box.show()
	return(box)
    
def loadStats():
	if GD.teachMaterial=='grammar': file = open(GD.statsFilePath+'gramatic_'+GD.userName+'.txt')
	if GD.teachMaterial=='words': file = open(GD.statsFilePath+'words_'+GD.userName+'.txt')
	ret=[]
	for line in file:
		ret.append(strip(line)[1:-1].replace('"','').split(','))
	file.close()
	return(ret)
	
def startMenu():
	if GD.m_w==None:
		GD.m_w=Menu_Window()
		gtk.main()

def showWindow():
	GD.m_w.vbox.set_size_request(0,0)
	GD.m_w.b_exit.show()
	GD.m_w.b_help.show()
	GD.m_w.b_options.show()
	GD.m_w.b_mark.show()
	GD.m_w.b_teach.show()
	GD.m_w.b_teach_words.show()
	GD.m_w.window.show()
	
	


#startMenu()
