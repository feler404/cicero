# coding: UTF-8
# teach_window.py
#
# library for: Cicero Open Project
# 

# PROGRAM CONSTATNTS
__version__='1.0.0'
__license__='GPL'
__author__='Cicero Develop Team'
__site__='www.cicero.glt.pl'

# IMPORTS
import pygtk
import gtk
import gobject
import string
import random
import time
import grammatic
import xml_base_handling
import global_direct as GD
pygtk.require('2.0')
from time import time
from string import strip, split
from xml_base_handling import xml_load_base, xml_read_line, xml_split_line
from gtk.gdk import CONTROL_MASK, SHIFT_MASK
from gtk.gdk import keyval_name
from random import randint
from string import strip, split, replace




class Teach_Window:

#dichotomy
	def __init__(self, data='dichotomy',quest=None, lesson_name=None):
		self.lesson_name=lesson_name
		self.v_quest=quest
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.set_title(GD.titleTeachWindow)
		self.window.set_size_request(GD.sizeWindow[0],GD.sizeWindow[1])
		self.window.connect("destroy", lambda wid: gtk.main_quit())
		self.window.connect("delete_event", lambda a1,a2:gtk.main_quit())
		self.window.set_icon_from_file("./data/pic/log_in.png")
		#self.window.set_decorated(gtk.FALSE)
		self.window.connect('key-press-event', self.key_press_event_cb)
		if GD.maximize: self.window.maximize()
		if GD.fullScreen: self.window.fullscreen()
		#self.tooltips = gtk.Tooltips()
		#self.tooltips.set_delay(1000)
		if GD.teachMaterial=='words':
			GD.tabelQuest=range((int(GD.v_lessons_1[GD.teachLesson][1])-1)*20+1,int(GD.v_lessons_1[GD.teachLesson][1])*20+1)
			GD.temporaryTabelQuest=set(GD.tabelQuest)
			GD.temporaryTabelQuest=list(GD.temporaryTabelQuest)

		self.i_wallpaper = gtk.Image()
		GD.backPic=random.randint(0,len(GD.backgroundPic)-1)
		self.i_wallpaper.set_from_file(GD.backgroundPic[GD.backPic])
		child = gtk.EventBox()
		child.add(self.i_wallpaper)
		
		#self.show_teach_staff(data='choose')
		self.answer_button=True
		self.answer=None
		self.v_try_time=0
		self.v_done1=0
		self.v_done2=GD.longTeach
		self.v_stats1=0
		self.v_stats2=0
		self.v_time_start=time()
		self.v_time1=0
		self.v_time2=0
		self.v_questions_number=0
		self.v_noun_tab=['Nominativus Singularis','Genetivus Singularis','Dativus Singularis',
						'Accusativus Singularis','Ablativus Singularis','Vocativus Singularis',
						'Nominativus Pluralis','Genetivus Pluralis','Dativus Pluralis',
						'Accusativus Pluralis','Ablativus Pluralis','Vocativus Pluralis']
		self.v_adjective_tab=['Nominativus Singularis','Genetivus Singularis','Dativus Singularis',
						'Accusativus Singularis','Ablativus Singularis','Vocativus Singularis',
						'Nominativus Pluralis','Genetivus Pluralis','Dativus Pluralis',
						'Accusativus Pluralis','Ablativus Pluralis','Vocativus Pluralis']
		self.v_adjective_tab=self.v_adjective_tab*3
		self.v_verb_tab=['1-st Singularis', '2-st Singularis', '3-st Singularis',
						'1-st Pluralis', '2-st Pluralis', '3-st Pluralis']
		self.v_verb_tab=self.v_verb_tab*12

		self.surface=gtk.TextView()
		self.surface.show()
		self.surface.set_editable(False)
		self.show_panel_stats(self)

		self.v_question_type=data

		if self.v_question_type=='choose':
			self.show_panel_teach_choos(self)
		if self.v_question_type=='write':
			self.show_panel_teach_write(self)	
		if self.v_question_type=='dichotomy':
			self.show_panel_teach_dichotomy(self)
				
		self.new_question(self)


		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 0, 0)
		if self.v_question_type=='write':self.e_answer.grab_focus()
		
		
		self.surface.show_all()
		#self.window.add(self.surface)
		self.surface.show_all()
		self.But1=gtk.Button(42*'\n')
		self.But2=gtk.Button(42*'\n')
		self.But3=gtk.Button()
		self.But4=gtk.Button(250*' ')
		
		self.But1.show()
		self.But2.show()
		self.But3.show()
		self.But4.show()
		self.But1.set_relief(gtk.RELIEF_NONE)
		self.But2.set_relief(gtk.RELIEF_NONE)
		self.But3.set_relief(gtk.RELIEF_NONE)
		self.But4.set_relief(gtk.RELIEF_NONE)
		self.VBoxMainSurface = gtk.VBox()
		self.HBoxMainSurface=gtk.HBox()
		self.HBoxMainSurfaceInto=gtk.HBox()
		self.VBoxMainSurface.show()
		self.HBoxMainSurface.show()
		self.HBoxMainSurfaceInto.show()
		
		self.HBoxMainSurfaceInto.pack_start(self.But1, False, False, 0)
		self.HBoxMainSurfaceInto.pack_start(self.surface, True, True, 0)
		self.HBoxMainSurfaceInto.pack_start(self.But2, False, False, 0)
		
		self.VBoxMainSurface.pack_start(self.But3, False, True, 0)
		self.VBoxMainSurface.pack_start(self.HBoxMainSurfaceInto, True, False, 0)
		self.VBoxMainSurface.pack_start(self.But4, False, True, 0)
		
		#self.HBoxMainSurface.pack_start(self.But1, False, False, 0)
		self.HBoxMainSurface.pack_start(self.VBoxMainSurface, True, False, 0)
		#self.HBoxMainSurface.pack_start(self.But2, False, False, 0)
		style = self.window.get_style()
		style = self.window.modify_bg(gtk.STATE_NORMAL, gtk.gdk.Color(0, 0, 0))
		self.window.set_style(style)
		
		self.window.add(self.HBoxMainSurface)
		self.window.show()
		
##########################
#Pictures of Rome
##########################

		self.window2 = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window2.set_title(GD.titleMenuWindow)
		self.window2.set_size_request(GD.sizeWindow[0],GD.sizeWindow[1])
		self.window2.connect("destroy", lambda wid: gtk.main_quit())
		self.window2.connect("delete_event", lambda a1,a2:gtk.main_quit())
		self.window2.set_icon_from_file("./data/pic/log_in.png")
		
		self.VBoxRome=gtk.VBox()
		self.VBoxRome.show()


		
		self.color = self.window2.get_colormap().alloc_color(110, 110, 110)
		self.window2.modify_bg(gtk.STATE_NORMAL, self.color)

		self.imageRome = gtk.Image()
		self.imageRome.set_from_file('./data/pic/Rome/1.jpg')
		self.imageRome.show()
		self.VBoxRome.pack_start(self.imageRome,False, False, 10)
		
		self.sentenceLabel=gtk.Label()
		self.sentenceLabel.show()
		self.VBoxRome.pack_start(self.sentenceLabel,False, False, 10)
		
		self.b_exit_pic = gtk.Button('OK')
		self.b_exit_pic.connect("clicked", self.exitPictureDialog)
		self.b_exit_pic.set_relief(gtk.RELIEF_HALF)
		#self.tooltips.set_tip(self.b_exit_pic, """Exit""")
		self.b_exit_pic.show()
		self.b_exit_pic.enter()

		self.VBoxRome.pack_start(self.b_exit_pic,False, False, 20)
		
		self.window2.add(self.VBoxRome)
		self.window2.fullscreen()
		
	def show_panel_teach_dichotomy(self, data=None):
		
		
		box1 = gtk.HBox(False, 0)
		box1.set_border_width(2)
		self.image_answer = gtk.Image()
		self.image_answer.set_from_file("./data/pic/add_user.png")
		self.label_answer = gtk.Label('Start')
		box1.pack_start(self.image_answer, False, False, 3)
		box1.pack_start(self.label_answer, False, False, 3)
		self.b_anser_box = gtk.Button()
		self.b_anser_box.connect("clicked", self.hideAnswerBox)
		self.b_anser_box.set_relief(gtk.RELIEF_HALF)
		self.b_anser_box.add(box1)
		child = gtk.EventBox()
		child.add(self.b_anser_box)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 300,400)
		
		
		question = gtk.EventBox()
		question.show()
		self.l_question = gtk.Label("")
		self.l_question.set_markup("<big><b></b></big>");
		self.l_question.set_line_wrap(True)
		question.add(self.l_question)
		self.l_question.show()
		question.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(question)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,50)
		
		answer1 = gtk.EventBox()
		answer1.show()
		self.label1 = gtk.Label("---")
		answer1.add(self.label1)
		self.label1.show()
		answer1.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer1.connect("button_press_event", self.check_answer, 1)
		answer1.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer1)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,150)

		answer2 = gtk.EventBox()
		answer2.show()
		self.label2 = gtk.Label("---")
		answer2.add(self.label2)
		self.label2.show()
		answer2.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer2.connect("button_press_event", self.check_answer, 0)
		answer2.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer2)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,200)
		
		
	def show_panel_teach_write(self, data=None):
		
		box1 = gtk.HBox(False, 0)
		box1.set_border_width(2)
		self.image_answer = gtk.Image()
		self.image_answer.set_from_file("./data/pic/add_user.png")
		self.label_answer = gtk.Label('Start')
		box1.pack_start(self.image_answer, False, False, 3)
		box1.pack_start(self.label_answer, False, False, 3)
		self.b_anser_box = gtk.Button()
		self.b_anser_box.connect("clicked", self.hideAnswerBox)
		self.b_anser_box.set_relief(gtk.RELIEF_HALF)
		self.b_anser_box.add(box1)
		child = gtk.EventBox()
		child.add(self.b_anser_box)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 300,400)
		
		
		question = gtk.EventBox()
		question.show()
		self.l_question = gtk.Label("")
		self.l_question.set_markup("<big><b></b></big>");
		self.l_question.set_line_wrap(True)
		question.add(self.l_question)
		self.l_question.show()
		question.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(question)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,50)
		
		self.e_answer = gtk.Entry()
		self.e_answer.show()
		self.e_answer.set_max_length(80)
		self.e_answer.select_region(0,0)
		self.e_answer.connect("activate", self.check_answer, self.e_answer)
		child = gtk.EventBox()
		child.add(self.e_answer)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,200)	

		
	def show_panel_teach_choos(self, data=None):


		box1 = gtk.HBox(False, 0)
		box1.set_border_width(2)
		self.image_answer = gtk.Image()
		self.image_answer.set_from_file("./data/pic/add_user.png")
		self.label_answer = gtk.Label('Start')
		box1.pack_start(self.image_answer, False, False, 3)
		box1.pack_start(self.label_answer, False, False, 3)
		self.b_anser_box = gtk.Button()
		self.b_anser_box.connect("clicked", self.hideAnswerBox)
		self.b_anser_box.set_relief(gtk.RELIEF_HALF)
		self.b_anser_box.add(box1)
		child = gtk.EventBox()
		child.add(self.b_anser_box)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 300,400)
		
		
		question = gtk.EventBox()
		question.show()
		self.l_question = gtk.Label("")
		self.l_question.set_markup("<big><b></b></big>");
		self.l_question.set_line_wrap(True)
		question.add(self.l_question)
		self.l_question.show()
		question.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(question)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,50)
		
		answer1 = gtk.EventBox()
		answer1.show()
		self.label1 = gtk.Label("---")
		answer1.add(self.label1)
		self.label1.show()
		answer1.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer1.connect("button_press_event", self.check_answer, 0)
		answer1.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer1)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,150)

		answer2 = gtk.EventBox()
		answer2.show()
		self.label2 = gtk.Label("---")
		answer2.add(self.label2)
		self.label2.show()
		answer2.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer2.connect("button_press_event", self.check_answer, 1)
		answer2.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer2)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,200)
		
		answer3 = gtk.EventBox()
		answer3.show()
		self.label3 = gtk.Label("---")
		answer3.add(self.label3)
		self.label3.show()
		answer3.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer3.connect("button_press_event", self.check_answer, 2)
		answer3.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer3)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,250)
		
		answer4 = gtk.EventBox()
		answer4.show()
		self.label4 = gtk.Label("---")
		answer4.add(self.label4)
		self.label4.show()
		answer4.set_events(gtk.gdk.BUTTON_PRESS_MASK)
		answer4.connect("button_press_event", self.check_answer,3)
		answer4.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse(GD.backColour[GD.backPic]))
		child = gtk.EventBox()
		child.add(answer4)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 100,300)
		
		self.i_arrow1 = gtk.Image()
		self.i_arrow1.set_from_file('./data/pic/arrow.png')
		child = gtk.EventBox()
		child.add(self.i_arrow1)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 80, 156)
		
		self.i_arrow2 = gtk.Image()
		self.i_arrow2.set_from_file('./data/pic/arrow.png')
		child = gtk.EventBox()
		child.add(self.i_arrow2)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 80, 206)
		
		self.i_arrow3 = gtk.Image()
		self.i_arrow3.set_from_file('./data/pic/arrow.png')
		child = gtk.EventBox()
		child.add(self.i_arrow3)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 80, 256)
		
		self.i_arrow4 = gtk.Image()
		self.i_arrow4.set_from_file('./data/pic/arrow.png')
		child = gtk.EventBox()
		child.add(self.i_arrow4)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_TOP, 80, 306)
		
	def show_panel_stats(self, data=None):


		self.b_LessonName = gtk.Button(GD.UILesNr+str(GD.teachLesson+1))
		self.b_LessonName.connect("clicked", self.dialog_lesson_name)
		self.b_LessonName.set_relief(gtk.RELIEF_HALF)
		image = gtk.Image()
		image.set_from_file("./data/pic/lesson_name.png")
		self.b_LessonName.set_image(image)
		#self.tooltips.set_tip(self.b_LessonName, GD.UILessonName)
		self.b_LessonName.show()
		self.b_LessonName.enter()
		child = gtk.EventBox()
		child.add(self.b_LessonName)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 900,410)
		
		self.b_help = gtk.Button()
		self.b_help.connect("clicked", self.dialog_help)
		self.b_help.set_relief(gtk.RELIEF_HALF)
		image = gtk.Image()
		image.set_from_file("./data/pic/help3.png")
		self.b_help.set_image(image)
		#self.tooltips.set_tip(self.b_help, GD.UIHelp)
		self.b_help.show()
		self.b_help.enter()
		child = gtk.EventBox()
		child.add(self.b_help)
		self.b_help.grab_focus()
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 900,50)
		
		self.b_exit = gtk.Button()
		self.b_exit.connect("clicked", self.exit)
		self.b_exit.set_relief(gtk.RELIEF_HALF)
		image = gtk.Image()
		image.set_from_file("./data/pic/exit3.png")
		self.b_exit.set_image(image)
		#self.tooltips.set_tip(self.b_exit, GD.UIExit)
		self.b_exit.show()
		self.b_exit.enter()
		child = gtk.EventBox()
		child.add(self.b_exit)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 940,50)
		
		
		self.b_done = gtk.Button()
		self.b_done.connect("clicked", self.dialog_done)
		self.b_done.set_relief(gtk.RELIEF_HALF)
		self.i_done = gtk.Image()
		self.i_done.set_from_file("./data/pic/done.png")
		self.b_done.set_label(GD.UIDone20)
		self.b_done.set_image(self.i_done)
		#self.tooltips.set_tip(self.b_done, GD.UIDone)
		self.b_done.show()
		self.b_done.enter()
		child = gtk.EventBox()
		child.add(self.b_done)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 900,260)
		
		self.b_stats = gtk.Button()
		self.b_stats.connect("clicked", self.dialog_stats)
		self.b_stats.set_relief(gtk.RELIEF_HALF)
		self.i_stats = gtk.Image()
		self.i_stats.set_from_file("./data/pic/stats.png")
		self.b_stats.set_label(GD.UIStats0)
		self.b_stats.set_image(self.i_stats)
		#self.tooltips.set_tip(self.b_stats, GD.UIStatistics)
		self.b_stats.show()
		self.b_stats.enter()
		child = gtk.EventBox()
		child.add(self.b_stats)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 900,310)
		
		self.b_time = gtk.Button('')
		self.b_time.connect("clicked", self.dialog_time)
		self.b_time.set_relief(gtk.RELIEF_HALF)
		self.i_time = gtk.Image()
		self.i_time.set_from_file("./data/pic/time.png")
		self.b_time.set_label(GD.UITime0)
		self.b_time.set_image(self.i_time)
		#self.tooltips.set_tip(self.b_time, GD.UITime)
		self.b_time.show()
		self.b_time.enter()
		child = gtk.EventBox()
		child.add(self.b_time)
		self.surface.add_child_in_window(child, gtk.TEXT_WINDOW_BOTTOM, 900,360)
		
		
	def time(self, data=None):
		self.b_time.set_label(GD.UITime1)

	def exit(self, data=None, off=None):
		self.window.hide()
		GD.mainWindow.window.show()
		

	def check_answer(self, data=None, off=None, on=None):
		
		try:
			if self.e_answer.get_text()=='': return();
		except:
			self.write_answer=' '
			pass
		#Przechwytywanie odpowiedzi lesji nie jest w parametrze
		if self.v_question_type=='write' and self.answer!=None:
			self.write_answer=on=self.e_answer.get_text()
			self.e_answer.set_text('')
			if GD.teachMaterial=='words':
				if str(on) in split(self.answer,','):
					on=self.answer=str('ok')
		#if self.v_question_type=='choose' and self.answer!=None:
		#	on=on
		#Odswierzanie statystyk
		
			
		if self.answer!=None:
			self.v_try_time+=1
			if self.answer==str(on):
				self.v_done1+=1
				self.v_stats1+=1
				self.v_stats2=self.v_stats1*100/self.v_try_time
				self.v_time2=int(time()-self.v_time_start)
				self.v_time1=int(self.v_time2/self.v_try_time)
				if GD.teachMaterial=='words':
					GD.tabelQuest.remove(GD.numberQuest)
				self.v_questions_number=self.v_done1
				self.image_answer.set_from_file('./data/pic/add_user.png')
				self.dialog_answer(GD.UIGood)
			else:
				self.v_stats2=self.v_stats1*100/self.v_try_time
				self.v_time1=int(self.v_time2/self.v_try_time)
				self.v_time2=int(time()-self.v_time_start)
				self.v_questions_number=self.v_done1
				if GD.teachMaterial=='grammar':
					#self.v_done1+=1
					if self.v_generation_ret[6]=='Yes':self.dialog_answer(GD.UISorryBadAnswer+' <b>'+GD.UIYes+'</b>');self.image_answer.set_from_file('./data/pic/delete_user.png');
					elif self.v_generation_ret[6]=='No':self.dialog_answer(GD.UISorryBadAnswer+' <b>'+GD.UINo+'</b>');self.image_answer.set_from_file('./data/pic/delete_user.png');
					else: self.dialog_answer('<small>('+self.write_answer+')</small> '+GD.UISorryBadAnswer+'<b>'+self.v_generation_ret[6]+'</b>');self.image_answer.set_from_file('./data/pic/delete_user.png');
				if GD.teachMaterial=='words':
					if self.v_generation_ret[6]=='Yes':self.dialog_answer(GD.UISorryBadAnswer+' <b>'+GD.UIYes+'</b>');self.image_answer.set_from_file('./data/pic/delete_user.png');
					elif self.v_generation_ret[6]=='No':self.dialog_answer(GD.UISorryBadAnswer+' <b>'+GD.UINo+'</b>');self.image_answer.set_from_file('./data/pic/delete_user.png');
					else:self.dialog_answer('<small>('+self.write_answer+')</small> '+GD.UISorryBadAnswer+' <b>'+self.v_generation_ret[8]+'</b>');self.image_answer.set_from_file('./data/pic/delete_user.png');

		
	def new_question(self, data=None, off=None, on=None):
		self.b_done.set_label (GD.UIDone+'\n'+str(self.v_done1)+'/'+str(self.v_done2))
		#self.b_stats.set_label("Stats\n"+str(self.v_stats1)+'/'+str(self.v_stats2))
		self.b_stats.set_label(GD.UIStats+'\n'+str(self.v_stats2)+'%')
		self.b_time.set_label (GD.UITime+' \n'+str(self.v_time1)+'/'+str(self.v_time2))
		no_exit=1
		
		
		if self.v_questions_number==GD.longTeach and self.v_question_type=='dichotomy':
			GD.statsTeachWindow[0]=[self.v_done1,self.v_done2,self.v_stats1,self.v_stats2,self.v_time1,self.v_time2]
			self.window.hide()
			self.__init__(data='choose',quest=self.v_quest, lesson_name=self.lesson_name)
		elif self.v_questions_number==GD.longTeach and self.v_question_type=='choose':
			GD.statsTeachWindow[1]=[self.v_done1,self.v_done2,self.v_stats1,self.v_stats2,self.v_time1,self.v_time2]
			self.window.hide()
			self.__init__(data='write',quest=self.v_quest, lesson_name=self.lesson_name)
		elif self.v_questions_number==GD.longTeach and self.v_question_type=='write':
			GD.statsTeachWindow[2]=[self.v_done1,self.v_done2,self.v_stats1,self.v_stats2,self.v_time1,self.v_time2]
			saveStats(GD.statsTeachWindow)
			GD.mainWindow.refreshTabel()
			no_exit=0
			self.exit()
		else:
			self.v_questions_number+=1
			
		if no_exit:
			#data=[pytanie,od1,od2,od3,od4,odp]
			#Ustawianie labeli na pytanie
			if self.v_question_type=='choose':
				data=self.generation(question_type='choose')
				if GD.teachMaterial=='grammar':
					self.l_question.set_markup('<big><b>'+data[0]+'</b></big>\n<small>'+data[7]+' - '+data[8]+'</small>');
				if GD.teachMaterial=='words':
					self.l_question.set_markup('<big><b>'+data[0]+'</b></big>\n');
				self.label1.set_label(data[1])
				self.label2.set_label(data[2])
				self.label3.set_label(data[3])
				self.label4.set_label(data[4])
				self.answer=data[5]
			if self.v_question_type=='write':
				data=self.generation('write')
				self.answer=data[5]
				if GD.teachMaterial=='grammar':
					self.l_question.set_markup('<big><b>'+data[0]+'</b></big>\n<small>'+data[7]+' - '+data[8]+'</small>');
				if GD.teachMaterial=='words':
					self.l_question.set_markup('<big><b>'+data[0]+'</b></big>\n');

				self.e_answer.set_text('')
			if self.v_question_type=='dichotomy':
				data=self.generation('dichotomy')
				self.answer=data[5]
				if GD.teachMaterial=='grammar':
					self.l_question.set_markup('<big><b>'+data[0]+'</b></big>\n<small>'+data[7]+' - '+data[8]+'</small>');
				if GD.teachMaterial=='words':
					self.l_question.set_markup('<big><b>'+data[0]+'</b></big>\n');

				self.label1.set_markup('<b>'+GD.UIYes+'</b>')
				self.label2.set_markup('<b>'+GD.UINo+'</b>')
			#licza pytan

	def key_press_event_cb(self, widget, event, data=None):
		
		#if event.state & CONTROL_MASK and event.state & SHIFT_MASK:
		if self.v_question_type=='choose' and not self.answer_button:
			if event.keyval == gtk.keysyms._1:
				self.check_answer(on=0)
			if event.keyval == gtk.keysyms._2:
				self.check_answer(on=1)
			if event.keyval == gtk.keysyms._3:
				self.check_answer(on=2)
			if event.keyval == gtk.keysyms._4:
				self.check_answer(on=3)
		if self.v_question_type=='dichotomy' and not self.answer_button:
			if event.keyval == gtk.keysyms._1:
				self.check_answer(on=1)
			if event.keyval == gtk.keysyms._2:
				self.check_answer(on=0)
		if self.answer_button:
			if event.keyval == gtk.keysyms.Return:
				self.hideAnswerBox()


	def generation(self,question_type=None):
		#self.v_quest=['V','1','Presentis','Indicativus','Activi']
		#self.v_generation_ret=[que=question,ans1,ans2,ans3,ans4,ansRight...
		if self.v_quest[0]=='N':
			right_generation=1
			while right_generation:
				self.v_gram_ret=grammatic.noun_declination(number=grammatic.random_question(part='N',declination=int(self.v_quest[1])))
				if len(self.v_gram_ret)>2:right_generation=0;
			self.v_quest_tab=self.v_gram_ret[0]
			self.v_number_line=self.v_gram_ret[1]
			self.v_number_inflection=self.v_gram_ret[2]
			self.v_lat_contest=(xml_read_line(file_name=GD.xml_file_lat_name ,contest='shank',number=self.v_gram_ret[1]))
			self.v_eng_contest=(xml_read_line(file_name=GD.xml_file_eng_name ,contest='shank',number=self.v_gram_ret[1]))
			if self.v_quest[0]=='N':
				if question_type=='choose':
					ans_1=(random.randint(0,11))#odp1
					go_search=1
					while go_search:
						ans_2=(random.randint(0,11))#odp2
						go_search+=1
						if go_search==100:go_search=0
						if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_2]:go_search=0
					go_search=1
					while go_search:
						ans_3=(random.randint(0,11))#odp2
						go_search+=1
						if go_search==100:go_search=0
						if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_3]:
							if self.v_quest_tab[ans_2]!=self.v_quest_tab[ans_3]:go_search=0
					go_search=1
					while go_search:
						ans_4=(random.randint(0,11))#odp2
						go_search+=1
						if go_search==100:go_search=0
						if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_4]:
							if self.v_quest_tab[ans_2]!=self.v_quest_tab[ans_4]:
								if self.v_quest_tab[ans_3]!=self.v_quest_tab[ans_4]:go_search=0
					ans_r=(random.randint(0,3))#pytanie
					que=[ans_1,ans_2,ans_3,ans_4][ans_r]#odp
					self.v_generation_ret=([self.v_noun_tab[que]+' '+self.v_quest_tab[0]+', '+self.v_quest_tab[1] ,self.v_quest_tab[ans_1],self.v_quest_tab[ans_2],self.v_quest_tab[ans_3],self.v_quest_tab[ans_4],str(ans_r),self.v_quest_tab[que],self.v_lat_contest,self.v_eng_contest])
				if question_type=='write':
					que=ans_r=(random.randint(0,11))#pytanie#odp
					self.v_generation_ret=([self.v_noun_tab[que]+' '+self.v_quest_tab[0]+', '+self.v_quest_tab[1] ,'','','','',self.v_quest_tab[ans_r],self.v_quest_tab[ans_r],self.v_lat_contest,self.v_eng_contest])
				if question_type=='dichotomy':
					que0=(random.randint(0,11))#pytanie#odp
					que=(random.randint(0,11))#pytanie#odp
					queTab=[que0,que]
					ans_1=queTab[(random.randint(0,1))]
					if self.v_quest_tab[ans_1]==self.v_quest_tab[que]:
						ans_r=1
						ans_rt='Yes'
					else:
						ans_r=0
						ans_rt='No'
					self.v_generation_ret=([self.v_noun_tab[que]+': '+self.v_quest_tab[ans_1],'','','','',str(ans_r),ans_rt,self.v_lat_contest,self.v_eng_contest])

		if self.v_quest[0]=='ADJ':
			self.v_adj_dec=self.v_quest[1]
			right_generation=1
			while right_generation:
				self.v_gram_ret=grammatic.adjective_declination(number=grammatic.random_question(part='ADJ',declination=int(self.v_quest[1])))
				if len(self.v_gram_ret)>2:right_generation=0;
			self.v_quest_tab=self.v_gram_ret[0]
			self.v_number_line=self.v_gram_ret[1]
			self.v_number_inflection=self.v_gram_ret[2]
			self.v_lat_contest=(xml_read_line(file_name=GD.xml_file_lat_name ,contest='shank',number=self.v_gram_ret[1]))
			self.v_eng_contest=(xml_read_line(file_name=GD.xml_file_eng_name ,contest='shank',number=self.v_gram_ret[1]))
			if self.v_adj_dec==str(1):v_rnd=[12,23]
			if self.v_adj_dec==str(2):v_rnd=[[0,11],[24,35]][random.randint(0,1)]
			if self.v_adj_dec==str(3):v_rnd=[0,35]

			if question_type=='choose':
				ans_1=(random.randint(v_rnd[0],v_rnd[1]))#odp1
				go_search=1
				while go_search:
					ans_2=(random.randint(v_rnd[0],v_rnd[1]))#odp2
					go_search+=1
					if go_search==100:go_search=0
					if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_2]:go_search=0
				go_search=1
				while go_search:
					ans_3=(random.randint(v_rnd[0],v_rnd[1]))#odp2
					go_search+=1
					if go_search==100:go_search=0
					if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_3]:
						if self.v_quest_tab[ans_2]!=self.v_quest_tab[ans_3]:go_search=0
				go_search=1
				while go_search:
					ans_4=(random.randint(v_rnd[0],v_rnd[1]))#odp2
					go_search+=1
					if go_search==100:go_search=0
					if self.v_quest_tab[ans_1]!=self.v_quest_tab[ans_4]:
						if self.v_quest_tab[ans_2]!=self.v_quest_tab[ans_4]:
							if self.v_quest_tab[ans_3]!=self.v_quest_tab[ans_4]:go_search=0
				ans_r=(random.randint(0,3))#pytanie
				que=[ans_1,ans_2,ans_3,ans_4][ans_r]#odp
				if que<12:v_kind='masculum'
				if que>11 and que<24:v_kind='femininum'
				if que>23 and que<36:v_kind='neutrum'
				self.v_generation_ret=([self.v_adjective_tab[que]+' ('+v_kind+') '+self.v_quest_tab[0],self.v_quest_tab[ans_1],self.v_quest_tab[ans_2],self.v_quest_tab[ans_3],self.v_quest_tab[ans_4],str(ans_r),self.v_quest_tab[que],self.v_lat_contest,self.v_eng_contest])
	
			if question_type=='write':
				que=ans_r=(random.randint(v_rnd[0],v_rnd[1]))#pytanie#odp
				if que<12:v_kind='masculum'
				if que>11 and que<24:v_kind='femininum'
				if que>23 and que<36:v_kind='neutrum'
				self.v_generation_ret=([self.v_adjective_tab[que]+' ('+v_kind+') '+self.v_quest_tab[0],'','','','',self.v_quest_tab[ans_r],self.v_quest_tab[ans_r],self.v_lat_contest,self.v_eng_contest])

			if question_type=='dichotomy':
				que0=(random.randint(v_rnd[0],v_rnd[1]))#pytanie#odp
				que=(random.randint(v_rnd[0],v_rnd[1]))#pytanie#odp
				queTab=[que0,que]
				ans_1=queTab[random.randint(0,1)]
				if que<12:v_kind='masculum'
				if que>11 and que<24:v_kind='femininum'
				if que>23 and que<36:v_kind='neutrum'
				if self.v_quest_tab[ans_1]==self.v_quest_tab[que]:
					ans_r=1
					ans_rt='Yes'
				else:
					ans_r=0
					ans_rt='No'
				self.v_generation_ret=([self.v_adjective_tab[que]+' ('+v_kind+')'+': '+self.v_quest_tab[ans_1],'','','','',str(ans_r),ans_rt,self.v_lat_contest,self.v_eng_contest])

		if self.v_quest[0]=='V':
			self.v_v_con=self.v_quest[1]
			right_generation=1
			while right_generation:
				self.v_gram_ret=grammatic.verb_coniugation(number=grammatic.random_question(part='V',coniugation=self.v_v_con))
				if len(self.v_gram_ret)>2:right_generation=0;
			self.v_quest_tab=self.v_gram_ret[0]
			
			self.v_number_line=self.v_gram_ret[1]
			self.v_number_inflection=self.v_gram_ret[2]
			self.v_lat_contest=(xml_read_line(file_name=GD.xml_file_lat_name ,contest='shank',number=self.v_number_line))
			self.v_eng_contest=(xml_read_line(file_name=GD.xml_file_eng_name ,contest='shank',number=self.v_number_line))

			if self.v_quest[2]=='Praesentis':self.v_quest_tab=self.v_quest_tab[0]
			if self.v_quest[2]=='Imperfecti':self.v_quest_tab=self.v_quest_tab[1]
			if self.v_quest[2]=='Futuri I':self.v_quest_tab=self.v_quest_tab[2]
			if self.v_quest[2]=='Perfecti':self.v_quest_tab=self.v_quest_tab[3]
			if self.v_quest[2]=='Plusquam-perfecti':self.v_quest_tab=self.v_quest_tab[4]
			if self.v_quest[2]=='Futuri exacti':self.v_quest_tab=self.v_quest_tab[5]

			if self.v_quest[3]=='Activi':self.v_quest_tab=self.v_quest_tab[0:30]
			if self.v_quest[3]=='Passivi':self.v_quest_tab=self.v_quest_tab[30:60]
	
			if question_type=='dichotomy':

				while 1:
					que0=(random.randint(0,len(self.v_quest_tab)-1))#pytanie#odp
					if self.v_quest_tab[que0]!='-':break
					
				while 1:
					que=(random.randint(0,len(self.v_quest_tab)-1))
					if self.v_quest_tab[que]!='-':break
					
				self.modus_que=GD.modus_tab[que/6]
				queTab=[que0,que]
				ans_1=queTab[random.randint(0,1)]
				#print self.v_quest_tab
				if self.v_quest_tab[ans_1]==self.v_quest_tab[que]:
					ans_r=1
					ans_rt='Yes'
				else:
					ans_r=0
					ans_rt='No'

				if self.v_quest_tab[que]=='-': self.v_generation_ret=([self.modus_que+' '+self.v_quest[2]+' '+self.v_quest[3]+' '+self.v_verb_tab[que]+': \nform not exist','','','','',str(ans_r),ans_rt,self.v_lat_contest,self.v_eng_contest])
				else: self.v_generation_ret=([self.modus_que+' '+self.v_quest[2]+' '+self.v_quest[3]+' '+self.v_verb_tab[que]+': \n'+self.v_quest_tab[ans_1],'','','','',str(ans_r),ans_rt,self.v_lat_contest,self.v_eng_contest])


			if question_type=='choose':

				while 1:
					ans_rr=que=(random.randint(0,len(self.v_quest_tab)-1))
					str_ans_0=self.v_quest_tab[ans_rr]
					self.modus_que=GD.modus_tab[que/6]
					str_ans_0=self.v_quest_tab[que]#odp2
					if str_ans_0 not in ('-','-'):break
				while 1:
					str_ans_1=self.v_quest_tab[random.randint(0,len(self.v_quest_tab)-1)]#odp2
					if str_ans_1 not in (str_ans_0,'-'):break
				while 1:
					str_ans_2=self.v_quest_tab[random.randint(0,len(self.v_quest_tab)-1)]#odp2
					if str_ans_2 not in (str_ans_0,str_ans_1,'-'):break
				while 1:
					str_ans_3=self.v_quest_tab[random.randint(0,len(self.v_quest_tab)-1)]#odp2
					if str_ans_3 not in (str_ans_0,str_ans_1,str_ans_2,'-'):break
				while 1:
					str_ans_4=self.v_quest_tab[random.randint(0,len(self.v_quest_tab)-1)]#odp2
					if str_ans_4 not in (str_ans_0,str_ans_1,str_ans_2,str_ans_3,'-'):break
					
				ans_r=random.randint(0,3)#pytanie
				if ans_r==0:str_ans_1=str_ans_0
				if ans_r==1:str_ans_2=str_ans_0
				if ans_r==2:str_ans_3=str_ans_0
				if ans_r==3:str_ans_4=str_ans_0
					

				self.v_generation_ret=([self.modus_que+' '+self.v_quest[2]+' '+self.v_quest[3]+' '+self.v_verb_tab[que],str_ans_1,str_ans_2,str_ans_3,str_ans_4,str(ans_r),self.v_quest_tab[que],self.v_lat_contest,self.v_eng_contest])
	
			if question_type=='write':
				while 1:
					ans_r=random.randint(0,len(self.v_quest_tab)-1)#odp2
					if self.v_quest_tab[ans_r] not in ('-','-'):break
					
				que=ans_r
				self.modus_que=GD.modus_tab[que/6]
				self.v_generation_ret=([self.modus_que+' '+self.v_quest[2]+' '+self.v_quest[3]+' '+self.v_verb_tab[ans_r],'','','','',self.v_quest_tab[ans_r],self.v_quest_tab[ans_r],self.v_lat_contest,self.v_eng_contest])

		if self.v_quest[0]=='W':
			self.v_les=int(self.v_quest[1])
			self.v_rnd=[(self.v_les-1)*20+1,self.v_les*20+1]
			self.v_number=random.randint(self.v_rnd[0],self.v_rnd[1])
			self.v_line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=self.v_number))
			a=xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=10)
			self.v_split_line=xml_split_line(line=self.v_line, baze='triBaze')
			self.v_lat_contest=self.v_split_line[0]
			self.v_eng_contest=self.v_split_line[1]
			self.v_pol_contest=self.v_split_line[2]
			if question_type=='dichotomy':
				ans_0=ans_1=GD.numberQuest=(random.choice(GD.tabelQuest))#odp1
				if random.randint(0,1):ans_1=(random.choice(GD.temporaryTabelQuest))

				if ans_0==ans_1:
					ans_r=1
					ans_rt='Yes'
				else:
					ans_r=0
					ans_rt='No'#self.v_lat_contest[ans_0]
				self.v_split_line_0=xml_split_line(line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=ans_0)), baze='triBaze')[0]
				self.v_split_line_1=xml_split_line(line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=ans_1)), baze='triBaze')[GD.langTeach]
				self.v_generation_ret=([addCommaSpace(self.v_split_line_0)+':\n\n<small><small>'+addCommaSpace(self.v_split_line_1)+'</small></small>','','','','',str(ans_r),ans_rt,'',''])
				
			if question_type=='choose':
				ans_0=(random.choice(GD.tabelQuest))#odp1
				while 1:
					ans_1=(random.choice(GD.temporaryTabelQuest))#odp2
					if ans_1 not in (ans_0,ans_0):break
				while 1:
					ans_2=(random.choice(GD.temporaryTabelQuest))#odp2
					if ans_2 not in (ans_0,ans_1):break
				while 1:
					ans_3=(random.choice(GD.temporaryTabelQuest))#odp2
					if ans_3 not in (ans_0,ans_1,ans_2):break
				while 1:
					ans_4=(random.choice(GD.temporaryTabelQuest))#odp2
					if ans_4 not in (ans_0,ans_1,ans_2,ans_3):break
					
				ans_r=random.randint(0,3)#pytanie
				if ans_r==0:ans_1=ans_0
				if ans_r==1:ans_2=ans_0
				if ans_r==2:ans_3=ans_0
				if ans_r==3:ans_4=ans_0
				GD.numberQuest=ans_0
				ans_0_str=xml_split_line(line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=ans_0)), baze='triBaze')[0]
				ans_1_str=xml_split_line(line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=ans_1)), baze='triBaze')[GD.langTeach]
				ans_2_str=xml_split_line(line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=ans_2)), baze='triBaze')[GD.langTeach]
				ans_3_str=xml_split_line(line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=ans_3)), baze='triBaze')[GD.langTeach]
				ans_4_str=xml_split_line(line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=ans_4)), baze='triBaze')[GD.langTeach]
				ans_r_str=xml_split_line(line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=ans_0)), baze='triBaze')[GD.langTeach]
				self.v_generation_ret=([addCommaSpace(ans_0_str),addCommaSpace(ans_1_str),addCommaSpace(ans_2_str),addCommaSpace(ans_3_str),addCommaSpace(ans_4_str),str(ans_r),'','',ans_r_str])

			if question_type=='write':
				ans_0=ans_1=(random.choice(GD.tabelQuest))#odp1
				GD.numberQuest=ans_0
				self.v_split_line_0=xml_split_line(line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=ans_0)), baze='triBaze')
				self.v_split_line_1=xml_split_line(line=(xml_read_line(file_name=GD.xml_file_word_teach_base,contest='all',number=ans_1)), baze='triBaze')
				self.v_generation_ret=([addCommaSpace(self.v_split_line_0[0]),'','','','',self.v_split_line_1[GD.langTeach],self.v_split_line_1[GD.langTeach],'',self.v_split_line_0[GD.langTeach]])

		return(self.v_generation_ret)
			
	def dialog_done(self,data=None):
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup(GD.UIDoneQuestions+str(self.v_done1)+'\n'+GD.UIAllQuestions+str(self.v_done2))
		dialog.run()
		dialog.destroy()
		
	def dialog_stats(self,data=None):
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup(GD.UINumberRightAnswers+str(self.v_stats1)+'\n'+GD.UIPercentageRightAnswers+str(self.v_stats2)+' %')
		dialog.run()
		dialog.destroy()
		
	def dialog_time(self,data=None):
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup(GD.UITimeRequestLastQuestion+str(self.v_time1)+' [s]\n'+GD.UIMeanTimeRequest+str(self.v_time2)+' [s]')
		dialog.run()
		dialog.destroy()
		
	def dialog_answer(self,data=None):
		#dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		#dialog.set_markup(data)
		#dialog.run()
		try: self.l_question.hide()
		except: pass
		try: self.label1.hide()
		except: pass
		try: self.label2.hide()
		except: pass
		try: self.label3.hide()
		except: pass
		try: self.label4.hide()
		except: pass
		try: self.i_arrow1.hide()
		except: pass
		try: self.i_arrow2.hide()
		except: pass
		try: self.i_arrow3.hide()
		except: pass
		try: self.i_arrow4.hide()
		except: pass
		try: self.e_answer.hide()
		except: pass

		self.answer_button=True
		self.b_anser_box.show()
		#print dir(self)
		#self.b_anser_box.do_focus(1)
		self.label_answer.set_markup(self.l_question.get_text()[:80]+'...\n\n'+data)
		
		if self.v_try_time%GD.relaxSenFreq==0 and GD.relaxSentece:
			self.window.hide()
			self.showSentence()
		if self.v_try_time%GD.relaxPicFreq==0 and GD.relaxGraphics:
			self.window.hide()
			self.showPicturesOfRome()
		self.new_question()
		#dialog.destroy()
		
	def dialog_help(self,data=None):
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup(self.lesson_name)
		dialog.run()
		#self.new_question()
		dialog.destroy()
		
	def dialog_lesson_name(self,data=None):
		if GD.teachMaterial=='grammar':data=GD.UIGramaticLessonNumber+str(GD.teachLesson+1)+'\n'+GD.v_lessons_name[GD.teachLesson]
		elif GD.teachMaterial=='words':data=GD.UIWordsLessonNumber+str(GD.teachLesson+1)+'\n'+GD.v_lessons_name_1[GD.teachLesson]
		dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
		dialog.set_markup(data)
		dialog.run()
		dialog.destroy()

	def hideAnswerBox(self, data=None):
		
		self.answer_button=False
		self.b_anser_box.hide()
		
		try: self.l_question.show()
		except: pass
		try: self.label1.show()
		except: pass
		try: self.label2.show()
		except: pass
		try: self.label3.show()
		except: pass
		try: self.label4.show()
		except: pass
		try: self.i_arrow1.show()
		except: pass
		try: self.i_arrow2.show()
		except: pass
		try: self.i_arrow3.show()
		except: pass
		try: self.i_arrow4.show()
		except: pass
		try: 
			self.e_answer.show()
			self.e_answer.grab_focus()
		
		except: pass

		
	def showSentence(self, data=None):
		
		self.v_number_sen=random.randint(1,180)
		self.v_line_sen=(xml_read_line(file_name=GD.xml_file_sentence_base,contest='all',number=self.v_number_sen))
		start=self.v_line_sen.find('sen="')
		med=self.v_line_sen.find('-')
		end=self.v_line_sen.find('">')
		
		sent_lat=self.v_line_sen[start+5:med-1]
		sent_pol=self.v_line_sen[med+1:end-1]
		
		self.imageRome.hide()
		self.sentenceLabel.show()
		self.window2.show()
		self.sentenceLabel.set_markup('<span foreground="white" size="40000">\n\n\n\n'+sent_lat+'\n</span><span foreground="#dcdcdc" size="10000">'+sent_pol+'\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n</span>')
		
	def showPicturesOfRome(self, data=None):
		self.sentenceLabel.hide()
		self.imageRome.show()
		self.imageRome.set_from_file(GD.romePic[randint(0,len(GD.romePic)-1)])
		self.window2.show()
		
	def exitPictureDialog(self, data=None):
		self.window2.hide()
		self.window.show()

def saveStats(newStats):
#newStats [[20, 20, 20, 90, 1, 25], [20, 20, 20, 90, 3, 69], [20, 20, 20, 95, 2, 56]]
#out [0,80,80,285]

	question=newStats[0][0]*2
	answer=int(newStats[1][2]*(float(newStats[1][3])/100))+int(newStats[2][2]*(float(newStats[2][3])/100))
	timeTeach=newStats[0][5]+newStats[1][5]+newStats[2][5]
	GD.teachLesson
	ret=[]
	upgrade=0
	
	if GD.teachMaterial=='grammar':file = open(GD.statsFilePath+'gramatic_'+GD.userName+'.txt')
	elif GD.teachMaterial=='words':file = open(GD.statsFilePath+'words_'+GD.userName+'.txt')
	
	for line in file:
		ret.append(strip(line)[1:-1].replace('"','').split(','))
		if ret[-1][0]==str(GD.teachLesson):
			upgrade=True 
			ret[-1][1]=str(int(ret[-1][1])+answer)
			ret[-1][2]=str(int(ret[-1][2])+question)
			ret[-1][3]=str(int(ret[-1][3])+timeTeach)
	if not upgrade:ret.append([str(GD.teachLesson),str(answer),str(question),str(timeTeach)])
		
		
	file.close()

	if GD.teachMaterial=='grammar':file = open(GD.statsFilePath+'gramatic_'+GD.userName+'.txt','w')
	elif GD.teachMaterial=='words':file = open(GD.statsFilePath+'words_'+GD.userName+'.txt','w')

	for i in range(len(ret)):
		out=str(ret[i])
		out=replace(out,' ','')
		out=replace(out,"'",'')
		file.writelines(out+"\n")

	file.close()
	
def addCommaSpace(strIn=None):
	return(string.replace(strIn,',',', '))
	
def startTeachWindow():
	GD.m_w=Teach_Window(quest=['V','1','0','0','0'])
	gtk.main()

#startTeachWindow()
