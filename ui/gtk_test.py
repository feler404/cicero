def adjective_declination(form_m=None, form_f=None, form_n=None, number=None, info=None, declination=None):
    '''
    Deklinacja przymiotnikow

    '''
    
    dec_1f=['a','ae','ae','am','a','a','ae','arum','is','as','is','ae']
    dec_2m=['us','i','o','um','o','e','i','orum','is','os','is','i']
    dec_2n=['um','i','o','um','o','um','o','orum','is','a','is','a']
    dec_31m=['','is','i','em','i','','es','ium','ibus','es','ibus','es']#-er, -is, -e
    dec_31f=['is','is','i','em','i','is','es','ium','ibus','es','ibus','es']#-er, -is, -e
    dec_31n=['e','is','i','e','i','e','ia','ium','ibus','ia','ibus','ia']#-er, -is, -e
    dec_32m=['is','is','i','em','i','is','es','ium','ibus','es','ibus','es']#-is, -is, -e
    dec_32f=['is','is','i','em','i','is','es','ium','ibus','es','ibus','es']#-is, -is, -e
    dec_32n=['e','is','i','e','i','e','ia','ium','ibus','ia','ibus','ia']#-is, -is, -e
    dec_33m=['','is','i','em','i','','es','ium','ibus','es','ibus','es']#-x, -x, -x
    dec_33f=['','is','i','es','i','','es','ium','ibus','es','ibus','es']#-x, -x, -x
    dec_33n=['','is','i','','i','','ia','ium','ibus','ia','ibus','ia']#-x, -x, -x
    declination_return=[]
    declination=0
    if declination==2:declination=1
    
    if number!=None:
        ret_all=(xml_read_line(contest='all',number=number))
        start=find(ret_all,'lat="')+5
        end=find(ret_all,'"',start+1)
        shank=ret_all[start:end]
        start=find(ret_all,'info="')+6
        end=find(ret_all,'"',start+1)
        info=ret_all[start:end]
    else:
        return([-1])
        
    if info!=None:
        point=0
        while find(info,',',point)+1:
            point=find(info,',',point)
            info=info[:point]+"'"+info[point:]
            info=info[:point+2]+"'"+info[point+2:]
            point+=3
        info="['"+info[1:-1]+"']"
        exec ('info='+info)
    if  info[0]!='ADJ':
        return([-1])
    if form_m==None or form_f==None or form_n==None:
        point1=find(shank,',')
        point2=find(shank,',',point1+1)
        form_m=shank[:point1]
        form_f=shank[point1+2:point2]
        form_n=shank[point2+2:]
        if '-um' in form_f:
            form_f=form_f[:-4]
            form_n=form_f[:-5]+'um'
#Declination:1,2
    if form_m[-2:]=='us' and form_f[-1:]=='a' and form_n[-2:]=='um':
        declination=1.0
        for i in range(12):#m
            declination_return.append(form_m[:-2]+dec_2m[i])
        for i in range(12):#f
            declination_return.append(form_f[:-1]+dec_1f[i])
        for i in range(12):#n
            declination_return.append(form_n[:-2]+dec_2n[i])
    if form_f[-2:]=='er' and form_f[-1:]=='a' and form_f[-2:]=='um':
        declination=1.1
        for i in range(12):#m
            declination_return.append(form_m[:-2]+dec_2m[i])
        for i in range(12):#f
            declination_return.append(form_f[:-1]+dec_1f[i])
        for i in range(12):#n
            declination_return.append(form_n[:-2]+dec_2n[i])
#Declination:31
    if form_m[-2:]=='er' and form_f[-2:]=='is' and form_n[-1:]=='e':
        declination=3.1
        for i in range(12):#m
            declination_return.append(form_m+dec_31m[i])
        for i in range(12):#f
            declination_return.append(form_f[:-2]+dec_31f[i])
        for i in range(12):#n
            declination_return.append(form_n[:-1]+dec_31n[i])
#Declination:32
    if form_m[-2:]=='is' and form_f[-2:]=='is' and form_n[-1:]=='e':
        declination=3.2
        for i in range(12):#m
            declination_return.append(form_m[:-2]+dec_32m[i])
        for i in range(12):#f
            declination_return.append(form_f[:-2]+dec_32f[i])
        for i in range(12):#n
            declination_return.append(form_n[:-1]+dec_32n[i])
#Declination:33
    if form_f=='(gen.)':
        declination=3.3
        for i in range(12):#m
            declination_return.append(form_n[:-2]+dec_33m[i])
        declination_return[0]=declination_return[5]=form_m
        for i in range(12):#f
            declination_return.append(form_n[:-2]+dec_33f[i])
        declination_return[0+12]=declination_return[5+12]=form_m
        for i in range(12):#n
            declination_return.append(form_n[:-2]+dec_33n[i])
        declination_return[0+24]=declination_return[5+24]=form_m
    if len(list(declination_return))!=36:
        #print info,len(list(declination_return)),form_m,form_f,form_n
        return(-1,'error declination')
    return(declination_return,number,declination)
