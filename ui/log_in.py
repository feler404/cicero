#!/usr/bin/env python
# coding: UTF-8
# log_in.py
#
# library for: Cicero Open Project
# 

# PROGRAM CONSTATNTS
__version__='1.0.0'
__license__='GPL'
__author__='Cicero Develop Team'
__site__='www.cicero.glt.pl'

# IMPORTS
import pygtk
import gtk
import gobject
import menu
import string
import os
import global_direct as GD
pygtk.require('2.0')
from string import strip

#os.chdir(os.path.dirname(__file__))


class LogIn:

	
	def __init__(self):
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.set_title(GD.UILoginPanel)
		self.window.set_size_request(300,300)
		self.window.set_gravity(gtk.gdk.GRAVITY_SOUTH_EAST)
		width, height = self.window.get_size()
		self.window.move((gtk.gdk.screen_width() - width)/2, (gtk.gdk.screen_height() - height)/2)

		self.window.connect("destroy", lambda wid: gtk.main_quit())
		self.window.connect("delete_event", lambda a1,a2:gtk.main_quit())
		self.window.set_icon_from_file("./data/pic/log_in.png")
		self.window.unmaximize()
		self.window.unfullscreen()
		self.tooltips = gtk.Tooltips()
		#self.tooltips.set_delay(1000)

		self.welcome_label = box_button(self.window, "./data/pic/log_in.png", 
							GD.UIAuthorizationUser, horizont=True, pack='center', markup='big')
		self.welcome_label.show()
		
		self.e_name = gtk.Entry()
		self.e_name.set_max_length(15)
		self.e_name.connect("activate", self.add_user)
		self.e_name.set_text("")
		self.e_name.set_size_request(250, 40)
		self.e_name.show()

		self.b_add_entry = gtk.Button()
		self.b_add_entry.connect("clicked", self.add_user)
		self.b_add_entry.set_relief(gtk.RELIEF_NONE)
		button_box = box_button(self.window, "./data/pic/add_user1.png", "")
		self.b_add_entry.set_size_request(50, 40)
		self.b_add_entry.add(button_box)
		self.b_add_entry.show()
		self.tooltips.set_tip(self.b_add_entry, GD.UIAddUser)
		
		self.hbox_entry = gtk.HBox(False, 0)
		self.hbox_entry.add(self.e_name)
		self.hbox_entry.add(self.b_add_entry)
		self.hbox_entry.hide()
		
		self.b_choose = gtk.Button()
		self.b_choose.connect("clicked", self.choose_user,'')
		self.b_choose.set_relief(gtk.RELIEF_NONE)
		button_box = box_button(self.window, "./data/pic/choose_user.png", GD.UIChoose)
		self.b_choose.add(button_box)
		self.b_choose.show()
		self.tooltips.set_tip(self.b_choose, GD.UISelectUser)

		self.b_new = gtk.Button()
		self.b_new.connect("clicked", self.show_panel_add_user)
		self.b_new.set_relief(gtk.RELIEF_NONE)
		button_box = box_button(self.window, "./data/pic/new_user.png", GD.UINew)
		self.b_new.add(button_box)
		self.b_new.show()
		self.tooltips.set_tip(self.b_new, GD.UIAddNewUser)

		self.b_delete = gtk.Button()
		self.b_delete.connect("clicked", self.delete_user, '')
		self.b_delete.set_relief(gtk.RELIEF_NONE)
		button_box = box_button(self.window, "./data/pic/delete_user.png", GD.UIDelete)
		self.b_delete.add(button_box)
		self.b_delete.show()
		self.tooltips.set_tip(self.b_delete, GD.UIDeleteUser)
		
		(self.scrolled_window,self.tree_view) = self.create_list()
		self.scrolled_window.show()
		
		self.hbox = gtk.HBox(False, 0)
		self.hbox.add(self.b_choose)
		self.hbox.add(self.b_new)
		self.hbox.add(self.b_delete)
		self.hbox.show()


		
		self.hboxLanguage = gtk.HBox(False, 0)
		self.hboxLanguage.show()
		self.comboLanguage = gtk.combo_box_new_text()
		self.comboLanguage.show()
		self.comboLanguage.append_text('Język/Language/Lingua')
		self.comboLanguage.append_text('Polski')
		self.comboLanguage.append_text('English')
		self.comboLanguage.append_text('Latinus')
		self.comboLanguage.set_active(0)
		self.comboLanguage.connect('changed', self.refreshLanguage)
		self.hboxLanguage.add(self.comboLanguage)
		
		file = open(GD.configUILangFile,'r')
		comboLanguageText=file.readlines()
		file.close()
		if comboLanguageText==["UI_polski"]:
			self.comboLanguage.set_active(1)
		elif comboLanguageText==["UI_english"]:
			self.comboLanguage.set_active(2)
		elif comboLanguageText==["UI_latinus"]:
			self.comboLanguage.set_active(3)
		elif comboLanguageText==["\n"]:
			file.close()
			file = open(GD.configUILangFile,'w')
			file.writelines("UI_latinus")
			file.close()
			self.comboLanguage.set_active(3)
			
		self.vbox=gtk.VBox(False,0)
		self.vbox.add(self.welcome_label)
		self.vbox.add(self.scrolled_window)
		self.vbox.add(self.hbox)
		self.vbox.add(self.hbox_entry)
		self.vbox.add(self.hboxLanguage)
		self.vbox.show()
		
		self.window.add(self.vbox)
		self.window.show()
		
	def refreshLanguage(self=None, data=None):

		
		
		file = open(GD.configUILangFile,'w')
		comboLanguageText=self.comboLanguage.get_active_text()
		print comboLanguageText
		if comboLanguageText=="Język/Language/Lingua":
			file.writelines("UI_latinus")

		elif comboLanguageText=="Polski":
			file.writelines("UI_polski")

		elif comboLanguageText=="English":
			file.writelines("UI_english")

		elif comboLanguageText=="Latinus":
			file.writelines("UI_latinus")
		file.close()
		
		if GD.tempFalg==True:
			dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_OK)
			dialog.set_markup('Aby zastosować zmianę języka trzeba zresetować program\n\nTo change the language you use to reset the program')
			dialog.run()
			dialog.destroy()
		else:
			GD.tempFalg=not GD.tempFalg

		
	def create_list(self):
		# Create a new scrolled window, with scrollbars only if needed
		scrolled_window = gtk.ScrolledWindow()
		scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		list_stor = gtk.ListStore(gobject.TYPE_STRING)
		tree_view = gtk.TreeView(list_stor)
		scrolled_window.add_with_viewport (tree_view)
		tree_view.show()

		# Add some messages to the window
		users=loadUsers()
		for i in range(len(users)):
			msg = users[i]
			iter = list_stor.append()
			list_stor.set(iter, 0, msg)

		cell = gtk.CellRendererText()
		column = gtk.TreeViewColumn(GD.UIUsers, cell, text=0)
		tree_view.append_column(column)
		#print dir(list_stor)

		return (scrolled_window,tree_view)
		
	def show_panel_add_user(self, widget, data=None):
		self.hbox_entry.show()

	def add_user(self, widget, data=None):
		selection = self.tree_view.get_selection()
		list_stor, iter = selection.get_selected()
		if len(self.e_name.get_text())!=0:
			iter = list_stor.append()
			addUser(self.e_name.get_text())
			list_stor.set(iter, 0, self.e_name.get_text())
		self.e_name.set_text('')
		self.hbox_entry.hide()
	
	def choose_user(self, widget, data=None):
		selection = self.tree_view.get_selection()
		list_stor, iter = selection.get_selected()
		
		
		if iter:
			GD.userName=list_stor.get_value(iter,0)
			if not os.path.exists(GD.statsFilePath+'words_'+GD.userName+".txt"):
				file = open(GD.statsFilePath+'words_'+GD.userName+".txt","w")
				file.close()
				file = open(GD.statsFilePath+'gramatic_'+GD.userName+".txt","w")
				file.close()
			self.window.hide()
			menu.Menu_Window()

	def delete_user(self, widget, data=None):
		selection = self.tree_view.get_selection()
		list_stor, iter = selection.get_selected()
		if iter:
			delateUser(list_stor.get_value(iter,0))
			list_stor.remove(iter)

            


def box_button(parent, filename, label_text, horizont=False, pack=None, markup=None):

	box = gtk.VBox(False, 0)
	box.set_border_width(2)
	
	if horizont==True:
		box = gtk.HBox(False, 0)
		box.set_border_width(2)
		
	image = gtk.Image()
	image.set_from_file(filename)

	label = gtk.Label(label_text)
	if pack=='center':
		box.pack_start(image, True, False, 3)
		box.pack_start(label, True, False, 3)
	else:
		box.pack_start(image, False, False, 3)
		box.pack_start(label, False, False, 3)
	if markup=='big':
		label.set_markup("<big>"+label_text+"</big>");
	

	image.show()
	label.show()
	box.show()
	return(box)
   
def startLogIn():
	LogIn()
	gtk.main()

def loadUsers():
	file = open(GD.usersTabelFile)
	ret=[]
	try:
		for line in file:
			ret.append(strip(line))
	finally:
		file.close()
	return(ret)
		
def addUser(name):
	file = open(GD.usersTabelFile,'a')
	file.writelines(name+"\n")
	file.close()
	
	
def delateUser(name):
	users=loadUsers()
	users.remove(name)
	file = open(GD.usersTabelFile,'w')
	for i in range(len(users)):
		file.writelines(users[i]+"\n")
	file.close()

	
def destroy(self=None, widget=None, data=None):
	gtk.main_quit()

startLogIn()

