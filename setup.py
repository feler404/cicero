#!/usr/bin/env python

__author__ = 'Dawid Aniol'
__version__ = '1.0'
__mail__ = 'feler404@tlen.pl'


import shutil
from distutils.core import setup

#shutil.unpack_archive("./xml.tar.gz")

setup(name='Cicero',
      version=__version__,
      author=__author__,
      author_email=__mail__,
      description='Program to learn latin language',)